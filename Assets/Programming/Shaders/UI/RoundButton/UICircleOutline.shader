﻿Shader "PongTask/UI/CircleOutline"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)

        Radius("Radius", Float) = 0.0
        Thin("Thin", Range(0, 0.5)) = 0.0
        Fill("Fill", Range(0, 0.5)) = 0.0

        _StencilComp ("Stencil Comparison", Float) = 8
        _Stencil ("Stencil ID", Float) = 0
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255

        _ColorMask ("Color Mask", Float) = 15

        [Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass [_StencilOp]
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }

        Cull Off
        Lighting Off
        ZWrite Off
        ZTest [unity_GUIZTestMode]
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask [_ColorMask]

        Pass
        {
            Name "Default"
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0

            #include "UnityCG.cginc"
            #include "UnityUI.cginc"
            #include "RoundRectLib.cginc"

            #pragma multi_compile __ UNITY_UI_CLIP_RECT
            #pragma multi_compile __ UNITY_UI_ALPHACLIP

            #define USE_DERIVATIVES 0
            #define IMPROVE_EDGES 1

            struct appdata_t
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
                float2 texcoord : TEXCOORD0;
                float2 aspect : TEXCOORD1;
                float2 size : TEXCOORD2;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
                float3 texcoord : TEXCOORD0;
                float4 aspect : TEXCOORD2;
                float4 worldPosition : TEXCOORD1;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            sampler2D _MainTex;
            fixed4 _Color;
            fixed4 _TextureSampleAdd;

            float4 _ClipRect;
            float4 _MainTex_ST;

            half Thin;
            half Fill;

            v2f vert(appdata_t v)
            {
                v2f OUT;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
                OUT.worldPosition = v.vertex;
                OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

#if IMPROVE_EDGES
                OUT.texcoord = float3(TRANSFORM_TEX(v.texcoord, _MainTex), v.size.y);
#else
                OUT.texcoord = float3(TRANSFORM_TEX(v.texcoord, _MainTex), 0);
#endif

                OUT.color = v.color * _Color;
                OUT.aspect = lerp(float4(1, v.aspect.x, 0, v.aspect.y), float4(v.aspect.x, 1, v.aspect.y, 0), v.size.x);
                return OUT;
            }

            fixed4 frag(v2f IN) : SV_Target
            {
                half4 color = (tex2D(_MainTex, IN.texcoord) + _TextureSampleAdd) * IN.color;

                #ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif

                float corner = SdfRoundedRectangle((IN.texcoord.xy - 0.5f) * IN.aspect.xy, IN.aspect.zw);

#if USE_DERIVATIVES
                float w = fwidth(corner);
                color.a *= saturate((smoothstep(Thin - w, Thin, corner) - smoothstep(0.5 - w, 0.5, corner)) + smoothstep(
                    Fill, Fill - w, corner));
#elif IMPROVE_EDGES
                float w = IN.texcoord.z;
                color.a *= saturate((smoothstep(Thin - w, Thin, corner) - smoothstep(0.5 - w, 0.5, corner)) + smoothstep(
                    Fill, Fill - w, corner));
#else
                float w = 0.01;
                color.a *= saturate((smoothstep(Thin - w, Thin, corner) - smoothstep(0.5 - w, 0.5, corner)) + smoothstep(
                    Fill, Fill - w, corner));
#endif

                #ifdef UNITY_UI_ALPHACLIP
                clip (color.a - 0.001);
                #endif

                return color;
            }
            ENDCG
        }
    }
}
