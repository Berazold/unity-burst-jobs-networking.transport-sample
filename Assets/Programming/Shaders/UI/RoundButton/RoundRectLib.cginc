﻿// define USE_DERIVATIVES or IMPROVE_EDGES to improve quality
half Radius;

float SdfRoundedRectangle(float2 p, in float2 size)
{
    float2 q = abs(p) - size + Radius;
    return min(max(q.x, q.y), 0.0) + length(max(q, 0.0)) - Radius;
}

// aspect.xy - aspect, aspect.zw - size
void ClipRectCorner(in float2 uv, in float4 aspect, inout float4 color)
{
    float corner = SdfRoundedRectangle((uv - 0.5f) * aspect.xy, aspect.zw);
#if USE_DERIVATIVES
    float w = fwidth(corner);
    color.a *= smoothstep(0.5, 0.5 - w, corner);
#elif IMPROVE_EDGES
    corner *= IN.texcoord.z;
    color.a *= saturate(IN.texcoord.z * 0.5015 - corner);
#else
    color.a *= smoothstep(0.5, 0.49, corner);
#endif
}

void ClipCircleCorners(float2 uv, float thin, inout float4 color)
{
    uv -= 0.5;
    float factor = dot(uv, uv);
    color.a *= smoothstep(Radius, Radius + thin, factor);
}
