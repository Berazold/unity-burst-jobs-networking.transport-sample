﻿Shader "Pong/InkTrailsPathGenerator"
{
    Properties
    {
        NoiseScale("Noise Scale", Vector) = (0, 0, 0, 0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Noise3D.hlsl"

            #define NOISE_OCTAVES 4
            #define PERSISTENCE 1.25
            #define LACUNARITY 1

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            half4 NoiseScale;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            half2 frag (v2f i) : SV_Target
            {
                half2 noise = GeneratePerlinCurlNoise(i.uv, NoiseScale.x, NOISE_OCTAVES, PERSISTENCE, NoiseScale.y);
                return noise;
            }
            ENDCG
        }
    }
}
