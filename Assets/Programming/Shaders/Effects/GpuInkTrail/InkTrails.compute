﻿#pragma kernel Init
#pragma kernel Spawn
#pragma kernel Update

#include "Packing.hlsl"
#include "Noise3D.hlsl"

Texture2D<half2> CurlNoise;
Texture2D<half4> LastParticleBuffer;
RWTexture2D<half4> ParticleBuffer;

half4 SpawnDir;
uint SpawnLine;
half4 SpawnParams;

#define LIFETIME (SpawnParams.x)
#define INV_SPAWN_COUNT (SpawnParams.y)
#define INITIAL_DIRECTION (SpawnParams.zw)

half DeltaTime;
half Speed;
half4 FieldBounds;

half3 NoiseScale;

#define INIT_GROUPS 4
#define SPAWN_GROUPS 8
#define UPDATE_GROUPS 4

inline half2 Left(half2 v)
{
    return half2(-v.y, v.x);
}

[numthreads(INIT_GROUPS, INIT_GROUPS, 1)]
void Init(uint2 id : SV_DispatchThreadID)
{
    ParticleBuffer[id.xy] = half4(0, 0, 0, 0);
}

[numthreads(SPAWN_GROUPS, 1, 1)]
void Spawn(uint3 id : SV_DispatchThreadID)
{
    half2 pos = lerp(SpawnDir.xy, SpawnDir.zw, ((half)id.x) * INV_SPAWN_COUNT);
    half2 direction = mad(INITIAL_DIRECTION, 0.5h, 0.5h);

    half4 particle = half4(
        pos,
        Pack(direction),
        LIFETIME
    );
    ParticleBuffer[uint2(id.x, SpawnLine)] = particle;
}

#define frequency 0.2
#define NOISE_OCTAVES 4
#define persistence 1.25
#define lacunarity 1


[numthreads(UPDATE_GROUPS, UPDATE_GROUPS, 1)]
void Update(uint2 id : SV_DispatchThreadID)
{
    half4 particle = LastParticleBuffer[id.xy];
    [branch]
    if (particle.w <= 0)
        return;
    half2 direction = mad(Unpack(particle.z), 2, -1);

    half2 noise = GeneratePerlinCurlNoise(particle.xy, NoiseScale.x, NOISE_OCTAVES, persistence, NoiseScale.y);
    direction = noise * (NoiseScale.z * DeltaTime);

    direction = normalize(direction);

    half du = Speed * DeltaTime;
    particle.xy = mad(direction, du, particle.xy);
    particle.w -= DeltaTime;

    direction = mad(direction, 0.5h, 0.5h);
    particle.z = Pack(direction);

    ParticleBuffer[id.xy] = particle;
}
