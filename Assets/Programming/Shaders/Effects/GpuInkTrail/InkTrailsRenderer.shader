﻿Shader "Pong/InkTrailsRenderer"
{
    Properties
    {
        _Color("Color", Color) = (0, 0, 0, 0)
        _Z("Z Position", Range(-2, 2)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        Cull Off
        ZWrite Off
        ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.5

            #include "UnityCG.cginc"
            #include "Packing.hlsl"

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _ParticleBuffer;

            half4 _Color;
            uint _PointsPerLine;
            uint _LinesCount;
            half2 _InvMapSize;
            half _Z;
            half _LifeTimeInv;

            half2 _Size;

            v2f vert (uint vertexId : SV_VertexID, uint particleId : SV_InstanceID)
            {
                v2f o;
                float2 uv = float2(
                    (float)(particleId % _PointsPerLine) + 0.5f,
                    (float)((int)(particleId / _PointsPerLine) + 0.5f)
                );
                uv *= _InvMapSize;
                half4 particle = tex2Dlod(_ParticleBuffer, float4(uv, 0, 0));
                vertexId = vertexId & 1;

                half2 position = particle.xy;
                half2 direction = mad(Unpack(particle.z), 2, -1) * vertexId;

                half absLife = 2.0h * (0.5 - abs(mad(particle.w, _LifeTimeInv, -0.5h)));
                half size = _Size.x * absLife;
                position = mad(size, direction, position);
                o.vertex = mul(UNITY_MATRIX_VP, float4(position.xy, _Z, 1.0f));
                o.uv = float2(vertexId, 0.5);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return _Color;
            }
            ENDCG
        }
    }
}
