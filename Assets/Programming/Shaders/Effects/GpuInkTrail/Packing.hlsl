﻿inline half Pack(in half2 data)
{
    return floor(255.0h * data.x) + data.y;
}

half2 Unpack(half data)
{
    half2 result;
    result.y = frac(data);
    result.x = (data - result.y) / 255.0h;
    return result;
}
