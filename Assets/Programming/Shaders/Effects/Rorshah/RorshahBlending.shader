﻿Shader "Pong/RorshahBlending"
{
    Properties
    {
        _Frame ("Frame", 2D) = "white" {}
        _Period("Period", Range(0, 1)) = 0.0

        _Background("Background", Color) = (1, 1, 1, 1)
        _Foreground("Foreground", Color) = (0, 0, 0, 0)

        _Sharpness ("Sharpness", Range(0, 1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _Frame;
            float Time;
            half _Period;

            half4 _Background;
            half4 _Foreground;

            half _Sharpness;

            #define STEP_FROM (0.65h)
            #define STEP_TO (0.71h)

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                half4 noise = tex2D(_Frame, i.uv);
                half2 factor = lerp(noise.xz, noise.yw, _Period);
                half result = smoothstep(STEP_FROM, STEP_TO, factor.x) * saturate(1.0 - factor.y * _Sharpness);;

                return lerp(_Background, _Foreground, result);
            }
            ENDCG
        }
    }
}
