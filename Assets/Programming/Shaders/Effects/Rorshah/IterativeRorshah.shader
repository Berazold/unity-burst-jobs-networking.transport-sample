﻿Shader "Pong/IterativeRorshah"
{
    Properties
    {
        _Speed ("Speed", Range(0, 1)) = 0.0
        _Scale ("Scale", Range(0, 5)) = 0.0
        _SimulationTime ("SimulationTime", Vector) = (0.0, 0.0, 0.0, 0.0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        Cull Off
        ZWrite Off
        ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            float _Speed;
            float _Scale;
            float2 _SimulationTime;

            const float F3 =  0.3333333;
            const float G3 =  0.1666667;

            float3 random3(float3 c)
            {
                float j = 4096.0 * sin(dot(c, float3(17.0, 59.4, 15.0)));
                float3 r;
                r.z = frac(512.0 * j);
                j *= .125;
                r.x = frac(512.0 * j);
                j *= .125;
                r.y = frac(512.0 * j);
                return r - 0.5;
            }

            float simplex3d(float3 p)
            {
                 float3 s = floor(p + dot(p, F3.xxx));
                 float3 x = p - s + dot(s, G3.xxx);

                 float3 e = step(float3(0.0, 0.0, 0.0), x - x.yzx);
                 float3 i1 = e*(1.0 - e.zxy);
                 float3 i2 = 1.0 - e.zxy*(1.0 - e);

                 float3 x1 = x - i1 + G3;
                 float3 x2 = x - i2 + 2.0*G3;
                 float3 x3 = x - 1.0 + 3.0*G3;

                 float4 w, d;

                 w.x = dot(x, x);
                 w.y = dot(x1, x1);
                 w.z = dot(x2, x2);
                 w.w = dot(x3, x3);

                 w = max(0.6 - w, 0.0);

                 d.x = dot(random3(s), x);
                 d.y = dot(random3(s + i1), x1);
                 d.z = dot(random3(s + i2), x2);
                 d.w = dot(random3(s + 1.0), x3);

                 w *= w;
                 w *= w;
                 d *= w;

                 return dot(d, float4(52.0, 52.0, 52.0, 52.0));
            }

            float fbm(float3 p)
            {
                float f = 0.0;
                float frequency = 1.0;
                float amplitude = 0.5;
                for (int i = 0; i < 5; i++)
                {
                    f += simplex3d(p * frequency) * amplitude;
                    amplitude *= 0.5;
                    frequency *= 2.0 + float(i) / 100.0;
                }
                return min(f, 1.0);
            }

            inline float2 TriangleVertexToUV(float2 vertex)
            {
                float2 uv = mad(vertex, 0.5f, 0.5);
                return uv;
            }

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = float4(v.vertex.xy, 0.0, 1.0);
                o.uv = TriangleVertexToUV(v.vertex.xy);

                #if UNITY_UV_STARTS_AT_TOP
                    o.uv = mad(o.uv, float2(1.0, -1.0), float2(0.0, 1.0));
                #endif
                return o;
            }

            #define DotMultiplier (-3)

            half4 frag (v2f i) : SV_Target
            {
                half4 pos = half4(i.uv - 0.5f, 0, 0);
                half bounds = saturate(mad(dot(pos.xy, pos.xy), DotMultiplier, 1));

                i.uv.x =  1.0 - abs(mad(i.uv.x, 2.0f, -1));

                pos = half4(i.uv.x, i.uv.y, _SimulationTime.x, _SimulationTime.y) * half4(_Scale, _Scale, _Speed, _Speed);
                half shapeFrom = fbm(mad(pos.xyz, 3.0, 8.0)) + bounds;
                half shadeFrom = fbm(mad(pos.xyz, 2.0, 16.0));

                pos.z = _SimulationTime.y * _Speed;
                half shapeTo = fbm(mad(pos.xyw, 3.0, 8.0)) + bounds;
                half shadeTo = fbm(mad(pos.xyw, 2.0, 16.0));

                return half4(shapeFrom, shapeTo, shadeFrom, shadeTo);
            }

            ENDCG
        }
    }
}
