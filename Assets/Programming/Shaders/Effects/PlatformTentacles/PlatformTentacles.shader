﻿Shader "Pong/PlatformTentacles"
{
    Properties
    {
        _Color("Color", Color) = (0, 0, 0, 0)
        _Z ("Z", Range(-4, 4)) = 1
        _Speed ("_Speed", Range(-4, 44)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        Cull Off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            half _Z;
            half _Speed;
            half4 _Color;

            v2f vert (appdata v)
            {
                v2f o;

                half2 pos = v.vertex.xy;
                float3 wPos = mul(unity_ObjectToWorld, float4(pos.xy, _Z, 1.0f)).xyz;

                half moveFactor = abs(v.uv.z);
                half moveSide = sign(v.uv.z);

                half2 direction = sin(pos.x * v.vertex.z + v.uv.w + wPos.x + _Time.x * moveSide * _Speed) * v.vertex.w;
//                pos = mad(direction, moveFactor, pos);
                pos = mad(direction, moveFactor, wPos.xy);

//                clipPos = mul(UNITY_MATRIX_VP, float4(posWorld, 1.0));
//                o.vertex = UnityObjectToClipPos(float4(pos, _Z, 1.0f));
                o.vertex = mul(UNITY_MATRIX_VP, float4(pos, wPos.z, 1.0f));
                o.uv = v.uv.xy;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return _Color;
            }
            ENDCG
        }
    }
}
