﻿Shader "Pong/TentacleWall"
{
    Properties
    {
        _ColorFrom("Color From", Color) = (0, 0, 0, 0)
        _ColorTo("Color To", Color) = (0, 0, 0, 0)
        _ColorSpeed("Color Speed", Range(0, 12)) = 0
        _Width ("Width", Range(0, 1)) = 0.1
        _Z ("Z", Range(-4, 4)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        Cull Off
        ZWrite On

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.5
            #pragma multi_compile_instancing

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _ControlsMap;
            //1 / size, half texel size
            float4 _InvControlsSize;

            half4 _ColorFrom;
            half4 _ColorTo;
            half4 _SinFactor;
            half _ColorSpeed;

            float _Width;
            float _Z;

            v2f vert (appdata v, uint instanceId : SV_InstanceID)
            {
                float t = v.vertex.x;
                int index = (int)(v.vertex.y + 0.5f);

                float uvY = mad(instanceId, _InvControlsSize.y, _InvControlsSize.w);
                float u0 =  mad(index, _InvControlsSize.x, _InvControlsSize.z);

                float4 u = mad(float4(0, 1, 2, 3), _InvControlsSize.x, u0.xxxx);

                half4 cp0 = tex2Dlod(_ControlsMap, float4(u.x, uvY, 0, 0));
				half4 cp1 = tex2Dlod(_ControlsMap, float4(u.y, uvY, 0, 0));
				half4 cp2 = tex2Dlod(_ControlsMap, float4(u.z, uvY, 0, 0));
				half4 cp3 = tex2Dlod(_ControlsMap, float4(u.w, uvY, 0, 0));

                half size = lerp(cp1.z, cp2.z, t) * lerp(cp1.w, cp2.w, t);

				half2 base0 = -cp0.xy + cp3.xy + (cp1.xy - cp2.xy) * 3.0h;
				half2 base1 = 2.0h * cp0.xy - 5.0h * cp1.xy + 4.0h * cp2.xy - cp3.xy;

				half2 pos = 0.5h * (base0 * (t * t * t) + base1 * (t * t) + (-cp0.xy + cp2.xy) * t + 2.0h * cp1.xy);
				half2 tang = base0 * (t * t * 1.5h) + base1 * t + 0.5h * (cp2.xy - cp0.xy);

				tang = normalize(tang) * size;
				pos = pos + lerp(half2(-tang.y, tang.x), half2(tang.y, -tang.x), v.uv.y);

				v2f o;
                o.vertex = mul(UNITY_MATRIX_VP, float4(pos.xy, _Z, 1.0));
				o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                half factor = smoothstep(0.07h, 0.12h, sin(i.uv.y * _ColorSpeed) * 0.25h + 0.001h);
                return lerp(_ColorFrom, _ColorTo, factor);
            }
            ENDCG
        }
    }
}
