﻿Shader "Pong/InkLevelBounds"
{
    Properties
    {
        _Noise("Noise", 2D) = "white" {}
        _Shape("Shape", 2D) = "white" {}
        _Scale ("Scale", Vector) = (0, 0, 0, 0)
        _RoundCorners ("Corners", Vector) = (0, 0, 0, 0)
        _Tiling ("Tiling", Vector) = (0, 0, 0, 0)
        _Texture ("Texture", Vector) = (0, 0, 0, 0)
        _Color0 ("Color0", Color) = (0, 0, 0, 0)
        _Color1 ("Color1", Color) = (0, 0, 0, 0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _Noise;
            sampler2D _Shape;

            half4 _Scale;
            half4 _RoundCorners;
            half4 _Texture;
            half4 _Tiling;
            half4 _Color0;
            half4 _Color1;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            half Circle(half2 p)
            {
                return 1 - length(p);
            }

            half4 frag (v2f i) : SV_Target
            {
                half2 uv = (half2)i.uv * _RoundCorners.xy;

                half2 circles = max(Circle(uv - half2(0.995h, 0)), Circle(uv - half2(_RoundCorners.x - 0.95h, 0)));
                half area = smoothstep(_Scale.z, _Scale.w, i.uv.y + sin(i.uv.x * 30 + 0.5h) * 0.05h);
                area = min(area, max(circles + circles, step(1.0h, _RoundCorners.x - uv.x) * step(1.0h, uv.x)));

                half noise = tex2D(_Noise, i.uv * _Tiling.xy  + half2(0, _Time.x * _Scale.x)).r;
                half shape = tex2D(_Shape, i.uv * _Tiling.zw + half2(0, _Time.x * _Scale.y)).r;

                half bounds = smoothstep(_Texture.z, _Texture.w, i.uv.y);
                half color = smoothstep(_Texture.x, _Texture.y, saturate(noise * shape * area));
                return lerp(_Color0, _Color1, color);

                return noise * shape * area;
            }
            ENDCG
        }
    }
}
