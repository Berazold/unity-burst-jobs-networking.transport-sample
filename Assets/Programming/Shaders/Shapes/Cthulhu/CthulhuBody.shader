﻿Shader "Pong/CthulhuBody"
{
    Properties
    {
        _Color ("Color", Color) = (0, 0, 0, 0)
        _Scale("Scale", Range(0, 20)) = 0.0
        _Bounds("Bounds", Vector) = (0, 0, 0, 0)

        _BreathScale("Breath Scale", Range(0, 1)) = 0.0
        _BreathPeriod("Breath Period", Range(0, 34)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            half4 _Color;

            half _Scale;
            half4 _Bounds;

            half _BreathScale;
            half _BreathPeriod;

            v2f vert (appdata v)
            {
                v2f o;

                half breath = sin(_Time * _BreathPeriod) * 0.5h + 0.5h;
                half scale = _BreathScale * breath;
                scale = 1 + lerp(0, _BreathScale, breath);

                v.vertex = lerp(v.vertex, v.vertex * scale, v.vertex.z);
                v.vertex.z = 0;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            float noise(float2 p )
            {
                float2 f = frac(p);
                p = floor(p);
                float v = p.x + p.y * 1000.0;
                float4 r = float4(v, v + 1.0, v + 1000.0, v + 1001.0);
                r = frac(10000.0 * sin(r * .001));
                f = f * f * (3.0 - 2.0 * f);
                return 2.0 * (lerp(lerp(r.x, r.y, f.x), lerp(r.z, r.w, f.x), f.y)) - 1.0;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return _Color;
            }
            ENDCG
        }
    }
}
