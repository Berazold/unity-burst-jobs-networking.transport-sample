﻿Shader "Pong/CthulhuEye"
{
    Properties
    {
        _Eye("Eye", Vector) = (0, 0, 0, 0)
        _BlinkParams("Blink Params", Vector) = (0, 0, 0, 0)
        _Blink("Blink", Range(0.075, -0.5)) = 0
        _ViewDir("View Dir", Vector) = (0, 0, 0, 0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            half4 _Eye;
            half4 _ViewDir;
            half2 _BlinkParams;
            half _Blink;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = i.uv - 0.5f;
                uv = mad(uv, _Eye.zw, _ViewDir.xy);
                float factor = dot(uv, uv);

                return
                    min(
                        min(
                            smoothstep(_BlinkParams.x, _BlinkParams.y, sin(i.uv.y + _Blink)),
                            smoothstep(_BlinkParams.x, _BlinkParams.y, sin(1 - i.uv.y + _Blink))
                        ),
                        smoothstep(_Eye.x, _Eye.y, factor)
                    );
            }
            ENDCG
        }
    }
}
