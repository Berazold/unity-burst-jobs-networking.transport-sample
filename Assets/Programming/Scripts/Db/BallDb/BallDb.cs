﻿using System.Runtime.CompilerServices;
using Pong.Visual;
using Unity.Mathematics;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Pong
{
    public class BallDb : ScriptableObject
    {
        [SerializeField]
        private BallSetup[] _presets;

        public BallSetup[] Presets
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _presets;
        }

        public BallVisualBase FindVisual(int id)
        {
            ref var preset = ref _presets[math.max(0, id % _presets.Length)];
            return preset.Visual;
        }

#if UNITY_EDITOR

        [MenuItem("CrazyRam/Create/" + nameof(BallDb))]
        public static void Create()
        {
            var asset = CreateInstance<BallDb>();
            AssetDatabase.CreateAsset(asset, $"Assets/{nameof(BallDb)}.asset");
        }

#endif
    }
}
