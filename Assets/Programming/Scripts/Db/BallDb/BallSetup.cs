﻿using System;
using Pong.Visual;
using UnityEngine;

namespace Pong
{
    [Serializable]
    public struct BallSetup
    {
        public string Name;

        public Color Icon;

        public BallVisualBase Visual;
    }
}
