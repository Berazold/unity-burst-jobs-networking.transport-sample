﻿using CrazyRam.Core;
using Pong.UI;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Pong
{
    public class UIDb : DbIndexedEntry<UIPopupBase>
    {
        [SerializeField]
        private LeaderboardSlot _leaderboardSlot;

        [SerializeField]
        private BallSlot _ballSlot;

        [SerializeField]
        private EnvironmentSlot _environmentSlot;

        public void Preallocate()
        {
            UnityPoolAllocator<LeaderboardSlot>.Bind(_leaderboardSlot);
            UnityPoolAllocator<BallSlot>.Bind(_ballSlot);
            UnityPoolAllocator<EnvironmentSlot>.Bind(_environmentSlot);
        }

#if UNITY_EDITOR

        [MenuItem("CrazyRam/Create/Db/" + nameof(UIDb))]
        public static void Create()
        {
            var asset = CreateInstance<UIDb>();
            AssetDatabase.CreateAsset(asset, $"Assets/{nameof(UIDb)}.asset");
        }

#endif
    }
}
