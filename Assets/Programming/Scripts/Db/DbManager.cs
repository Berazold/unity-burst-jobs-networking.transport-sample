using System.Runtime.CompilerServices;
using CrazyRam.Core.Initialization;
using Pong.Utils;
using UnityEngine;

namespace Pong
{
    public class DbManager : MonoBehaviour, IInitializer
    {
        [SerializeField]
        private UIDb _uiDb;

        [SerializeField]
        private BallDb _ballDb;

        [SerializeField]
        private EnvironmentDb _environment;

        [SerializeField]
        private NetworkConfig _networkConfig;

        [SerializeField]
        private XORCipher16BytesKey _saveKey;

        [SerializeField]
        private int _topScoreCapacity;

        [SerializeField]
        private int _scoreIncrement;

        public UIDb UIDb
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _uiDb;
        }

        public BallDb BallDb
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _ballDb;
        }

        public EnvironmentDb Environment
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _environment;
        }

        public NetworkConfig NetworkConfig
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _networkConfig;
        }

        public XORCipher16BytesKey SaveKey
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _saveKey;
        }

        public int TopScoreCapacity
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _topScoreCapacity;
        }

        public int ScoreIncrement
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _scoreIncrement;
        }

        public void Init()
        {
            _uiDb.Init();
            _uiDb.Preallocate();
            ReferenceResolver.Instance.Push(this);
        }
    }
}
