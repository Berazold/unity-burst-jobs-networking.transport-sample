﻿using Unity.Mathematics;

namespace Pong
{
    [System.Serializable]
    public struct PlatformModel
    {
        public float2 Position;

        public float2 Forward;

        public float2 Extents;

        public float2 DirectionMask;
    }
}
