﻿using CrazyRam.Core.Helpers;
using Unity.Mathematics;
using UnityEngine;

namespace Pong
{
    [System.Serializable]
    public class EnvironmentCamera
    {
        [System.Serializable]
        public struct PlayerSideCameraPoint
        {
            public Vector3 Position;

            public Vector3 EulerAngles;
        }

        [SerializeField]
        private PlayerSideCameraPoint[] _cameraPoints;

        [SerializeField]
        private bool _isOrthographic;

        [SerializeField]
        private float _fov;

        [SerializeField]
        private Color _clearColor;

        [SerializeField]
        private CameraClearFlags _clearFlags;

        [SerializeField] //rect bounds
        private float3 _boundsSize;

        public void PlaceCamera(Camera camera)
        {
            if (!_cameraPoints.InBounds(0))
                return;
            var point = _cameraPoints[0];
            camera.transform.SetPositionAndRotation(point.Position, Quaternion.Euler(point.EulerAngles));
        }

        public void Apply(Camera camera)
        {
            camera.backgroundColor = _clearColor;
            camera.clearFlags = _clearFlags;

            camera.orthographic = _isOrthographic;
            camera.fieldOfView = _fov;
            camera.FitWorldSize(_boundsSize.x, _boundsSize.y);
        }
    }
}
