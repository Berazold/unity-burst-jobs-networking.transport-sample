﻿using Pong.Systems.Battle;
using Pong.Visual;
using Unity.Mathematics;

namespace Pong
{
    [System.Serializable]
    public struct ObstacleModel
    {
        public float2 Position;

        public float2 Extents;

        public float2 Forward;

        public PongPhysicsLayer Layer;

        public ObstacleBase VisualModel;

        public bool IsTrigger;
    }
}
