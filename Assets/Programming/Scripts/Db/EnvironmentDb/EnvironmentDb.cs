﻿using System.Runtime.CompilerServices;
using Unity.Mathematics;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Pong
{
    public class EnvironmentDb : ScriptableObject
    {
        [SerializeField]
        private EnvironmentSetup[] _environments;

        [SerializeField]
        private EnvironmentCamera _mainMenuCamera;

        public EnvironmentSetup[] Environments
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _environments;
        }

        public EnvironmentSetup Find(int id)
        {
            return _environments[math.max(0, id % _environments.Length)];
        }

        public EnvironmentCamera MainMenuCamera
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _mainMenuCamera;
        }

#if UNITY_EDITOR

        [MenuItem("CrazyRam/Create/Db/" + nameof(EnvironmentDb))]
        public static void Create()
        {
            var asset = CreateInstance<EnvironmentDb>();
            AssetDatabase.CreateAsset(asset, $"Assets/{nameof(EnvironmentDb)}.asset");
        }

#endif
    }
}
