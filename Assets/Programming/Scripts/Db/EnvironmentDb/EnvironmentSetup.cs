﻿using Pong.Visual;
using Unity.Mathematics;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Pong
{
    public class EnvironmentSetup : ScriptableObject
    {
        public string Name;

        public Color Icon;

        public int MaxPlayers;

        public int MaxNetworkPlayers;

        public float MaxStartDirectionAngle;

        public float PlatformSpeed;

        public float MaxPlatformReflectAngle;

        public EnvironmentBase Environment;

        public EnvironmentCamera Camera;

        public float4 MinMaxFieldBounds;

        public PlatformBase[] PlatformVariants;

        public PlatformModel[] PlatformModels;

        public BallModel[] BallModels;

        public ObstacleModel[] Obstacles;

#if UNITY_EDITOR

        [MenuItem("CrazyRam/Create/Visual/" + nameof(EnvironmentSetup))]
        public static void Create()
        {
            var asset = CreateInstance<EnvironmentSetup>();
            AssetDatabase.CreateAsset(asset, $"Assets/{nameof(EnvironmentSetup)}.asset");
        }

#endif
    }
}
