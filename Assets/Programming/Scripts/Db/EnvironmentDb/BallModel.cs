﻿using Unity.Mathematics;

namespace Pong
{
    [System.Serializable]
    public struct BallModel
    {
        public float2 Position;

        public float2 SpeedFromTo;

        public float2 RadiusFromTo;
    }
}
