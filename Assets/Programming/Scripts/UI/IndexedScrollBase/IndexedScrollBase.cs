﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using CrazyRam.Core;
using UnityEngine;

namespace Pong.UI
{
    [Serializable]
    public abstract class IndexedScrollBase<T, TV> where T : MonoBehaviour, IScrollEntry<TV>
    {
        [SerializeField]
        protected RectTransform Root;

        protected readonly List<T> ActiveSlots = new List<T>();

        public IReadOnlyList<T> ReadonlySlots
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => ActiveSlots;
        }

        public virtual void Release()
        {
            for (int i = 0; i < ActiveSlots.Count; i++)
            {
                var slot = ActiveSlots[i];
                if (!slot)
                    continue;
                slot.Hibernate();
                slot.ReturnToPool();
            }

            ActiveSlots.Clear();
        }

        public abstract void PostProcessEntry(T entry);

        public void Setup(TV[] data)
        {
            var allocator = UnityPoolAllocator<T>.Default;
            for (int i = 0; i < data.Length; i++)
            {
                var entry = allocator.NextOrNew();
                entry.Init(ref data[i], i);

                entry.gameObject.SetActive(true);
                entry.transform.SetParent(Root, false);
                PostProcessEntry(entry);
                ActiveSlots.Add(entry);
            }
        }
    }
}
