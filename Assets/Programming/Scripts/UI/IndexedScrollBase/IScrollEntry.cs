﻿namespace Pong.UI
{
    public interface IScrollEntry<T>
    {
        void Init(ref T data, int id);

        void Hibernate();
    }
}
