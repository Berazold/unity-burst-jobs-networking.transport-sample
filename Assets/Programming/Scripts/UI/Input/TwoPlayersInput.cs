﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Systems.Battle;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Pong.UI
{
    public class TwoPlayersInput : UIPopupBase, IPointerDownHandler, IPointerClickHandler, IPointerExitHandler,
        IPointerUpHandler, IDragHandler
    {
        private readonly struct PlayerInput
        {
            public readonly PlayerSide Side;

            public readonly float2 TouchStart;

            public readonly float2 Diff;

            public readonly bool Active;

            public PlayerInput(PlayerSide side, float2 touchStart, float2 diff = default, bool active = false)
            {
                Side = side;
                TouchStart = touchStart;
                Diff = diff;
                Active = active;
            }
        }

        private NativeHashMap<int, PlayerInput> _pointerToSide;

        private static float _minMoveDelta;

        private const float MinMoveRate = 0.05f;

        public override void Init(GlobalState state)
        {
            base.Init(state);
            _minMoveDelta = math.min(Screen.width, Screen.height) * MinMoveRate;
            transform.SetAsFirstSibling();

            if (!_pointerToSide.IsCreated)
            {
                _pointerToSide = new NativeHashMap<int, PlayerInput>(5, Allocator.Persistent);
                return;
            }

            ref var map = ref _pointerToSide;
            map.Clear();
        }

        public override void Refresh()
        {
            base.Refresh();
            var inputs = _pointerToSide.GetValueArray(Allocator.Temp);
            for (int i = 0; i < inputs.Length; i++)
            {
                var input = inputs[i];
                if (inputs[i].Active)
                    ClientCommutator<UIInputRequestEvent>.Default += new UIInputRequestEvent(input.Diff, input.Side);
            }

            inputs.Dispose();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static PlayerSide EventDataToSide(PointerEventData eventData)
        {
            return eventData.position.y < Screen.height / 2f ? PlayerSide.Bottom : PlayerSide.Top;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            var side = EventDataToSide(eventData);
            _pointerToSide.Add(eventData.pointerId, new PlayerInput(side, eventData.position));
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _pointerToSide.Remove(eventData.pointerId);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _pointerToSide.Remove(eventData.pointerId);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _pointerToSide.Remove(eventData.pointerId);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool CheckActive(float2 pos, in PlayerInput input)
        {
            return input.Active ||
                   math.any(math.abs(pos - input.TouchStart) > new float2(_minMoveDelta, _minMoveDelta));
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!_pointerToSide.TryGetValue(eventData.pointerId, out var input))
                return;
            var pos = (float2) eventData.position;
            var active = CheckActive(pos, in input);
            var diff = pos - input.TouchStart;
            _pointerToSide[eventData.pointerId] = new PlayerInput(input.Side, input.TouchStart, diff, active);
        }
    }
}
