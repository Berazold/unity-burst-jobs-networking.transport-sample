﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Systems.Battle;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Pong.UI
{
    public class SinglePlayerInput : UIPopupBase, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler,
        IDragHandler
    {
        private float2 _touchPoint;

        private float2 _diff;

        private bool _active;

        private static float _minMoveDelta;

        private const float MinMoveRate = 0.05f;

        public override void Init(GlobalState state)
        {
            base.Init(state);
            _minMoveDelta = math.min(Screen.width, Screen.height) * MinMoveRate;
            transform.SetAsFirstSibling();
        }

        public override void Refresh()
        {
            base.Refresh();
            if (_active)
                ClientCommutator<UIInputRequestEvent>.Default += new UIInputRequestEvent(_diff, PlayerSide.Bottom);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool CheckActive(float2 pos, float2 touchPoint, bool active)
        {
            return active ||
                   math.any(math.abs(pos - touchPoint) > new float2(_minMoveDelta, _minMoveDelta));
        }

        public void OnDrag(PointerEventData eventData)
        {
            var pos = (float2) eventData.position;
            _active = CheckActive(pos, _touchPoint, _active);
            _diff = pos - _touchPoint;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _touchPoint = eventData.position;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _active = false;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _active = false;
        }
    }
}
