﻿using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    public class UIGradientImage : BaseMeshEffect
    {
        private enum Side
        {
            Horizontal = 0,
            Vertical = 1
        }

        private interface IVertexColorizer
        {
            void Colorize(ref UIVertex vertex);
        }

        private readonly struct HorizontalColorizer : IVertexColorizer
        {
            private readonly Color _left;

            private readonly Color _right;

            public HorizontalColorizer(Color left, Color right)
            {
                _left = left;
                _right = right;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Colorize(ref UIVertex vertex)
            {
                vertex.color = Color.Lerp(_left, _right, vertex.uv0.x);
            }
        }

        private readonly struct VerticalColorizer : IVertexColorizer
        {
            private readonly Color _bottom;

            private readonly Color _top;

            public VerticalColorizer(Color bottom, Color top)
            {
                _bottom = bottom;
                _top = top;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Colorize(ref UIVertex vertex)
            {
                vertex.color = Color.Lerp(_bottom, _top, vertex.uv0.y);
            }
        }

        [SerializeField]
        private Color _from;

        [SerializeField]
        private Color _to;

        [SerializeField]
        private Side _side = Side.Horizontal;

        public override void ModifyMesh(VertexHelper vh)
        {
            if (_side == Side.Horizontal)
            {
                var colorizer = new HorizontalColorizer(_from, _to);
                Apply(ref colorizer, vh);
            }
            else
            {
                var colorizer = new VerticalColorizer(_from, _to);
                Apply(ref colorizer, vh);
            }
        }

        private static void Apply<T>(ref T colorizer, VertexHelper vh) where T : struct, IVertexColorizer
        {
            var vertex = new UIVertex();
            for (int i = 0; i < vh.currentVertCount; i++)
            {
                vh.PopulateUIVertex(ref vertex, i);
                colorizer.Colorize(ref vertex);
                vh.SetUIVertex(vertex, i);
            }
        }

        public void Setup(Color from, Color to)
        {
            _from = from;
            _to = to;
            graphic.SetVerticesDirty();
        }
    }
}
