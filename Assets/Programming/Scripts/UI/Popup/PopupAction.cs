﻿namespace Pong.UI
{
    public enum PopupAction : byte
    {
        Show = 0,
        Hide = 1,
        Clear = 2,
        Back = 3
    }
}
