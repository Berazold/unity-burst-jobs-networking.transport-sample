﻿using CrazyRam.Core.MessageBus;
using Pong.Events;
using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    public class UICloseablePopupBase : UIPopupBase
    {
        [SerializeField]
        private Button _background;

        [SerializeField]
        private Button _back;

        private float _lastBackClick;

        protected const float ClickDelay = 0.5f;

        public override void Init(GlobalState state)
        {
            base.Init(state);
            _background.onClick.AddListener(OnBackCick);
            _back.onClick.AddListener(OnBackCick);
            _lastBackClick = Time.realtimeSinceStartup - ClickDelay;
        }

        public override void ReleaseResources()
        {
            base.ReleaseResources();
            _background.onClick.RemoveAllListeners();
            _back.onClick.RemoveAllListeners();
        }

        private void OnBackCick()
        {
            if (Time.realtimeSinceStartup - _lastBackClick < ClickDelay)
                return;
            _lastBackClick = Time.realtimeSinceStartup;
            ClientCommutator<UIPopupActionEvent>.Default += new UIPopupActionEvent(PopupAction.Back);
        }
    }
}
