﻿using CrazyRam.Core;
using CrazyRam.Core.Initialization;
using UnityEngine;

namespace Pong.UI
{
    public class UIPopupBase : MonoBehaviour, IIndexedEntry, IDisposer
    {
        public int Id { get; private set; }

        public virtual bool Reusable => true;

        public PopupType Type => (PopupType) Id;

        public bool Active { get; protected set; }

        public void InitIndex(int id)
        {
            Id = id;
        }

        public virtual void Init(GlobalState state)
        {
        }

        public virtual void ReleaseResources()
        {
        }

        public virtual void Refresh()
        {
        }

        public void Bind(RectTransform root)
        {
            transform.SetParent(root, false);
        }

        public virtual void Show()
        {
            Active = true;
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            Active = false;
            if (Reusable)
                gameObject.SetActive(false);
            else
                Destroy(gameObject);
        }
    }
}
