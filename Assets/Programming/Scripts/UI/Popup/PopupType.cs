﻿namespace Pong.UI
{
    public enum PopupType : byte
    {
        NameSelection = 0,
        MainMenu = 1,
        NewGame = 2,
        Leaderboard = 3,
        Settings = 4,
        About = 5,
        InGameMenu = 6,
        InGameUI = 7,
        SinglePlayerInput = 8,
        TwoPlayersInput = 9,
        ScanningNetwork = 10,
        NoInternetConnection = 11,
        WaitingPlayers = 12,
        InternalNetworkError = 13,
    }
}
