﻿using System.Runtime.CompilerServices;
using UnityEngine;

namespace Pong.UI
{
    public class ScanningNetworkPopup : UIPopupBase
    {
        [SerializeField]
        private TextAppearanceLoopAnimation _textAnimation;

        public override bool Reusable
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => false;
        }

        public override void Init(GlobalState state)
        {
            base.Init(state);
            _textAnimation.Init();
        }
    }
}
