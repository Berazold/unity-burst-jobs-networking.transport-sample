﻿using System;
using TMPro;
using UnityEngine;

namespace Pong.UI
{
    public class LeaderboardSlot : MonoBehaviour, IScrollEntry<ScoreResultEntry>
    {
        [SerializeField]
        private TextMeshProUGUI _name;

        [SerializeField]
        private TextMeshProUGUI _score;

        [SerializeField]
        private TextMeshProUGUI _timestamp;

        public void Init(ref ScoreResultEntry result, int id)
        {
            gameObject.SetActive(true);
            _name.text = result.Name;
            _score.text = result.Score.ToString();

            var time = DateTime.FromBinary(result.TimeStamp);
            _timestamp.text = $"{time.Day}.{time.Month}.{time.Year % 100} {time.Hour:00}:{time.Minute:00}";
        }

        public void Hibernate()
        {
        }
    }
}
