﻿using System;
using CrazyRam.Core.Helpers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    public class LeaderboardPopup : UICloseablePopupBase
    {
        [Serializable]
        private class ScoreResultsIndexedScroll : IndexedScrollBase<LeaderboardSlot, ScoreResultEntry>
        {
            public override void PostProcessEntry(LeaderboardSlot entry)
            {
            }
        }

        [SerializeField]
        private TextMeshProUGUI _caption;

        [SerializeField]
        private ScrollRect _scroll;

        [SerializeField]
        private ScoreResultsIndexedScroll _resultsIndexedScroll;

        public override void Init(GlobalState state)
        {
            base.Init(state);
            SpawnSlots(state.TopScores);

            _caption.text = state.TopScores.Length > 0 ? $"Top {state.TopScores.Length} players" : "No top scores";
        }

        private void SpawnSlots(ScoreResultEntry[] results)
        {
            if (results.IsNullOrEmpty())
                return;
            _resultsIndexedScroll.Setup(results);
            _scroll.verticalNormalizedPosition = 1;
        }

        public override void ReleaseResources()
        {
            base.ReleaseResources();
            _resultsIndexedScroll.Release();
        }
    }
}
