﻿using UnityEngine;

namespace Pong.UI
{
    public class AboutPopup : UICloseablePopupBase
    {
        [SerializeField]
        private TextAppearanceAnimation _animation;

        public override void Init(GlobalState state)
        {
            base.Init(state);
            _animation.Init();

            var description = _animation.Text;
            description.rectTransform.sizeDelta =
                new Vector2(description.rectTransform.sizeDelta.x, description.preferredHeight);
        }
    }
}
