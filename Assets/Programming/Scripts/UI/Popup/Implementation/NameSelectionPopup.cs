﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    public class NameSelectionPopup : UIPopupBase
    {
        [SerializeField]
        private TMP_InputField _input;

        [SerializeField]
        private Button _ok;

        public override bool Reusable
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => false;
        }

        private const int MaxLength = 64;

        public override void Init(GlobalState state)
        {
            base.Init(state);
            var oldName = string.IsNullOrEmpty(state.Player.Name) ? state.Player.PreviousName : state.Player.Name;
            if (!string.IsNullOrEmpty(oldName))
                _input.text = oldName;
            if (_ok)
                _ok.onClick.AddListener(OnOkClick);

            _input.characterLimit = MaxLength;
        }

        public override void ReleaseResources()
        {
            base.ReleaseResources();
            if (_ok)
                _ok.onClick.RemoveAllListeners();
        }

        private void OnOkClick()
        {
            var playerName = _input.text;
            if (string.IsNullOrEmpty(playerName) || string.IsNullOrWhiteSpace(playerName) ||
                playerName.Length > MaxLength)
            {
                //todo show UI error
                return;
            }

            var renameMessage = new PlayerNameSelectedUIEvent(playerName);
            renameMessage.RaiseEvent();
        }
    }
}
