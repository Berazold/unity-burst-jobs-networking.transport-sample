﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.MessageBus;
using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    public class WaitingForPlayersPopup : UIPopupBase
    {
        [SerializeField]
        private TextAppearanceLoopAnimation _textAnimation;

        [SerializeField]
        private Button _cancel;

        public override bool Reusable
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => false;
        }

        private bool _used;

        public override void Init(GlobalState state)
        {
            base.Init(state);
            _used = false;
            _textAnimation.Init();
            _cancel.onClick.AddListener(OnCancel);
        }

        public override void ReleaseResources()
        {
            base.ReleaseResources();
            if (_cancel)
                _cancel.onClick.RemoveAllListeners();
        }

        private void OnCancel()
        {
            if (_used)
                return;
            _used = false;
            var cancel = new UIWaitingForPlayersCancelEvent();
            cancel.RaiseEvent();
        }
    }
}
