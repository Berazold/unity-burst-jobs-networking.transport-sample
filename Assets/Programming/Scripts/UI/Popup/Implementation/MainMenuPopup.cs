﻿using System;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    public class MainMenuPopup : UIPopupBase
    {
        [Serializable]
        private class MenuControlButton
        {
            [SerializeField]
            private PopupType _type;

            [SerializeField]
            private Button _button;

            private float _lastClick;

            private const float ClickDelay = 0.5f;

            public void Setup()
            {
                _lastClick = Time.realtimeSinceStartup - ClickDelay;
                if (_button)
                    _button.onClick.AddListener(OnClick);
            }

            public void Release()
            {
                if (_button)
                    _button.onClick.RemoveAllListeners();
            }

            private void OnClick()
            {
                if (Time.realtimeSinceStartup - _lastClick < ClickDelay)
                    return;
                _lastClick = Time.realtimeSinceStartup;
                ClientCommutator<UIPopupActionEvent>.Default += new UIPopupActionEvent(_type, PopupAction.Show, true);
            }
        }

        [SerializeField]
        private TextMeshProUGUI _caption;

        [SerializeField]
        private MenuControlButton[] _controls;

        public override void Init(GlobalState state)
        {
            base.Init(state);
            for (int i = 0; i < _controls.Length; i++)
                _controls[i].Setup();
            _caption.text = $"Welcome, {state.Player.Name}";
            _caption.rectTransform.sizeDelta =
                new Vector2(_caption.rectTransform.sizeDelta.x, _caption.preferredHeight);
        }

        public override void ReleaseResources()
        {
            base.ReleaseResources();
            for (int i = 0; i < _controls.Length; i++)
                _controls[i].Release();
        }
    }
}
