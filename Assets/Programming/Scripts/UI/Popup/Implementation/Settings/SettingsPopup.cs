﻿using System;
using CrazyRam.Core.Localization;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    public class SettingsPopup : UICloseablePopupBase
    {
        [Serializable]
        public class BallTypeIndexedScroll : IndexedScrollBase<BallSlot, BallSetup>
        {
            public override void PostProcessEntry(BallSlot entry)
            {
            }
        }

        [Serializable]
        public class EnvironmentTypeIndexedScroll : IndexedScrollBase<EnvironmentSlot, EnvironmentSetup>
        {
            public override void PostProcessEntry(EnvironmentSlot entry)
            {
            }
        }

        [SerializeField]
        private TMP_Dropdown _language;

        [SerializeField]
        private BallTypeIndexedScroll _ballsScroll;

        [SerializeField]
        private EnvironmentTypeIndexedScroll _environmentScroll;

        [SerializeField]
        private Button _clearSaves;

        public override void Init(GlobalState state)
        {
            base.Init(state);
            _language.onValueChanged.AddListener(OnLaunguageSelected);
            var dbManager = state.DbManager;
            _ballsScroll.Setup(dbManager.BallDb.Presets);
            _environmentScroll.Setup(dbManager.Environment.Environments);
            _clearSaves.onClick.AddListener(OnSavesClear);
        }

        public override void ReleaseResources()
        {
            base.ReleaseResources();
            _language.onValueChanged.RemoveAllListeners();
            _ballsScroll.Release();
            _environmentScroll.Release();
            _clearSaves.onClick.RemoveAllListeners();
        }

        private static void OnLaunguageSelected(int language)
        {
            var @event = new LocalizationLanguageChangeEvent((Languages) language);
            @event.RaiseEvent();
        }

        private static void OnSavesClear()
        {
            var @event = new DeleteSaveFileEvent();
            @event.RaiseEvent();
        }
    }
}
