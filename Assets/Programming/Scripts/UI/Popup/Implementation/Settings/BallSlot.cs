﻿using CrazyRam.Core.MessageBus;
using Pong.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    public class BallSlot : MonoBehaviour, IScrollEntry<BallSetup>
    {
        [SerializeField]
        private TextMeshProUGUI _caption;

        [SerializeField]
        private Image _icon;

        [SerializeField]
        private Button _background;

        private int _id;

        public void Init(ref BallSetup data, int id)
        {
            _id = id;
            _caption.text = data.Name;
            _icon.color = data.Icon;
            _background.onClick.AddListener(OnClick);
        }

        public void Hibernate()
        {
            _background.onClick.RemoveAllListeners();
        }

        private void OnClick()
        {
            var @event = new UIBallSelectedEvent(_id);
            @event.RaiseEvent();
        }
    }
}
