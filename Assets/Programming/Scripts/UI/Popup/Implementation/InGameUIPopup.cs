﻿using CrazyRam.Core.Helpers;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    public class InGameUIPopup : UIPopupBase
    {
        [SerializeField]
        private Button _settings;

        [SerializeField]
        private TextMeshProUGUI[] _playersInfo;

        [SerializeField]
        private TextMeshProUGUI _score;

        private string[] _nameCache;

        private const string InfoScheme = "{0} • {1}";

        private const string ScoreScheme = "Score: {0}";

        public override void Init(GlobalState state)
        {
            base.Init(state);
            _nameCache = new string[_playersInfo.Length];
            for (int i = 0; i < _playersInfo.Length; i++)
                _playersInfo[i].text = string.Empty;
            _settings.onClick.AddListener(OnSettings);
        }

        public override void ReleaseResources()
        {
            base.ReleaseResources();
            if (_settings)
                _settings.onClick.RemoveAllListeners();
        }

        public override void Refresh()
        {
            base.Refresh();
            UpdatePlayersSetup();
            UpdateScore();
        }

        private void UpdateScore()
        {
            ref var queue = ref ClientCommutator<BattleScoreUpdateEvent>.Default;
            var (hasValue, scoreUpdate) = queue.Drain();
            if (!hasValue)
                return;
            _score.text = string.Format(ScoreScheme, scoreUpdate.Score);
        }

        private void UpdatePlayersSetup()
        {
            ref var channel = ref ClientCommutator<UIInGameDataSetupEvent>.Default;
            if (!channel.HasNext)
                return;
            int readId = 0;
            while (channel.CanDrainQueue(readId))
            {
                ref readonly var message = ref channel.DrainQueueRef(ref readId);
                ProcessPlayerInfo(in message);
            }

            channel.ResetCounter();
        }

        private static void OnSettings()
        {
            var showSettings = new UIShowInGameSettingsEvent();
            showSettings.RaiseEvent();
        }

        private void ProcessPlayerInfo(in UIInGameDataSetupEvent data)
        {
            if (!_playersInfo.InBounds(data.Id))
                return;
            _nameCache[data.Id] = data.Name;
            _playersInfo[data.Id].text = string.Format(InfoScheme, _nameCache[data.Id], data.MaxScore);
        }
    }
}
