﻿using System;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    public class NewGamePopup : UICloseablePopupBase
    {
        [Serializable]
        private class NewGameButton
        {
            [SerializeField]
            private BattleType _type;

            [SerializeField]
            private Button _button;

            private float _lastClick;

            public void Setup()
            {
                _lastClick = Time.realtimeSinceStartup - ClickDelay;
                if (_button)
                    _button.onClick.AddListener(OnClick);
            }

            public void Release()
            {
                if (_button)
                    _button.onClick.RemoveAllListeners();
            }

            private void OnClick()
            {
                if (Time.realtimeSinceStartup - _lastClick < ClickDelay)
                    return;
                _lastClick = Time.realtimeSinceStartup;
                var message = new UIBattleTypeSelectedEvent(_type);
                message.RaiseEvent();
            }
        }

        [SerializeField]
        private NewGameButton[] _buttons;

        public override void Init(GlobalState state)
        {
            base.Init(state);
            for (int i = 0; i < _buttons.Length; i++)
                _buttons[i].Setup();
        }

        public override void ReleaseResources()
        {
            base.ReleaseResources();
            for (int i = 0; i < _buttons.Length; i++)
                _buttons[i].Release();
        }
    }
}
