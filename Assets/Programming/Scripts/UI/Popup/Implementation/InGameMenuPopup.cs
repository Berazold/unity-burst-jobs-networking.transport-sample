﻿using CrazyRam.Core.MessageBus;
using Pong.Events;
using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    public class InGameMenuPopup : UICloseablePopupBase
    {
        [SerializeField]
        private Button _switchSoundState;

        [SerializeField]
        private Button _restart;

        [SerializeField]
        private Button _exitToMenu;

        public override void Init(GlobalState state)
        {
            base.Init(state);

            _restart.onClick.AddListener(OnRestart);
            _exitToMenu.onClick.AddListener(OnExit);
            _switchSoundState.onClick.AddListener(OnSoundSwitch);

            var pauseRequest = new UIPauseRequestEvent(true);
            pauseRequest.RaiseEvent();
        }

        public override void ReleaseResources()
        {
            base.ReleaseResources();

            var pauseRequest = new UIPauseRequestEvent(false);
            pauseRequest.RaiseEvent();

            if (_restart)
                _restart.onClick.RemoveAllListeners();
            if (_exitToMenu)
                _exitToMenu.onClick.RemoveAllListeners();
            if (_switchSoundState)
                _switchSoundState.onClick.RemoveAllListeners();
        }

        private static void OnSoundSwitch()
        {
        }

        private static void OnRestart()
        {
            var restart = new UIRestartMatchRequestEvent();
            restart.RaiseEvent();
        }

        private static void OnExit()
        {
            var close = new UIInGameExitEvent();
            close.RaiseEvent();
        }
    }
}
