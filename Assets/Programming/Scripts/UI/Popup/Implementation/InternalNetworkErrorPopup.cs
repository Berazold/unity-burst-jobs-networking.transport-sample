﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    public class InternalNetworkErrorPopup : UIPopupBase
    {
        [SerializeField]
        private Button _ok;

        [SerializeField]
        private float _timeout;

        private float _initTime;

        private bool _clicked;

        public override bool Reusable
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => false;
        }

        public override void Init(GlobalState state)
        {
            base.Init(state);
            _initTime = Time.realtimeSinceStartup;
            _clicked = false;

            _ok.onClick.AddListener(OnOk);
        }

        private void OnDestroy()
        {
            if (_ok)
                _ok.onClick.RemoveAllListeners();
        }

        public override void Refresh()
        {
            base.Refresh();
            if (Time.realtimeSinceStartup - _initTime >= _timeout)
                OnOk();
        }

        private void OnOk()
        {
            if (_clicked)
                return;
            _clicked = true;
            var close = new UINetworkErrorCloseEvent();
            close.RaiseEvent();
        }
    }
}
