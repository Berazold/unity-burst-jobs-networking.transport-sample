﻿using UnityEngine;

namespace Phong.UI
{
    public static class UIRadiusHelper
    {
        private const float MinShaderRadius = -0.5f;

        private const float MaxShaderRadius = 0;

        public static readonly int RadiusId = Shader.PropertyToID("Radius");

        // public static Vector4 Calculate(Vector4 pixelRadius, Vector2 size)
        // {
        //     var minSide = Mathf.Min(size.x, size.y);
        //     var radius = (pixelRadius - (minSide * 0.5f).Xxxx()) / minSide;
        //
        //     radius = radius.Clamp(MinShaderRadius, MaxShaderRadius);
        //     return radius;
        // }

        public static float Calculate(float pixelRadius, Vector2 size)
        {
            var minSide = Mathf.Min(size.x, size.y);
            var radius = (pixelRadius - (minSide * 0.5f)) / minSide;
            radius = Mathf.Clamp(radius, MinShaderRadius, MaxShaderRadius);
            return radius;
        }
    }
}
