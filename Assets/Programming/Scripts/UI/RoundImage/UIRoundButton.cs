﻿using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class UIRoundButton : BaseMeshEffect
{
    [SerializeField, Range(0, 0.1f)]
    private float _sharpScale = 0.005f;

    //uv1 - scale, offset
    //uv2 - shape type, minSide
    public override void ModifyMesh(VertexHelper vh)
    {
        var size = ((RectTransform) transform).rect;

        float aspect = size.width > size.height ? size.width / size.height : size.height / size.width;
        float diff = (aspect - 1) * 0.5f;

        var sign = size.width > size.height ? 1 : 0;
        // var minSide = _sharpScale * math.min(size.width, size.height) / math.min((float) Screen.width, Screen.height);
        var minSide = _sharpScale * math.max(size.width, size.height) / math.max((float) Screen.width, Screen.height);

        var vertex = new UIVertex();
        for (int i = 0; i < vh.currentVertCount; i++)
        {
            vh.PopulateUIVertex(ref vertex, i);
            vertex.uv1 = new Vector2(aspect, diff);
            // vertex.uv2 = new Vector2(sign, minSide * sharpness);
            vertex.uv2 = new Vector2(sign, minSide);
            vh.SetUIVertex(vertex, i);
        }
    }
}
