﻿using UnityEngine;
using UnityEngine.UI;

namespace Phong.UI
{
    public class UICircleRadius : BaseMeshEffect
    {
        [SerializeField]
        private float _radius;

        public override void ModifyMesh(VertexHelper vh)
        {
            if (graphic.material == null)
                return;
            var size = ((RectTransform) transform).rect;
            var radius = UIRadiusHelper.Calculate(_radius, new Vector2(size.width, size.height));
            graphic.material.SetFloat(UIRadiusHelper.RadiusId, radius);
        }
    }
}
