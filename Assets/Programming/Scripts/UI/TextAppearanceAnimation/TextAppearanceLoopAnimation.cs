﻿using System.Runtime.CompilerServices;
using TMPro;
using Unity.Mathematics;
using UnityEngine;

namespace Pong.UI
{
    public class TextAppearanceLoopAnimation : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI _text;

        [SerializeField]
        private float _characterTime;

        [SerializeField]
        private int _lastCharactersCount;

        [SerializeField]
        private int _activeId;

        private float _progress;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ApplyCharactersLimit()
        {
            _text.maxVisibleCharacters = math.max(_text.text.Length - (_lastCharactersCount - _activeId), 0);
        }

        public void Init()
        {
            _progress = 0;
            _activeId = 0;
            ApplyCharactersLimit();
        }

        private void Update()
        {
            _progress += Time.deltaTime;
            var nextSymbold = (int) (_progress / _characterTime);
            if (nextSymbold == _activeId)
                return;
            _activeId = nextSymbold % (_lastCharactersCount + 1);
            ApplyCharactersLimit();
        }
    }
}
