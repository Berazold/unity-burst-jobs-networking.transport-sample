﻿using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;

namespace Pong.UI
{
    public class TextAppearanceAnimation : MonoBehaviour
    {
        private enum State
        {
            Awaiting = 0,
            Progress = 1
        }

        [SerializeField]
        private TextMeshProUGUI _text;

        [SerializeField]
        private float _characterTime;

        private int _activeSymbol;

        private State _state;

        private float _progress;

        public TextMeshProUGUI Text
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _text;
        }

        public void Init()
        {
            _state = State.Progress;
            _progress = 0;
            _activeSymbol = 0;
            _text.maxVisibleCharacters = _activeSymbol;
        }

        private void Update()
        {
            if (_state != State.Progress)
                return;

            _progress += Time.deltaTime;
            var nextSymbold = (int) (_progress / _characterTime);
            if (nextSymbold == _activeSymbol)
                return;
            _activeSymbol = nextSymbold;
            _text.maxVisibleCharacters = _activeSymbol;

            if (_activeSymbol >= _text.text.Length)
                _state = State.Awaiting;
        }
    }
}
