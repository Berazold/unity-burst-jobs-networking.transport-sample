﻿using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    public class UISdfImage : BaseMeshEffect
    {
        [SerializeField, Range(0, 1)]
        private float _threshold;

        [SerializeField, Range(0, 1)]
        private float _smoothness;

        //uv1 - threshold, smoothness
        public override void ModifyMesh(VertexHelper vh)
        {
            var size = ((RectTransform) transform).rect;
            var smoothness = _smoothness * math.max(size.width, size.height) /
                             math.max((float) Screen.width, Screen.height);

            var vertex = new UIVertex();
            for (int i = 0; i < vh.currentVertCount; i++)
            {
                vh.PopulateUIVertex(ref vertex, i);
                vertex.uv1 = new Vector2(_threshold, smoothness);
                vh.SetUIVertex(vertex, i);
            }
        }
    }
}
