﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using CrazyRam.Core.Framework;
using CrazyRam.Core.Helpers;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Network;
using Pong.Systems.Battle;
using Pong.Systems.Battle.Physics;
using Pong.UI;
using Pong.Utils;
using Pong.Visual;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
using Unity.Networking.Transport;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Pong.Systems
{
    public class BattleSystem : IGameSystem,
        IMessageReceiver<BattleStartEvent>,
        IMessageReceiver<UIInGameExitEvent>,
        IMessageReceiver<NetworkDisconnectEvent>,
        IMessageReceiver<BattlePlayerJoinEvent>,
        IMessageReceiver<BattleClientStartEvent>,
        IMessageReceiver<BattleServerStartEvent>,
        IMessageReceiver<BattleBallLostEvent>,
        IMessageReceiver<UIRestartMatchRequestEvent>,
        IMessageReceiver<UIPauseRequestEvent>,
        IMessageReceiver<NetworkCommandEvent<RoundResultCommand>>
    {
        private GlobalState _state;

        private BattleState _battleState;

        private IGameSystem[] _battleSystems;

        private bool ShouldUpdate
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => !_battleSystems.IsNullOrEmpty() && _battleState != null;
        }

        public void Init(ILevelState state)
        {
            _state = state as GlobalState;

            ClientDispatcher<BattleStartEvent>.Default += this;
            ClientDispatcher<UIInGameExitEvent>.Default += this;
            ClientDispatcher<NetworkDisconnectEvent>.Default += this;
            ClientDispatcher<BattleBallLostEvent>.Default += this;
            ClientDispatcher<NetworkCommandEvent<RoundResultCommand>>.Default += this;

            ClientDispatcher<BattlePlayerJoinEvent>.Default += this;
            ClientDispatcher<BattleClientStartEvent>.Default += this;
            ClientDispatcher<BattleServerStartEvent>.Default += this;
            ClientDispatcher<UIRestartMatchRequestEvent>.Default += this;
            ClientDispatcher<UIPauseRequestEvent>.Default += this;
        }

        public void Dispose()
        {
            DisposeBattle();

            ClientDispatcher<BattleStartEvent>.Default -= this;
            ClientDispatcher<UIInGameExitEvent>.Default -= this;
            ClientDispatcher<NetworkDisconnectEvent>.Default -= this;
            ClientDispatcher<BattleBallLostEvent>.Default -= this;
            ClientDispatcher<NetworkCommandEvent<RoundResultCommand>>.Default -= this;

            ClientDispatcher<BattlePlayerJoinEvent>.Default -= this;
            ClientDispatcher<BattleClientStartEvent>.Default -= this;
            ClientDispatcher<BattleServerStartEvent>.Default -= this;
            ClientDispatcher<UIRestartMatchRequestEvent>.Default -= this;
            ClientDispatcher<UIPauseRequestEvent>.Default -= this;
        }

        public void Update()
        {
            if (!ShouldUpdate)
                return;
            if ((_battleState.State == MatchState.Waiting ||
                 _battleState.State == MatchState.Ending) &&
                Time.realtimeSinceStartup - _battleState.LastStateSwapTime > 3)
                _battleState.State = MatchState.InProgress;

            _battleState.Tick++;
            for (int i = 0; i < _battleSystems.Length; i++)
                _battleSystems[i].Update();
        }

        public void LateUpdate()
        {
            if (!ShouldUpdate)
                return;
            for (int i = 0; i < _battleSystems.Length; i++)
                _battleSystems[i].LateUpdate();
        }

        public void PhysicsUpdate()
        {
            if (!ShouldUpdate)
                return;
            for (int i = 0; i < _battleSystems.Length; i++)
                _battleSystems[i].PhysicsUpdate();
        }

        private void OnRoundOver()
        {
            SendScoreEvent();
            ResetPhysicalPositions();
            _battleState.State = MatchState.Ending;

            var scores = _battleState.Scores;
            for (int i = 0; i < scores.Length; i++)
                scores[i] = 0;
            ClientCommutator<UIInputRequestEvent>.Default.ResetCounter();
            ClientCommutator<PhysicsContactEvent>.Default.ResetCounter();
            ClientCommutator<BattleChangeBoxDirectionEvent>.Default.ResetCounter();
            ClientCommutator<BattleChangeBallDirectionEvent>.Default.ResetCounter();
            ClientCommutator<NetworkCommandEvent<InputReplicationCommand>>.Default.ResetCounter();
            ClientCommutator<UIInputRequestEvent>.Default.ResetCounter();
            ClientCommutator<BattleScoreUpdateEvent>.Default.ResetCounter();

            ClientCommutator<BattleScoreUpdateEvent>.Default += new BattleScoreUpdateEvent(0);
        }

        private void ResetPhysicalPositions()
        {
            var environment = _state.DbManager.Environment.Find(_battleState.EnvironmentId);

            var platformModels = environment.PlatformModels;
            var ballModels = environment.BallModels;

            var ballDirections = MemoryHelper.Temporal<float3>(_battleState.World.Spheres.Length);
            for (int i = 0; i < ballDirections.Length; i++)
                ballDirections[i] = new float3(
                    SelectBallRandomDirection(environment.MaxStartDirectionAngle),
                    Random.Range(ballModels[i].RadiusFromTo.x, ballModels[i].RadiusFromTo.y) * 0.5f
                );

            var syncPositions = new BattleForceResetPhysics(platformModels, ballModels, ballDirections);
            syncPositions.RaiseEvent();
        }

        private static float2 SelectBallRandomDirection(float maxBallAngle)
        {
            var sign = Random.value >= 0.5f ? 1 : -1;

            maxBallAngle = math.radians(maxBallAngle);
            var angle = math.lerp(-maxBallAngle, maxBallAngle, Random.value);

            math.sincos(angle, out var sin, out var cos);
            var direction = new float2(-sin, cos) * sign;
            return math.normalize(direction);
        }

        private void SendScoreEvent()
        {
            if (_battleState == null)
                return;
            var scores = _battleState.Scores;
            if (!scores.IsCreated || _battleState.MyId >= scores.Length || _battleState.Type == BattleType.TwoPlayers)
                return;
            var roundOver = new BattleRoundOverEvent(scores[_battleState.MyId]);
            roundOver.RaiseEvent();
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
            if (_battleSystems.IsNullOrEmpty())
                return;
            for (int i = 0; i < _battleSystems.Length; i++)
                _battleSystems[i].OnDrawGizmos();
        }
#endif

        private void DisposeBattle()
        {
            if (!_battleSystems.IsNullOrEmpty())
            {
                for (int i = 0; i < _battleSystems.Length; i++)
                    _battleSystems[i].Dispose();
            }

            _battleSystems = Array.Empty<IGameSystem>();

            var clientStop = new NetworkClientStopEvent();
            clientStop.RaiseEvent();

            var serverStop = new NetworkServerStopEvent();
            serverStop.RaiseEvent();

            _battleState?.Dispose();
            _battleState = null;
        }

        private static IGameSystem SelectAISystem(BattleType type)
        {
            switch (type)
            {
                case BattleType.SinglePlayer:
                    return new AISystem();
                case BattleType.TwoPlayers:
                    return new AIPlaceholderSystem();
                case BattleType.Multiplayer:
                    return new AIPlaceholderSystem();
                case BattleType.Server:
                    return new AIPlaceholderSystem();
            }

            return new AIPlaceholderSystem();
        }

        private void SetupSinglePlayerSession()
        {
            _battleSystems = new[]
            {
                new InputSystem(),
                SelectAISystem(BattleType.SinglePlayer),
                new PhysicsSystem(),
                new CollisionSystem(),
                new VisualUpdateSystem(),
                new RenderExtensionSystem(),
            };
            for (int i = 0; i < _battleSystems.Length; i++)
                _battleSystems[i].Init(_state);

            var uiDataSetup = new UIInGameDataSetupEvent(_battleState.MyId, _state.Player.Name, (uint) _state.MaxScore);
            ClientCommutator<UIInGameDataSetupEvent>.Default += uiDataSetup;

            ClientCommutator<UIPopupActionEvent>.Default +=
                new UIPopupActionEvent(PopupType.SinglePlayerInput, PopupAction.Show, false, false);
        }

        private void SetupTwoPlayersSession()
        {
            _battleSystems = new[]
            {
                new InputSystem(),
                SelectAISystem(BattleType.TwoPlayers),
                new PhysicsSystem(),
                new CollisionSystem(),
                new VisualUpdateSystem(),
                new RenderExtensionSystem(),
            };
            for (int i = 0; i < _battleSystems.Length; i++)
                _battleSystems[i].Init(_state);
            ClientCommutator<UIPopupActionEvent>.Default +=
                new UIPopupActionEvent(PopupType.TwoPlayersInput, PopupAction.Show, false, false);
        }

        private void SetupServerSession()
        {
            _battleSystems = new[]
            {
                new ServerInputSystem(),
                SelectAISystem(BattleType.Server),
                new PhysicsSystem(),
                new WorldSerializationSystem(),
                new CollisionSystem(),
                new VisualUpdateSystem(),
                new RenderExtensionSystem(),
            };
            for (int i = 0; i < _battleSystems.Length; i++)
                _battleSystems[i].Init(_state);

            ClientCommutator<UIPopupActionEvent>.Default +=
                new UIPopupActionEvent(PopupType.SinglePlayerInput, PopupAction.Show, false, false);
        }

        private void SetupClientSession()
        {
            _battleSystems = new IGameSystem[]
            {
                new ClientInputSystem(),
                new WorldReplicationSystem(),
                new CollisionSystem(),
                new VisualUpdateSystem(),
                new RenderExtensionSystem(),
            };
            for (int i = 0; i < _battleSystems.Length; i++)
                _battleSystems[i].Init(_state);

            ClientCommutator<UIPopupActionEvent>.Default +=
                new UIPopupActionEvent(PopupType.SinglePlayerInput, PopupAction.Show, false, false);
        }

#region WorldInitialization

        private void SpawnObstacles(ObstacleModel[] obstacles)
        {
            var physics = _battleState.World;

            for (int i = 0; i < obstacles.Length; i++)
            {
                var id = Id<PhysicsSystem>.Next();
                var model = obstacles[i];

                physics.Obstacles[i] = new StaticBox
                {
                    Box = new Box
                    {
                        Position = model.Position,
                        Extents = model.Extents,
                        PhysicsId = id.Value
                    },
                    IsTrigger = model.IsTrigger
                };
            }
        }

        private void SpawnBalls(BallModel[] balls, float maxDirectionAngle)
        {
            var physics = _battleState.World;

            for (int i = 0; i < balls.Length; i++)
            {
                var id = Id<PhysicsSystem>.Next();
                var model = balls[i];
                float radius = Random.Range(model.RadiusFromTo.x, model.RadiusFromTo.y);
                float speed = Random.Range(model.SpeedFromTo.x, model.SpeedFromTo.y);

                physics.Spheres[i] = new Sphere
                {
                    Position = model.Position,
                    Direction = SelectBallRandomDirection(maxDirectionAngle),
                    Speed = speed,
                    PhysicsId = id.Value,
                    Radius = radius * 0.5f
                };
            }
        }

        private void SpawnPlatforms(PlatformModel[] platforms)
        {
            var physics = _battleState.World;

            for (int i = 0; i < platforms.Length; i++)
            {
                var id = Id<PhysicsSystem>.Next();
                var model = platforms[i];

                _battleState.PhysicalIdToPlayerId[id.Value] = i;
                _battleState.PlatformParams[i] = new PlatformParams(model.Forward, model.DirectionMask);
                physics.Platforms[i] = new DynamicBox
                {
                    Box = new Box
                    {
                        Position = model.Position,
                        Extents = model.Extents,
                        PhysicsId = id.Value
                    },
                    Direction = float2.zero,
                    Forward = model.Forward
                };
            }
        }

        private void SpawnPlatformsVisual(PlatformModel[] platforms, EnvironmentSetup environmentSetup)
        {
            var world = _battleState.World;
            var visualWorld = _battleState.VisualWorld;

            for (int i = 0; i < platforms.Length; i++)
            {
                var model = platforms[i];

                var instance = Object.Instantiate(environmentSetup.PlatformVariants.Random());
                instance.Init(model.Position, model.Forward);
                visualWorld.Platforms[i] = instance;

                world.Properties[world.Platforms[i].Box.PhysicsId] =
                    new PhysicalProperties(PongPhysicsLayer.Platform, instance.CallbackReceived);
            }
        }

        private void SpawnBallsVisual(BallModel[] balls, BallVisualBase ballReference)
        {
            var world = _battleState.World;
            var visualWorld = _battleState.VisualWorld;

            for (int i = 0; i < balls.Length; i++)
            {
                var model = balls[i];

                var instance = Object.Instantiate(ballReference);
                instance.Init(model.Position);
                instance.FitSize(world.Spheres[i].Radius * 2);
                visualWorld.BallVisuals[i] = instance;

                world.Properties[world.Spheres[i].PhysicsId] =
                    new PhysicalProperties(PongPhysicsLayer.Ball, instance.CallbackReceived);
            }
        }

        private void SpawnObstaclesVisual(ObstacleModel[] obstacles)
        {
            var world = _battleState.World;
            var visualWorld = _battleState.VisualWorld;

            for (int i = 0; i < obstacles.Length; i++)
            {
                var model = obstacles[i];

                var instance = Object.Instantiate(model.VisualModel);
                instance.Init(model.Position, model.Forward);
                visualWorld.Obstacles[i] = instance;

                world.Properties[world.Obstacles[i].Box.PhysicsId] =
                    new PhysicalProperties(model.Layer, instance.CallbackReceived);
            }
        }

        private static PongPhysicsWorld InitPhysicalWorld(EnvironmentSetup environmentSetup)
        {
            var platforms = environmentSetup.PlatformModels;
            var balls = environmentSetup.BallModels;
            var obstacles = environmentSetup.Obstacles;

            const NativeArrayOptions unclearedMemory = NativeArrayOptions.UninitializedMemory;
            const int maxContactsPerFrame = 10;
            var contacts = new NativeFixedList<PhysicsContactEvent>(Allocator.Persistent, maxContactsPerFrame, true);
            var physics = new PongPhysicsWorld
            {
                Platforms = MemoryHelper.Persistent<DynamicBox>(platforms.Length, unclearedMemory),
                Spheres = MemoryHelper.Persistent<Sphere>(balls.Length, unclearedMemory),
                Obstacles = MemoryHelper.Persistent<StaticBox>(obstacles.Length, unclearedMemory),
                Contacts = contacts,
                Properties = new Dictionary<int, PhysicalProperties>(),
                PlatformSpeed = environmentSetup.PlatformSpeed,
                MaxReflectAngle = environmentSetup.MaxPlatformReflectAngle,
                Bounds = environmentSetup.MinMaxFieldBounds
            };
            return physics;
        }

        private void SpawnWorld(BattleType type, int environmentType)
        {
            var dbManager = _state.DbManager;
            var environmentSetup = dbManager.Environment.Find(environmentType);

            environmentSetup.Camera.PlaceCamera(_state.SceneSetup.Camera);
            environmentSetup.Camera.Apply(_state.SceneSetup.Camera);

            _battleState = new BattleState(type);
            _state.BattleState = _battleState;
            _battleState.EnvironmentId = environmentType;
            _battleState.State = MatchState.Waiting;

            var physics = InitPhysicalWorld(environmentSetup);
            const NativeArrayOptions unclearedMemory = NativeArrayOptions.UninitializedMemory;

            var platforms = environmentSetup.PlatformModels;
            var balls = environmentSetup.BallModels;
            var obstacles = environmentSetup.Obstacles;

            _battleState.World = physics;
            _battleState.Scores = MemoryHelper.Persistent<int>(platforms.Length);
            _battleState.PlatformParams = MemoryHelper.Persistent<PlatformParams>(platforms.Length, unclearedMemory);

            Id<PhysicsSystem>.Reset();
            SpawnPlatforms(platforms);
            SpawnBalls(balls, environmentSetup.MaxStartDirectionAngle);
            SpawnObstacles(obstacles);
        }

        private void SpawnVisualWorld(int environmentType, int ballType)
        {
            var dbManager = _state.DbManager;

            var ballReference = dbManager.BallDb.FindVisual(ballType);
            var environmentSetup = dbManager.Environment.Find(environmentType);

            var platforms = environmentSetup.PlatformModels;
            var balls = environmentSetup.BallModels;
            var obstacles = environmentSetup.Obstacles;

            var visualWorld = new VisualWorld(
                platforms.Length,
                balls.Length,
                obstacles.Length,
                environmentSetup.Environment
            );
            _battleState.VisualWorld = visualWorld;

            SpawnPlatformsVisual(platforms, environmentSetup);
            SpawnBallsVisual(balls, ballReference);
            SpawnObstaclesVisual(obstacles);
        }

#endregion

        public void OnMessage(in BattleStartEvent data)
        {
            SpawnWorld(data.Type, _state.PreferredEnvironmentType);
            switch (data.Type)
            {
                case BattleType.SinglePlayer:
                    _battleState.AIPlayers.Add((int) PlayerSide.Top);
                    SetupSinglePlayerSession();
                    break;
                case BattleType.TwoPlayers:
                    SetupTwoPlayersSession();
                    break;
            }

            SpawnVisualWorld(_state.PreferredEnvironmentType, _state.PreferredBallType);
        }

        public void OnMessage(in UIInGameExitEvent data)
        {
            SendScoreEvent();
            DisposeBattle();
        }

        public void OnMessage(in NetworkDisconnectEvent data)
        {
            SendScoreEvent();
            DisposeBattle();
        }

        public void OnMessage(in BattlePlayerJoinEvent data)
        {
            //todo support > 2 players

            var stopAcceptNewClients = new NetworkServerStopAccepClientsEvent();
            stopAcceptNewClients.RaiseEvent();
        }

        private unsafe void PushSphereParams(int ballParametersLength, long ballParameters)
        {
            if (ballParametersLength <= 0)
                return;
            var ballParams = UnityHelper.ExistingDataToNativeArray<half>((void*) ballParameters, ballParametersLength);

            var spheres = _battleState.World.Spheres;
            var sphereSize = (long) UnsafeUtility.SizeOf<Sphere>();
            var spheresPtr = (long) spheres.GetUnsafePtr();

            int count = math.min(spheres.Length, ballParametersLength);
            for (int i = 0; i < count; i++)
            {
                var sphere = (Sphere*) math.mad(i, sphereSize, spheresPtr);
                sphere->Radius = ballParams[i];
            }


            var visualWorld = _battleState.VisualWorld;
            count = math.min(visualWorld.BallVisuals.Length, ballParametersLength);
            for (int i = 0; i < count; i++)
                visualWorld.BallVisuals[i].FitSize(ballParams[i] * 2);

#if UNITY_EDITOR
            ballParams.ReleaseSafetyHandle();
#endif
        }

        public void OnMessage(in BattleClientStartEvent data)
        {
            var matchParams = data.MatchParams;
            SpawnWorld(BattleType.Multiplayer, matchParams.LocationType);
            _battleState.MyId = data.MatchParams.PlatformId;

            SetupClientSession();
            SpawnVisualWorld(matchParams.LocationType, matchParams.BallType);
            PushSphereParams(matchParams.BallParametersLength, matchParams.BallParameters);

            var uiDataSetup = new UIInGameDataSetupEvent(_battleState.MyId, _state.Player.Name, (uint) _state.MaxScore);
            ClientCommutator<UIInGameDataSetupEvent>.Default += uiDataSetup;

            //todo send server id
            var serverSide = matchParams.PlatformId == (int) PlayerSide.Top ? PlayerSide.Bottom : PlayerSide.Top;
            uiDataSetup = new UIInGameDataSetupEvent((int) serverSide, matchParams.ServerName.ToString(),
                matchParams.MaxServerScore);
            ClientCommutator<UIInGameDataSetupEvent>.Default += uiDataSetup;
        }

        public unsafe void OnMessage(in BattleServerStartEvent data)
        {
            var environmentType = _state.PreferredEnvironmentType;
            var ballType = _state.PreferredBallType;
            SpawnWorld(BattleType.Server, environmentType);

            _battleState.MyId = (int) PlayerSide.Bottom;
            const int enemyId = (int) PlayerSide.Top;
            _battleState.NetworkToPlayerId[data.Client.InternalId] = enemyId;

            SetupServerSession();
            SpawnVisualWorld(environmentType, ballType);

            var stopAcceptNewClients = new NetworkServerStopAccepClientsEvent();
            stopAcceptNewClients.RaiseEvent();

            var balls = _battleState.World.Spheres;
            var ballParameters =
                UnsafeUtility.Malloc(sizeof(ushort) * balls.Length, UnsafeUtility.AlignOf<half>(), Allocator.TempJob);
            for (int i = 0; i < balls.Length; i++)
                UnsafeUtility.WriteArrayElement(ballParameters, i, new half(balls[i].Radius));

            var response = new MatchConnectionResponse(
                _state.Player.Name, (uint) _state.MaxScore, (long) ballParameters, (byte) balls.Length,
                (byte) environmentType, (byte) ballType, enemyId, Allocator.TempJob);
            SendMatchConnectionResponse(data.Client, ref response);

            var uiDataSetup = new UIInGameDataSetupEvent(_battleState.MyId, _state.Player.Name, (uint) _state.MaxScore);
            ClientCommutator<UIInGameDataSetupEvent>.Default += uiDataSetup;

            var request = data.Request;
            uiDataSetup = new UIInGameDataSetupEvent(enemyId, request.Name.ToString(), request.MaxScore);
            ClientCommutator<UIInGameDataSetupEvent>.Default += uiDataSetup;
        }

        private void SendMatchConnectionResponse(NetworkConnection address, ref MatchConnectionResponse response)
        {
            var networkState = _state.NetworkState;

            var serializedMessage = SerializationHelper.SerializeCommand(ref response, _state.NetworkState.Provider);
            var transportMessage = new TransportMessage(address, serializedMessage.Data,
                _state.BattleState.Tick, serializedMessage.Size, _state.NetworkState.IsLittleEndian,
                serializedMessage.CommandType);
            networkState.ActiveOutput.Enqueue(transportMessage);

            var clone = TransportMessage.Clone(in transportMessage);
            networkState.ActiveOutput.Enqueue(clone);
        }

        public void OnMessage(in NetworkCommandEvent<RoundResultCommand> data)
        {
            OnRoundOver();
            var command = data.Command;
            PushSphereParams(command.BallParametersLength, command.BallParameters);
            command.Dispose();
        }

        public void OnMessage(in BattleBallLostEvent data)
        {
            RestartLevel();
        }

        public void OnMessage(in UIRestartMatchRequestEvent data)
        {
            RestartLevel();
            ClientCommutator<UIPopupActionEvent>.Default +=
                new UIPopupActionEvent(PopupType.InGameMenu, PopupAction.Hide);
        }

        private void RestartLevel()
        {
            OnRoundOver();
            if (_battleState.Type == BattleType.Server)
                SendRoundResult();
        }

        private unsafe void SendRoundResult()
        {
            var activeConnections = _state.NetworkState.ActiveConnections;
            for (int i = 0; i < activeConnections.Length; i++)
            {
                var connection = activeConnections[i];
                if (!_battleState.NetworkToPlayerId.TryGetValue(connection.InternalId, out var playerId))
                    continue;
                var score = _battleState.Scores[playerId];

                var balls = _battleState.World.Spheres;
                var ballParameters = UnsafeUtility.Malloc(sizeof(ushort) * balls.Length, UnsafeUtility.AlignOf<half>(),
                    Allocator.Temp);
                for (int ballId = 0; ballId < balls.Length; ballId++)
                    UnsafeUtility.WriteArrayElement(ballParameters, ballId, new half(balls[ballId].Radius));

                var roundResult = new RoundResultCommand((uint) score, (byte) balls.Length, (long) ballParameters,
                    Allocator.Temp);
                var serializedMessage =
                    SerializationHelper.SerializeCommand(ref roundResult, _state.NetworkState.Provider);
                var transportMessage = new TransportMessage(connection, serializedMessage.Data,
                    _state.BattleState.Tick, serializedMessage.Size, _state.NetworkState.IsLittleEndian,
                    serializedMessage.CommandType);
                _state.NetworkState.ActiveOutput.Enqueue(transportMessage);
            }
        }

        public void OnMessage(in UIPauseRequestEvent data)
        {
            if (_battleState != null && _battleState.Type != BattleType.Multiplayer)
                _battleState.State = data.On ? MatchState.Pause : MatchState.InProgress;
        }
    }
}
