﻿using System;
using CrazyRam.Core.Framework;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Network;
using Pong.UI;
using Pong.Utils;

namespace Pong.Systems
{
    public class LifecycleSystem : IGameSystem,

#region Events

//todo code generation
        IMessageReceiver<CoreSceneLoadedEvent>,
        IMessageReceiver<PlayerNameSelectedUIEvent>,
        IMessageReceiver<UIBallSelectedEvent>,
        IMessageReceiver<UIEnvironmentSelectedEvent>,
        IMessageReceiver<DeleteSaveFileEvent>,
        IMessageReceiver<NetworkConnectionRefreshEvent>,
        IMessageReceiver<UIBattleTypeSelectedEvent>,
        IMessageReceiver<UINoInternetConnectionTimeoutEvent>,
        IMessageReceiver<UIWaitingForPlayersCancelEvent>,
        IMessageReceiver<NetworkScanTimeoutEvent>,
        IMessageReceiver<NetworkConnectedToServerEvent>,
        IMessageReceiver<UIShowInGameSettingsEvent>,
        IMessageReceiver<NetworkScanErrorEvent>,
        IMessageReceiver<UINetworkErrorCloseEvent>,
        IMessageReceiver<UIInGameExitEvent>,
        IMessageReceiver<NetworkDisconnectEvent>,
        IMessageReceiver<BattleRoundOverEvent>,
        IMessageReceiver<NetworkCommandEvent<MatchConnectionResponse>>,
        IMessageReceiver<NetworkCommandEvent<MatchConnectionRequest>>,
        IPointerMessageReceiver<SaveGameEvent>

#endregion

    {
        private GlobalState _state;

        public void Init(ILevelState state)
        {
            _state = state as GlobalState;

#region Subscribe

            ClientDispatcher<CoreSceneLoadedEvent>.Default += this;
            ClientDispatcher<PlayerNameSelectedUIEvent>.Default += this;

            ClientPointerDispatcher<SaveGameEvent>.Default += this;

            ClientDispatcher<UIBallSelectedEvent>.Default += this;
            ClientDispatcher<UIEnvironmentSelectedEvent>.Default += this;
            ClientDispatcher<DeleteSaveFileEvent>.Default += this;

            ClientDispatcher<NetworkConnectionRefreshEvent>.Default += this;
            ClientDispatcher<UIBattleTypeSelectedEvent>.Default += this;
            ClientDispatcher<UINoInternetConnectionTimeoutEvent>.Default += this;
            ClientDispatcher<UIWaitingForPlayersCancelEvent>.Default += this;
            ClientDispatcher<NetworkScanTimeoutEvent>.Default += this;
            ClientDispatcher<NetworkConnectedToServerEvent>.Default += this;
            ClientDispatcher<UIShowInGameSettingsEvent>.Default += this;
            ClientDispatcher<NetworkScanErrorEvent>.Default += this;
            ClientDispatcher<UINetworkErrorCloseEvent>.Default += this;
            ClientDispatcher<UIInGameExitEvent>.Default += this;
            ClientDispatcher<NetworkDisconnectEvent>.Default += this;
            ClientDispatcher<BattleRoundOverEvent>.Default += this;

            ClientDispatcher<NetworkCommandEvent<MatchConnectionResponse>>.Default += this;
            ClientDispatcher<NetworkCommandEvent<MatchConnectionRequest>>.Default += this;

#endregion
        }

        public void Dispose()
        {
#region Unsubscribe

            ClientDispatcher<CoreSceneLoadedEvent>.Default -= this;
            ClientDispatcher<PlayerNameSelectedUIEvent>.Default -= this;

            ClientPointerDispatcher<SaveGameEvent>.Default -= this;

            ClientDispatcher<UIBallSelectedEvent>.Default -= this;
            ClientDispatcher<UIEnvironmentSelectedEvent>.Default -= this;
            ClientDispatcher<DeleteSaveFileEvent>.Default -= this;

            ClientDispatcher<NetworkConnectionRefreshEvent>.Default -= this;
            ClientDispatcher<UIBattleTypeSelectedEvent>.Default -= this;
            ClientDispatcher<UINoInternetConnectionTimeoutEvent>.Default -= this;
            ClientDispatcher<UIWaitingForPlayersCancelEvent>.Default -= this;
            ClientDispatcher<NetworkScanTimeoutEvent>.Default -= this;
            ClientDispatcher<NetworkConnectedToServerEvent>.Default -= this;
            ClientDispatcher<UIShowInGameSettingsEvent>.Default -= this;
            ClientDispatcher<NetworkScanErrorEvent>.Default -= this;
            ClientDispatcher<UINetworkErrorCloseEvent>.Default -= this;
            ClientDispatcher<UIInGameExitEvent>.Default -= this;
            ClientDispatcher<NetworkDisconnectEvent>.Default -= this;
            ClientDispatcher<BattleRoundOverEvent>.Default -= this;

            ClientDispatcher<NetworkCommandEvent<MatchConnectionResponse>>.Default -= this;
            ClientDispatcher<NetworkCommandEvent<MatchConnectionRequest>>.Default -= this;

#endregion
        }

        public void Update()
        {
        }

        public void LateUpdate()
        {
        }

        public void PhysicsUpdate()
        {
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
        }
#endif

        private void SaveGame()
        {
            var saveData = new SaveData
            {
                LastName = _state.Player.Name,
                TopScores = _state.TopScores,
                PreferredBallType = _state.PreferredBallType,
                PreferredEnvironmentType = _state.PreferredEnvironmentType
            };
            var @event = new SaveGameEvent(saveData);
            @event.RaisePointerEvent();
        }

        public void OnMessage(in CoreSceneLoadedEvent data)
        {
            var sceneSetup = _state.SceneSetup;
            sceneSetup.Camera = data.Camera;
            sceneSetup.CanvasRoot = data.RootCanvas;

            ClientCommutator<UIPopupActionEvent>.Default +=
                new UIPopupActionEvent(PopupType.NameSelection, PopupAction.Show);
        }

        public void OnRefMessage(ref SaveGameEvent data)
        {
            var dbManager = _state.DbManager;
            var key = dbManager.SaveKey;
            PongSaveUtils.SaveState(ref data.SaveData, key);
        }

        private void ResetupMenu(PopupType type)
        {
            var camera = _state.SceneSetup.Camera;
            if (camera)
            {
                var setup = _state.DbManager.Environment.MainMenuCamera;
                setup.PlaceCamera(camera);
                setup.Apply(camera);
            }

            ClientCommutator<UIPopupActionEvent>.Default += new UIPopupActionEvent(PopupAction.Clear);
            ClientCommutator<UIPopupActionEvent>.Default +=
                new UIPopupActionEvent(type, PopupAction.Show, true);
        }

        public void OnMessage(in PlayerNameSelectedUIEvent data)
        {
            if (string.IsNullOrEmpty(_state.Player.Name))
                ResetupMenu(PopupType.MainMenu);
            else
                ClientCommutator<UIPopupActionEvent>.Default += new UIPopupActionEvent(PopupAction.Back);

            _state.Player.Name = data.Name;
            if (!string.Equals(_state.Player.Name, _state.Player.PreviousName))
                SaveGame();
        }

        public void OnMessage(in UIBallSelectedEvent data)
        {
            if (_state.PreferredBallType == data.Type)
                return;
            _state.PreferredBallType = data.Type;
            SaveGame();
        }

        public void OnMessage(in UIEnvironmentSelectedEvent data)
        {
            if (_state.PreferredEnvironmentType == data.Type)
                return;
            _state.PreferredEnvironmentType = data.Type;
            SaveGame();
        }

        public void OnMessage(in DeleteSaveFileEvent data)
        {
            _state.Player.PreviousName = string.Empty;
            _state.TopScores = Array.Empty<ScoreResultEntry>();
            _state.PreferredBallType = 0;
            _state.PreferredEnvironmentType = 0;
            PongSaveUtils.ClearSave();
        }

        public void OnMessage(in NetworkConnectionRefreshEvent data)
        {
            if (!data.HasInternetConnection)
            {
                ResetupMenu(PopupType.NoInternetConnection);
                return;
            }

            if (_state.Stage != GameStage.NetworkScan)
                return;

            var startObserve = new NetworkObserverStartEvent();
            startObserve.RaiseEvent();
        }

        public void OnMessage(in UIBattleTypeSelectedEvent data)
        {
            if (_state.Stage != GameStage.Menu || data.Type == BattleType.Server)
                return;
            if (data.Type == BattleType.Multiplayer)
            {
                _state.Stage = GameStage.NetworkScan;
                var startScan = new NetworkScanStartEvent();
                startScan.RaiseEvent();

                ResetupMenu(PopupType.ScanningNetwork);
                return;
            }

            ResetupMenu(PopupType.InGameUI);

            _state.Stage = GameStage.Match;
            var startBattle = new BattleStartEvent(data.Type);
            startBattle.RaiseEvent();
        }

        public void OnMessage(in UINoInternetConnectionTimeoutEvent data)
        {
            if (_state.Stage != GameStage.NetworkScan)
                return;
            _state.Stage = GameStage.Menu;
            ResetupMenu(PopupType.MainMenu);
        }

        public void OnMessage(in UIWaitingForPlayersCancelEvent data)
        {
            if (_state.Stage != GameStage.WaitingForPlayers)
                return;
            _state.Stage = GameStage.Menu;
            var stopServer = new NetworkServerStopEvent();
            stopServer.RaiseEvent();
            ResetupMenu(PopupType.MainMenu);
        }

        public void OnMessage(in NetworkScanTimeoutEvent data)
        {
            if (_state.Stage != GameStage.NetworkScan)
                return;
            _state.Stage = GameStage.WaitingForPlayers;

            var startServer = new NetworkServerStartEvent();
            startServer.RaiseEvent();

            ResetupMenu(PopupType.WaitingPlayers);
        }

        public void OnMessage(in NetworkScanErrorEvent data)
        {
            if (_state.Stage != GameStage.NetworkScan)
                return;
            ResetupMenu(PopupType.InternalNetworkError);
        }

        public void OnMessage(in NetworkConnectedToServerEvent data)
        {
            if (_state.Stage != GameStage.NetworkScan)
                return;
            _state.Stage = GameStage.Match;

            var disposeObserver = new NetworkObserverDisposeEvent();
            disposeObserver.RaiseEvent();

            var startClient = new NetworkClientStartEvent();
            startClient.RaiseEvent();
        }

        public void OnMessage(in UIShowInGameSettingsEvent data)
        {
            if (_state.Stage != GameStage.Match)
            {
                ResetupMenu(PopupType.MainMenu);
                return;
            }

            ClientCommutator<UIPopupActionEvent>.Default +=
                new UIPopupActionEvent(PopupType.InGameMenu, PopupAction.Show, true, false);
        }

        public void OnMessage(in UINetworkErrorCloseEvent data)
        {
            if (_state.Stage != GameStage.NetworkScan)
                return;
            _state.Stage = GameStage.Menu;
            ResetupMenu(PopupType.MainMenu);
        }

        public void OnMessage(in UIInGameExitEvent data)
        {
            if (_state.Stage == GameStage.Menu)
                return;
            _state.Stage = GameStage.Menu;
            ResetupMenu(PopupType.MainMenu);
        }

        public void OnMessage(in NetworkDisconnectEvent data)
        {
            if (_state.Stage == GameStage.Menu)
                return;
            _state.Stage = GameStage.Menu;
            ResetupMenu(PopupType.MainMenu);
        }

        public void OnMessage(in NetworkCommandEvent<MatchConnectionResponse> data)
        {
            if (_state.Stage == GameStage.Match)
            {
                data.Command.Dispose();
                return;
            }

            var connectedToServer = new NetworkConnectedToServerEvent();
            connectedToServer.RaiseEvent();

            _state.Stage = GameStage.Match;
            ResetupMenu(PopupType.InGameUI);

            var battleStart = new BattleClientStartEvent(data.Command);
            battleStart.RaiseEvent();

            data.Command.Dispose();
        }

        public void OnMessage(in NetworkCommandEvent<MatchConnectionRequest> data)
        {
            if (_state.Stage == GameStage.Match)
            {
                var connect = new BattlePlayerJoinEvent(data.Connection);
                connect.RaiseEvent();

                data.Command.Dispose();
                return;
            }

            ResetupMenu(PopupType.InGameUI);

            _state.Stage = GameStage.Match;
            var battleStart = new BattleServerStartEvent(data.Connection, data.Command);
            battleStart.RaiseEvent();

            data.Command.Dispose();
        }

        private static int FindMaxScoreId(ScoreResultEntry[] scores, int score)
        {
            for (int i = 0; i < scores.Length; i++)
            {
                if (scores[i].Score < score)
                    return i;
            }

            return -1;
        }

        private static bool InsertNewScore(ScoreResultEntry[] scores, ref ScoreResultEntry score)
        {
            int maxId = FindMaxScoreId(scores, score.Score);
            if (maxId < 0)
                return false;
            if (maxId < scores.Length - 1)
            {
                for (int id = scores.Length - 1; id > maxId; id--)
                    scores[id] = scores[id - 1];
            }

            scores[maxId] = score;
            return true;
        }

        public void OnMessage(in BattleRoundOverEvent data)
        {
            if (data.Score == 0)
                return;
            var score = new ScoreResultEntry
            {
                Name = _state.Player.Name,
                Score = data.Score,
                TimeStamp = DateTime.Now.ToBinary()
            };
            var scores = _state.TopScores;
            if (_state.TopScores.Length < _state.DbManager.TopScoreCapacity)
            {
                var newScores = new ScoreResultEntry[scores.Length + 1];
                for (int i = 0; i < scores.Length; i++)
                    newScores[i] = scores[i];
                InsertNewScore(newScores, ref score);
                _state.TopScores = newScores;
                SaveGame();
                return;
            }

            if (InsertNewScore(scores, ref score))
                SaveGame();
        }
    }
}
