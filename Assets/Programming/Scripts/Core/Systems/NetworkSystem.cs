﻿using CrazyRam.Core.Framework;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Network;

namespace Pong.Systems
{
    public class NetworkSystem : IGameSystem,
        IMessageReceiver<NetworkScanStartEvent>,
        IMessageReceiver<NetworkServerStopEvent>,
        IMessageReceiver<NetworkServerStartEvent>,
        IMessageReceiver<NetworkClientStartEvent>,
        IMessageReceiver<NetworkClientStopEvent>
    {
        private GlobalState _state;

        private IGameSystem _activeSystem;

        public void Init(ILevelState state)
        {
            _state = state as GlobalState;

            ClientDispatcher<NetworkScanStartEvent>.Default += this;
            ClientDispatcher<NetworkServerStopEvent>.Default += this;
            ClientDispatcher<NetworkClientStopEvent>.Default += this;
            ClientDispatcher<NetworkServerStartEvent>.Default += this;
            ClientDispatcher<NetworkClientStartEvent>.Default += this;
        }

        public void Dispose()
        {
            CleanupNetwork();

            ClientDispatcher<NetworkScanStartEvent>.Default -= this;
            ClientDispatcher<NetworkServerStopEvent>.Default -= this;
            ClientDispatcher<NetworkClientStopEvent>.Default -= this;
            ClientDispatcher<NetworkServerStartEvent>.Default -= this;
            ClientDispatcher<NetworkClientStartEvent>.Default -= this;
        }

        private void DisposeActiveSystem()
        {
            _activeSystem?.Dispose();
            _activeSystem = null;
        }

        private void CleanupNetwork()
        {
            DisposeActiveSystem();
            var networkState = _state.NetworkState;

            networkState.Clear();
            if (networkState.Driver.IsCreated)
                networkState.Driver.Dispose();
        }

        private bool CheckDisconnect()
        {
            var disconnect = _state.NetworkState.Disconnect;
            return disconnect.IsCreated && disconnect.Length > 0;
        }

        public void Update()
        {
            if (_activeSystem == null)
                return;
            if (CheckDisconnect())
            {
                var disconnet = new NetworkDisconnectEvent();
                disconnet.RaiseEvent();
                return;
            }

            _activeSystem.Update();
        }

        public void LateUpdate()
        {
            _activeSystem?.LateUpdate();
        }

        public void PhysicsUpdate()
        {
            _activeSystem?.PhysicsUpdate();
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
            _activeSystem?.OnDrawGizmos();
        }
#endif

        public void OnMessage(in NetworkScanStartEvent data)
        {
            DisposeActiveSystem();
            _activeSystem = new NetworkObserverSystem();
            _activeSystem.Init(_state);
        }

        public void OnMessage(in NetworkServerStartEvent data)
        {
            DisposeActiveSystem();

            _activeSystem = new NetworkServerSystem();
            _activeSystem.Init(_state);
        }

        public void OnMessage(in NetworkServerStopEvent data)
        {
            CleanupNetwork();
            SerializationHelper.ClearSerializedCommandQueues();
        }

        public void OnMessage(in NetworkClientStopEvent data)
        {
            CleanupNetwork();
            SerializationHelper.ClearSerializedCommandQueues();
        }

        public void OnMessage(in NetworkClientStartEvent data)
        {
            DisposeActiveSystem();
            _activeSystem = new NetworkClientSystem();
            _activeSystem.Init(_state);
        }
    }
}
