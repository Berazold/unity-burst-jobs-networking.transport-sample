﻿using System;
using System.Runtime.CompilerServices;
using CrazyRam.Core;
using CrazyRam.Core.Framework;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Network;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Networking.Transport;
using UnityEngine;

namespace Pong.Systems
{
    public class NetworkServerSystem : IGameSystem, IMessageReceiver<NetworkServerStopAccepClientsEvent>
    {
        private bool _refreshConnections;

        [BurstCompile]
        private struct ServerConnectionJob : IJob
        {
            public NetworkDriver Driver;

            public NativeList<NetworkConnection> Connections;

            public void Execute()
            {
                for (int i = 0; i < Connections.Length; i++)
                {
                    if (!Connections[i].IsCreated)
                    {
                        Connections.RemoveAtSwapBack(i);
                        i--;
                    }
                }

                NetworkConnection conn;
                while ((conn = Driver.Accept()) != default)
                {
                    Connections.Add(conn);
#if UNITY_EDITOR
                    Debug.Log("Client connected");
#endif
                }
            }
        }

        [BurstCompile]
        public struct ServerReceiveJob : IJobParallelForDefer
        {
            public NetworkDriver.Concurrent Driver;

            [NativeDisableContainerSafetyRestriction]
            public NativeList<byte>.ParallelWriter Disconnect;

            [NativeDisableContainerSafetyRestriction]
            public NativeQueue<TransportMessage>.ParallelWriter InputQueue;

            [ReadOnly]
            public NativeArray<NetworkConnection> Connections;

            public byte LittleEndianServer;

            public void Execute(int index)
            {
                if (!Connections.IsCreated || index >= Connections.Length)
                    return;

                var connection = Connections[index];
                if (!connection.IsCreated)
                {
                    Disconnect.AddNoResize(0);
                    return;
                }

                NetworkEvent.Type type;
                while ((type = Driver.PopEventForConnection(connection, out var reader)) !=
                       NetworkEvent.Type.Empty)
                {
                    switch (type)
                    {
                        case NetworkEvent.Type.Data:
                            int attempts = 0;
                            const int maxAttempts = 20;
                            while (reader.GetBytesRead() < reader.Length && attempts < maxAttempts)
                            {
                                if (!NetworkHelper.ReadMessage(ref reader, ref connection, LittleEndianServer,
                                    out var message))
                                    break;
                                InputQueue.Enqueue(message);
                                attempts++;
                            }

                            break;
                        case NetworkEvent.Type.Disconnect:
                            Disconnect.AddNoResize(0);
                            break;
                    }
                }
            }
        }

        [BurstCompile]
        private struct ServerSendJob : IJob
        {
            public NetworkDriver Driver;

            [NativeDisableContainerSafetyRestriction]
            public NativeList<byte> Disconnect;

            [NativeDisableContainerSafetyRestriction]
            public NativeQueue<TransportMessage> OutputQueue;

            public byte LittleEndianServer;

            public unsafe void Execute()
            {
                if (Disconnect.IsCreated && Disconnect.Length > 0)
                    return;
                if (!OutputQueue.IsCreated || OutputQueue.Count == 0)
                    return;

                while (OutputQueue.Count > 0)
                {
                    var message = OutputQueue.Dequeue();
                    var connection = message.Connection;
                    if (!connection.IsCreated)
                    {
                        UnsafeUtility.Free((void*) message.Data, Allocator.TempJob);
                        Disconnect.Add(0);
                        continue;
                    }

                    var writer = Driver.BeginSend(connection);
                    NetworkHelper.WriteMessage(ref writer, ref message, LittleEndianServer);
                    Driver.EndSend(writer);
                }
            }
        }

        private GlobalState _state;

        private Option<JobHandle> _updateHandle;

        public void Init(ILevelState state)
        {
            _state = (GlobalState) state;
            var networkState = _state.NetworkState;
            networkState.Clear();
            if (networkState.Driver.IsCreated)
                networkState.Driver.Dispose();
            networkState.Driver = NetworkDriver.Create();

            var endpoint = NetworkEndPoint.AnyIpv4;
            endpoint.Port = _state.DbManager.NetworkConfig.Port;

#if UNITY_EDITOR
            Debug.Log($"Start listening {endpoint.Port}");
#endif

            int bindResult = -1;
            try
            {
                bindResult = networkState.Driver.Bind(endpoint);
            }
            catch (Exception)
            {
                // ignored
            }

            if (bindResult == 0)
                networkState.Driver.Listen();
            else
            {
                Debug.Log($"Can't setup server {bindResult}");
                var disconnect = new NetworkDisconnectEvent();
                disconnect.RaiseEvent();
            }

            _refreshConnections = true;

            ClientDispatcher<NetworkServerStopAccepClientsEvent>.Default += this;
        }

        public void Dispose()
        {
            CompleteHandle();
            _state.NetworkState.DisconnectPlayers();
            ClientDispatcher<NetworkServerStopAccepClientsEvent>.Default -= this;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void CompleteHandle()
        {
            var (hasValue, handle) = _updateHandle;
            if (hasValue && !handle.IsCompleted)
                handle.Complete();
        }

        public void Update()
        {
            CompleteHandle();

            var networkState = _state.NetworkState;
            networkState.SwapInOutBuffers();


            var driver = networkState.Driver;
            var handle = driver.ScheduleUpdate();

            if (_refreshConnections)
            {
                var connectJob = new ServerConnectionJob
                {
                    Driver = driver,
                    Connections = networkState.ActiveConnections
                };
                handle = connectJob.Schedule(handle);
            }

            var receiveJob = new ServerReceiveJob
            {
                Driver = driver.ToConcurrent(),

                Connections = networkState.ActiveConnections.AsDeferredJobArray(),
                Disconnect = networkState.Disconnect.AsParallelWriter(),
                InputQueue = networkState.InactiveInput.AsParallelWriter(),
                LittleEndianServer = networkState.IsLittleEndian
            };
            handle = receiveJob.Schedule(networkState.ActiveConnections, 1, handle);

            if (networkState.InactiveOutput.IsCreated && networkState.InactiveOutput.Count > 0)
            {
                var sendJob = new ServerSendJob
                {
                    Driver = driver,
                    Disconnect = networkState.Disconnect,
                    OutputQueue = networkState.InactiveOutput,
                    LittleEndianServer = networkState.IsLittleEndian
                };
                handle = sendJob.Schedule(handle);
            }

            _updateHandle = handle.AsOption();

            networkState.ActiveOutput.Clear();
            if (networkState.ActiveInput.Count > 0)
                SerializationHelper.ParseIncomingMessages(_state, networkState.ActiveInput);
            networkState.ActiveInput.Clear();
        }

        public void LateUpdate()
        {
        }

        public void PhysicsUpdate()
        {
        }

        public void OnDrawGizmos()
        {
        }

        public void OnMessage(in NetworkServerStopAccepClientsEvent data)
        {
            _refreshConnections = false;
        }
    }
}
