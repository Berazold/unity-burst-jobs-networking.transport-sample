﻿using System.Runtime.CompilerServices;
using CrazyRam.Core;
using CrazyRam.Core.Framework;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Network;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Networking.Transport;

namespace Pong.Systems
{
    public class NetworkClientSystem : IGameSystem
    {
        [BurstCompile]
        private struct ClientUpdateJob : IJob
        {
            public NetworkDriver Driver;

            [NativeDisableContainerSafetyRestriction]
            public NativeList<byte> Disconnect;

            [NativeDisableContainerSafetyRestriction]
            public NativeList<NetworkConnection> ServerConn;

            [NativeDisableContainerSafetyRestriction]
            public NativeQueue<TransportMessage> InputQueue;

            public byte LittleEndianClient;

            public void Execute()
            {
                if (!ServerConn.IsCreated || ServerConn.Length == 0 || !ServerConn[0].IsCreated)
                    return;
                if (Disconnect.IsCreated && Disconnect.Length > 0)
                    return;
                var connection = ServerConn[0];
                if (!connection.IsCreated)
                {
                    Disconnect.Add(0);
                    return;
                }

                NetworkEvent.Type type;
                while ((type = Driver.PopEventForConnection(connection, out var reader)) !=
                       NetworkEvent.Type.Empty)
                {
                    switch (type)
                    {
                        case NetworkEvent.Type.Data:
                            int attempts = 0;
                            const int maxAttempts = 20;
                            while (reader.GetBytesRead() < reader.Length && attempts < maxAttempts)
                            {
                                if (!NetworkHelper.ReadMessage(ref reader, ref connection, LittleEndianClient,
                                    out var message))
                                    break;
                                InputQueue.Enqueue(message);
                                attempts++;
                            }

                            break;
                        case NetworkEvent.Type.Disconnect:
                            Disconnect.AddNoResize(0);
                            break;
                    }
                }
            }
        }

        private struct ClientSendJob : IJob
        {
            public NetworkDriver Driver;

            [NativeDisableContainerSafetyRestriction]
            public NativeList<byte> Disconnect;

            [NativeDisableContainerSafetyRestriction]
            public NativeList<NetworkConnection> ServerConn;

            [NativeDisableContainerSafetyRestriction]
            public NativeQueue<TransportMessage> OutputQueue;

            public byte LittleEndianClient;

            public void Execute()
            {
                if (!ServerConn.IsCreated || !ServerConn[0].IsCreated)
                    return;
                if (Disconnect.IsCreated && Disconnect.Length > 0)
                    return;
                if (!OutputQueue.IsCreated || OutputQueue.Count == 0)
                    return;
                var connection = ServerConn[0];

                while (OutputQueue.Count > 0)
                {
                    var message = OutputQueue.Dequeue();

                    var writer = Driver.BeginSend(connection);
                    NetworkHelper.WriteMessage(ref writer, ref message, LittleEndianClient);
                    Driver.EndSend(writer);
                }
            }
        }

        private GlobalState _state;

        private Option<JobHandle> _updateHandle;

        public void Init(ILevelState state)
        {
            _state = (GlobalState) state;
            var networkState = _state.NetworkState;
            var activeConnections = networkState.ActiveConnections;
            if (!networkState.Driver.IsCreated || activeConnections.Length == 0 || activeConnections.Length > 1)
            {
                var disconnect = new NetworkDisconnectEvent();
                disconnect.RaiseEvent();
                return;
            }

            networkState.ActiveInput.Clear();
            networkState.InactiveInput.Clear();

            networkState.ActiveOutput.Clear();
            networkState.InactiveOutput.Clear();

            networkState.Disconnect.Clear();
        }

        public void Dispose()
        {
            CompleteHandle();
            _state.NetworkState.DisconnectPlayers();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void CompleteHandle()
        {
            var (hasValue, handle) = _updateHandle;
            if (hasValue && !handle.IsCompleted)
                handle.Complete();
        }

        public void Update()
        {
            CompleteHandle();

            var networkState = _state.NetworkState;
            networkState.SwapInOutBuffers();

            var driver = networkState.Driver;
            var handle = driver.ScheduleUpdate();

            var readJob = new ClientUpdateJob
            {
                Driver = driver,
                ServerConn = networkState.ActiveConnections,
                Disconnect = networkState.Disconnect,
                InputQueue = networkState.InactiveInput,
                LittleEndianClient = networkState.IsLittleEndian
            };
            handle = readJob.Schedule(handle);

            if (networkState.InactiveOutput.IsCreated && networkState.InactiveOutput.Count > 0)
            {
                var sendJob = new ClientSendJob
                {
                    Driver = driver,
                    ServerConn = networkState.ActiveConnections,
                    Disconnect = networkState.Disconnect,
                    OutputQueue = networkState.InactiveOutput,
                    LittleEndianClient = networkState.IsLittleEndian
                };
                handle = sendJob.Schedule(handle);
            }

            _updateHandle = handle.AsOption();

            if (networkState.ActiveInput.Count > 0)
                SerializationHelper.ParseIncomingMessages(_state, networkState.ActiveInput);
            networkState.ActiveInput.Clear();
            networkState.ActiveOutput.Clear();
        }

        public void LateUpdate()
        {
        }

        public void PhysicsUpdate()
        {
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
        }
#endif
    }
}
