﻿using CrazyRam.Core.Framework;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Network;
using Pong.Utils.Serialization;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Networking.Transport;
using UnityEngine;

namespace Pong.Systems
{
    public class NetworkObserverSystem : IGameSystem,
        IMessageReceiver<NetworkObserverStartEvent>,
        IMessageReceiver<NetworkObserverDisposeEvent>
    {
        private GlobalState _state;

        private NativeList<NetworkConnection> _scanConnections;

        private NativeList<NetworkConnection> _pendingConnections;

        private float _startObserveTime;

        //Unity.Networking.Transport private constant
        private const int RawDataLength = 16; // Maximum space needed to hold a IPv6 Address

        //Unity.Networking.Transport private constant
        private const int RawLength = RawDataLength + 3; // SizeOf<Baselib_NetworkAddress>

        public void Init(ILevelState state)
        {
            _state = (GlobalState) state;
            if (_state.NetworkState.Driver.IsCreated)
                _state.NetworkState.Driver.Dispose();
            _state.NetworkState.Clear();
            _state.NetworkState.Driver = NetworkDriver.Create();

            _scanConnections = new NativeList<NetworkConnection>(byte.MaxValue, Allocator.Persistent);
            _pendingConnections = new NativeList<NetworkConnection>(2, Allocator.Persistent);
            NetworkHelper.RefreshInternetConnection();

            _startObserveTime = -1;

            ClientDispatcher<NetworkObserverStartEvent>.Default += this;
            ClientDispatcher<NetworkObserverDisposeEvent>.Default += this;
        }

        public void Dispose()
        {
            if (_scanConnections.IsCreated)
                _scanConnections.Dispose();
            if (_pendingConnections.IsCreated)
                _pendingConnections.Dispose();

            ClientDispatcher<NetworkObserverStartEvent>.Default -= this;
            ClientDispatcher<NetworkObserverDisposeEvent>.Default -= this;
        }

        public void Update()
        {
            if (_startObserveTime < 0)
                return;

            if (CheckObserveTimeout())
            {
                OnObserverTimeout();
                return;
            }

            if (_scanConnections.IsCreated && _scanConnections.Length > 0 && _pendingConnections.IsCreated)
                RefreshConnection();
            else
                RaiseErrorEvent();
        }

        public void LateUpdate()
        {
        }

        public void PhysicsUpdate()
        {
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
        }
#endif

        private void Observe()
        {
            var localAddresses = NetworkHelper.CollectLocalLanAddresses(Allocator.Temp);
            Scan(localAddresses);
        }

        private unsafe void Scan(NativeList<NetworkEndPoint.RawNetworkAddress> localAddresses)
        {
            if (!localAddresses.IsCreated || localAddresses.Length == 0)
            {
                RaiseErrorEvent();
                return;
            }

            var config = _state.DbManager.NetworkConfig;
            ushort port = config.Port;

            if (ByteIO.IsLittleEndian)
                port = ByteIO.ByteSwap(port);

            var driver = _state.NetworkState.Driver;

            for (int addressId = 0; addressId < localAddresses.Length; addressId++)
            {
                for (int i = 0; i <= byte.MaxValue; i++)
                {
                    var address = localAddresses[addressId];
                    if (i == address.ipv4_bytes[3])
                        continue;

                    var endPoint = new NetworkEndPoint
                    {
                        Family = NetworkFamily.Ipv4,
                        RawPort = port,
                        length = RawLength,
                    };
                    address.ipv4_bytes[3] = (byte) i;
                    UnsafeUtility.MemCpy(endPoint.rawNetworkAddress.ipv4_bytes, address.ipv4_bytes, sizeof(uint));

                    _scanConnections.Add(driver.Connect(endPoint));
                }
            }

            localAddresses.Dispose();
        }

        private static void RemoveClient(NetworkConnection conn, NativeList<NetworkConnection> connections)
        {
            if (!connections.IsCreated)
                return;
            for (int i = 0; i < connections.Length; i++)
                if (connections[i] == conn)
                    connections.RemoveAtSwapBack(i);
        }

        private static void SafePushClient(NetworkConnection conn, ref NativeList<NetworkConnection> connections)
        {
            if (!connections.IsCreated)
                return;
            for (int i = 0; i < connections.Length; i++)
            {
                if (connections[i] == conn)
                    return;
            }

            connections.Add(conn);
        }

        private void ConsumeNetworkEvents(NetworkDriver driver)
        {
            if (!driver.IsCreated)
                return;
            bool awaitServer = true;
            var inputQueue = _state.NetworkState.ActiveInput;
            var outputQueue = _state.NetworkState.ActiveOutput;
            NetworkEvent.Type cmd;
            while ((cmd = driver.PopEvent(out var conn, out var reader)) != NetworkEvent.Type.Empty)
            {
                switch (cmd)
                {
                    case NetworkEvent.Type.Data:
                        if (!awaitServer)
                            continue;
                        int attempts = 0;
                        const int maxAttempts = 20;
                        while (reader.GetBytesRead() < reader.Length && attempts < maxAttempts)
                        {
                            attempts++;

                            if (!NetworkHelper.ReadMessage(ref reader, ref conn, _state.NetworkState.IsLittleEndian,
                                out var message))
                                break;

                            if (message.CommandType != (byte) CommandType.MatchConnectionResponse)
                            {
                                unsafe
                                {
                                    if ((void*) message.Data != null)
                                        UnsafeUtility.Free((void*) message.Data, Allocator.TempJob);
                                }

                                continue;
                            }


                            RemoveClient(conn, _pendingConnections);

                            var activeConnections = _state.NetworkState.ActiveConnections;
                            _state.NetworkState.DisconnectPlayers();
                            NetworkState.DisconnecConnectedClients(driver, activeConnections);
                            activeConnections.Clear();
                            activeConnections.Add(conn);

                            inputQueue.Enqueue(message);
                            awaitServer = false;
                        }

                        break;
                    case NetworkEvent.Type.Connect:
#if UNITY_EDITOR
                    {
                        var ip = driver.RemoteEndPoint(conn);
                        Debug.Log($"Disconnect {ip.Address}");
                    }
#endif
                        RemoveClient(conn, _scanConnections);
                        SafePushClient(conn, ref _pendingConnections);
                        _startObserveTime = Time.realtimeSinceStartup;

                        var request = new MatchConnectionRequest(_state.Player.Name, (uint) _state.MaxScore);
                        var serializedMessage =
                            SerializationHelper.SerializeCommand(ref request, _state.NetworkState.Provider);
                        var transportMessage = new TransportMessage(conn, serializedMessage.Data, 0,
                            serializedMessage.Size, _state.NetworkState.IsLittleEndian, serializedMessage.CommandType);

                        outputQueue.Enqueue(transportMessage);
                        break;
                    case NetworkEvent.Type.Disconnect:
#if UNITY_EDITOR
                    {
                        var ip = driver.RemoteEndPoint(conn);
                        Debug.Log($"Disconnect {ip.Address}");
                    }
#endif

                        RemoveClient(conn, _scanConnections);
                        RemoveClient(conn, _pendingConnections);
                        break;
                    case NetworkEvent.Type.Empty:
                        break;
                }
            }

            if (outputQueue.Count > 0)
            {
                var littleEndian = _state.NetworkState.IsLittleEndian;
                while (outputQueue.Count > 0)
                {
                    var transportMessage = outputQueue.Dequeue();
                    var writer = driver.BeginSend(transportMessage.Connection);
                    NetworkHelper.WriteMessage(ref writer, ref transportMessage, littleEndian);
                    driver.EndSend(writer);
                }
            }

            outputQueue.Clear();

            if (inputQueue.Count > 0)
                SerializationHelper.ParseIncomingMessages(_state, inputQueue);
            inputQueue.Clear();
        }

        private void RefreshConnection()
        {
            var driver = _state.NetworkState.Driver;
            driver.ScheduleUpdate().Complete();
            ConsumeNetworkEvents(driver);
        }

        private void DropExtraConnections(NativeList<NetworkConnection> connections, NetworkConnection active)
        {
            if (!connections.IsCreated || connections.Length == 0)
                return;
            var driver = _state.NetworkState.Driver;
            if (!driver.IsCreated)
                return;
            for (int i = 0; i < connections.Length; i++)
            {
                var connection = connections[i];
                if (connection == active ||
                    driver.GetConnectionState(connection) == NetworkConnection.State.Disconnected)
                    continue;
                driver.Disconnect(connection);
            }

            connections.Clear();
        }

        private bool CheckObserveTimeout()
        {
            var config = _state.DbManager.NetworkConfig;
            return _startObserveTime > 0 &&
                   Time.realtimeSinceStartup - _startObserveTime >= config.ServerConnectionTimeout;
        }

        private void OnObserverTimeout()
        {
            DropExtraConnections(_scanConnections, default);
            DropExtraConnections(_pendingConnections, default);
            if (_state.NetworkState.Driver.IsCreated)
            {
                _state.NetworkState.Driver.Dispose();
                _state.NetworkState.Driver = default;
            }

            var timeout = new NetworkScanTimeoutEvent();
            timeout.RaiseEvent();
        }

        private static void RaiseErrorEvent()
        {
            var scanError = new NetworkScanErrorEvent();
            scanError.RaiseEvent();
        }

        public void OnMessage(in NetworkObserverStartEvent data)
        {
            if (_startObserveTime > 0)
                return;
            _startObserveTime = Time.realtimeSinceStartup;
            Observe();
        }

        public void OnMessage(in NetworkObserverDisposeEvent data)
        {
            DropExtraConnections(_scanConnections, default);
            DropExtraConnections(_pendingConnections, default);
        }
    }
}
