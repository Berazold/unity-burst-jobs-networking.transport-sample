﻿using System;
using System.Collections.Generic;
using CrazyRam.Core;
using CrazyRam.Core.Framework;
using CrazyRam.Core.MessageBus;
using CrazyRam.Core.Utils;
using Pong.Events;
using Pong.UI;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Pong.Systems
{
    public class UISystem : IGameSystem
    {
        private Dictionary<PopupType, UIPopupBase> _popups;

        private Stack<PopupType> _history;

        private UIDb _manager;

        private GlobalState _state;

        public void Init(ILevelState state)
        {
            _popups = new Dictionary<PopupType, UIPopupBase>();
            _history = new Stack<PopupType>();
            _state = state as GlobalState;
            if (_state == null)
                throw new Exception("State is invalid!");
            _manager = _state.DbManager.UIDb;
        }

        public void Dispose()
        {
        }

        public void Update()
        {
        }

        public void LateUpdate()
        {
            HandleActions();

            foreach (var popup in _popups)
            {
                if (popup.Value && popup.Value.Active)
                    popup.Value.Refresh();
            }
        }

        public void PhysicsUpdate()
        {
        }

        private void HandleActions()
        {
            ref var channel = ref ClientCommutator<UIPopupActionEvent>.Default;
            if (!channel.HasNext)
                return;
            var root = _state.SceneSetup.CanvasRoot;
            if (!root)
            {
                channel.ResetCounter();
                return;
            }

            int readId = 0;
            Option<UIPopupActionEvent> result;
            while ((result = channel.DrainQueue(ref readId)).HasValue)
            {
                var command = result.Value;
                switch (command.Action)
                {
                    case PopupAction.Show:
                        ShowPopup(command.Type);
                        if (command.GrabFocus == ByteAction.On && _history.Count > 0)
                            HidePopup(_history.Peek());
                        if (command.StoreState == ByteAction.On)
                            _history.Push(command.Type);
                        break;
                    case PopupAction.Hide:
                        HidePopup(command.Type);
                        break;
                    case PopupAction.Clear:
                        Clear();
                        break;
                    case PopupAction.Back:
                        if (_history.Count < 2)
                            break;
                        var active = _history.Pop();
                        HidePopup(active);
                        ShowPopup(_history.Peek());
                        break;
                }
            }

            channel.ResetCounter();
        }

        private unsafe void Clear()
        {
            var toRemove = stackalloc byte[_popups.Count];
            int removeCount = 0;
            foreach (var popup in _popups)
            {
                if (!popup.Value || (popup.Value.Active && !popup.Value.Reusable))
                    toRemove[removeCount++] = (byte) popup.Key;
                if (popup.Value && popup.Value.Active)
                    HidePopupInternal(popup.Value, false);
            }

            for (int i = 0; i < removeCount; i++)
                _popups.Remove((PopupType) toRemove[i]);
            _history.Clear();
        }

        private UIPopupBase FetchPopup(PopupType type)
        {
            if (_popups.TryGetValue(type, out var popup) && popup)
                return popup;
            var root = _state.SceneSetup.CanvasRoot;
            var reference = _manager.Find((int) type);
            if (!reference || !root)
                return null;
            popup = Object.Instantiate(reference);
            popup.Bind(root);
            _popups[type] = popup;
            return popup;
        }

        private void ShowPopup(PopupType type)
        {
            var popup = FetchPopup(type);
            if (!popup || popup.Active)
                return;

            popup.InitIndex((int) type);
            popup.Init(_state);
            popup.Show();
        }

        private void HidePopup(PopupType type)
        {
            if (_popups.TryGetValue(type, out var popup) && popup && popup.Active)
                HidePopupInternal(popup);
        }

        private void HidePopupInternal(UIPopupBase popup, bool shouldRemove = true)
        {
            popup.ReleaseResources();
            popup.Hide();
            if (!popup.Reusable && shouldRemove)
                _popups.Remove(popup.Type);
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
        }
#endif
    }
}
