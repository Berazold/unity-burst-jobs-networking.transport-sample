﻿using UnityEngine;

namespace Pong.Events
{
    public readonly struct CoreSceneLoadedEvent
    {
        public readonly Camera Camera;

        public readonly RectTransform RootCanvas;

        public CoreSceneLoadedEvent(Camera camera, RectTransform rootCanvas)
        {
            Camera = camera;
            RootCanvas = rootCanvas;
        }
    }
}
