﻿namespace Pong.Events
{
    public readonly struct NetworkConnectionRefreshEvent
    {
        public readonly bool HasInternetConnection;

        public NetworkConnectionRefreshEvent(bool hasInternetConnection)
        {
            HasInternetConnection = hasInternetConnection;
        }
    }
}
