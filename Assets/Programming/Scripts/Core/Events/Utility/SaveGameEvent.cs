﻿namespace Pong.Events
{
    public struct SaveGameEvent
    {
        public SaveData SaveData;

        public SaveGameEvent(SaveData saveData)
        {
            SaveData = saveData;
        }
    }
}
