﻿namespace Pong.Events
{
    public readonly struct BattleStartEvent
    {
        public readonly BattleType Type;

        public BattleStartEvent(BattleType type)
        {
            Type = type;
        }
    }
}
