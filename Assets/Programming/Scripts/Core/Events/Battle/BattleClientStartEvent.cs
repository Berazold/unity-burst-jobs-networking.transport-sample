﻿using Pong.Network;

namespace Pong.Events
{
    public readonly struct BattleClientStartEvent
    {
        public readonly MatchConnectionResponse MatchParams;

        public BattleClientStartEvent(MatchConnectionResponse matchParams)
        {
            MatchParams = matchParams;
        }
    }
}
