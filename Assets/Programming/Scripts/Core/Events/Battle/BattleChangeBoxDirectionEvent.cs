﻿using Unity.Mathematics;

namespace Pong.Events
{
    public readonly struct BattleChangeBoxDirectionEvent
    {
        public readonly int Id;

        public readonly float2 Direction;

        public BattleChangeBoxDirectionEvent(int id, float2 direction)
        {
            Id = id;
            Direction = direction;
        }
    }
}
