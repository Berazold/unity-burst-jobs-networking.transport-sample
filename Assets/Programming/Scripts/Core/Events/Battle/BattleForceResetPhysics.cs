﻿using Unity.Collections;
using Unity.Mathematics;

namespace Pong.Events
{
    public readonly struct BattleForceResetPhysics
    {
        public readonly PlatformModel[] PlatformModels;

        public readonly BallModel[] BallModels;

        public readonly NativeArray<float3> BallDirections;

        public BattleForceResetPhysics(PlatformModel[] platformModels, BallModel[] ballModels,
            NativeArray<float3> ballDirections)
        {
            PlatformModels = platformModels;
            BallModels = ballModels;
            BallDirections = ballDirections;
        }
    }
}
