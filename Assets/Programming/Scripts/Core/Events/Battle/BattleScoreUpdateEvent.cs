﻿namespace Pong.Events
{
    public readonly struct BattleScoreUpdateEvent
    {
        public readonly int Score;

        public BattleScoreUpdateEvent(int score)
        {
            Score = score;
        }
    }
}
