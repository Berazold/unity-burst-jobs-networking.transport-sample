﻿namespace Pong.Events
{
    public readonly struct BattleRoundOverEvent
    {
        public readonly int Score;

        public BattleRoundOverEvent(int score)
        {
            Score = score;
        }
    }
}
