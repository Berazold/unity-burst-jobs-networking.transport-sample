﻿using Pong.Network;
using Unity.Networking.Transport;

namespace Pong.Events
{
    public readonly struct BattleServerStartEvent
    {
        public readonly NetworkConnection Client;

        public readonly MatchConnectionRequest Request;

        public BattleServerStartEvent(NetworkConnection client, MatchConnectionRequest request)
        {
            Client = client;
            Request = request;
        }
    }
}
