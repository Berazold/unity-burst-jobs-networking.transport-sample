﻿using Unity.Networking.Transport;

namespace Pong.Events
{
    public readonly struct BattlePlayerJoinEvent
    {
        public readonly NetworkConnection Client;

        public BattlePlayerJoinEvent(NetworkConnection client)
        {
            Client = client;
        }
    }
}
