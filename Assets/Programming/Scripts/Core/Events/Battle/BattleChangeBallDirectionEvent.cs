﻿using Unity.Mathematics;

namespace Pong.Events
{
    public readonly struct BattleChangeBallDirectionEvent
    {
        public readonly int Id;

        public readonly float2 Direction;

        public BattleChangeBallDirectionEvent(int id, float2 direction)
        {
            Id = id;
            Direction = direction;
        }
    }
}
