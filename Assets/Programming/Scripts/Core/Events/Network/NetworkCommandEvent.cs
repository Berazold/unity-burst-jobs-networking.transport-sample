﻿using Pong.Network;
using Unity.Networking.Transport;

namespace Pong.Events
{
    public readonly struct NetworkCommandEvent<T> where T : struct, INetworkCommand
    {
        public readonly NetworkConnection Connection;

        public readonly uint Tick;

        public readonly T Command;

        public NetworkCommandEvent(NetworkConnection connection, uint tick, T command)
        {
            Connection = connection;
            Tick = tick;
            Command = command;
        }
    }
}
