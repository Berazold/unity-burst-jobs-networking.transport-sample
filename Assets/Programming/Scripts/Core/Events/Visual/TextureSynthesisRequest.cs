﻿namespace Pong.Visual
{
    public readonly struct TextureSynthesisRequest
    {
        public readonly TextureSynthesisBag Bag;

        public readonly TextureSynthesisPipeline Pipeline;

        public TextureSynthesisRequest(TextureSynthesisBag bag, TextureSynthesisPipeline pipeline)
        {
            Bag = bag;
            Pipeline = pipeline;
        }
    }
}
