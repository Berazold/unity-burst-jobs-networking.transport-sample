﻿namespace Pong.Visual
{
    public readonly struct UnregisterRenderExtension
    {
        public readonly IRenderingExtension Extension;

        public readonly RenderQueue Queue;

        public UnregisterRenderExtension(IRenderingExtension extension, RenderQueue queue)
        {
            Extension = extension;
            Queue = queue;
        }
    }
}
