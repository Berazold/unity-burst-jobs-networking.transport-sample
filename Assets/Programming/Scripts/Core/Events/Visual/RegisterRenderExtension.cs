﻿namespace Pong.Visual
{
    public readonly struct RegisterRenderExtension
    {
        public readonly IRenderingExtension Extension;

        public readonly RenderQueue Queue;

        public RegisterRenderExtension(IRenderingExtension extension, RenderQueue queue)
        {
            Extension = extension;
            Queue = queue;
        }
    }
}
