﻿using Pong.Systems.Battle;
using Unity.Mathematics;

namespace Pong.Events
{
    public readonly struct UIInputRequestEvent
    {
        public readonly float2 Direction;

        public readonly PlayerSide Side;

        public UIInputRequestEvent(float2 direction, PlayerSide side)
        {
            Direction = direction;
            Side = side;
        }
    }
}
