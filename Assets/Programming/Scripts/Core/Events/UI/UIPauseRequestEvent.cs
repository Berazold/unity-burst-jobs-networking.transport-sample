﻿namespace Pong.Events
{
    public readonly struct UIPauseRequestEvent
    {
        public readonly bool On;

        public UIPauseRequestEvent(bool @on)
        {
            On = @on;
        }
    }
}
