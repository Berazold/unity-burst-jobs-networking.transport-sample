﻿namespace Pong.Events
{
    public readonly struct PlayerNameSelectedUIEvent
    {
        public readonly string Name;

        public PlayerNameSelectedUIEvent(string name)
        {
            Name = name;
        }
    }
}
