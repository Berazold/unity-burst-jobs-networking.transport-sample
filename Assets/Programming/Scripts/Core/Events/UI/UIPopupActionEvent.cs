﻿using System.Runtime.InteropServices;
using CrazyRam.Core.Utils;
using Pong.UI;

namespace Pong.Events
{
    //todo use pointer size
    [StructLayout(LayoutKind.Sequential, Pack = 1, Size = sizeof(uint))]
    public readonly struct UIPopupActionEvent
    {
        public readonly PopupType Type;

        public readonly PopupAction Action;

        public readonly byte StoreState;

        /// <summary>
        /// Hide active popup
        /// </summary>
        public readonly byte GrabFocus;

        public UIPopupActionEvent(PopupType type, PopupAction action, bool storeState = false, bool grabFocus = true)
        {
            Type = type;
            Action = action;
            StoreState = storeState ? ByteAction.On : ByteAction.Off;
            GrabFocus = grabFocus ? ByteAction.On : ByteAction.Off;
        }

        public UIPopupActionEvent(PopupAction action) : this()
        {
            Action = action;
        }
    }
}
