﻿namespace Pong.Events
{
    public readonly struct UIEnvironmentSelectedEvent
    {
        public readonly int Type;

        public UIEnvironmentSelectedEvent(int type)
        {
            Type = type;
        }
    }
}
