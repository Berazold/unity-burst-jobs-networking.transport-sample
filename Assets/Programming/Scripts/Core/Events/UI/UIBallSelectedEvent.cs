﻿namespace Pong.Events
{
    public readonly struct UIBallSelectedEvent
    {
        public readonly int Type;

        public UIBallSelectedEvent(int type)
        {
            Type = type;
        }
    }
}
