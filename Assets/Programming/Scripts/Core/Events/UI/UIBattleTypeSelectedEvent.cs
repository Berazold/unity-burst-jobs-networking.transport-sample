﻿namespace Pong.Events
{
    public readonly struct UIBattleTypeSelectedEvent
    {
        public readonly BattleType Type;

        public UIBattleTypeSelectedEvent(BattleType type)
        {
            Type = type;
        }
    }
}
