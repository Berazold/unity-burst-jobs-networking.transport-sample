﻿namespace Pong.Events
{
    public readonly struct UIInGameDataSetupEvent
    {
        public readonly int Id;

        public readonly string Name;

        public readonly uint MaxScore;

        public UIInGameDataSetupEvent(int id, string name, uint maxScore)
        {
            Id = id;
            Name = name;
            MaxScore = maxScore;
        }
    }
}
