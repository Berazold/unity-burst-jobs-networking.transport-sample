using System;
using CrazyRam.Core.Framework;
using Pong.Network;
using Pong.Systems;
using Pong.Utils;
using Pong.Utils.Serialization;
using UnityEngine;
using UnityEngine.Scripting;

namespace Pong
{
    public class CoreInitializer : ResourceInitializerBase
    {
        private const int RequestedFramerate = 60;

        public override void Init()
        {
            Application.targetFrameRate = RequestedFramerate;

            var dbManager = ReferenceResolver.Instance.Pull<DbManager>();

            var key = dbManager.SaveKey;
            var saveData = PongSaveUtils.PreloadSaveData(key);

            var serializationProvider = new PongNetworkSerializer(new INetworkSerializer[]
                {
                    new MatchConnectionRequestSerializer(),
                    new MatchConnectionResponseSerializer(),
                    new InputReplicationCommandSerializer(),
                    new WorldStateReplicationCommandSerializer(),
                    new RoundResultCommandSerializer()
                }
            );
            var state = new GlobalState(ref saveData, dbManager, serializationProvider, dbManager.ScoreIncrement);

            Systems = new IGameSystem[]
            {
                new NetworkSystem(),
                new LifecycleSystem(),
                new BattleSystem(),
                new UISystem(),
            };

            for (int i = 0; i < Systems.Length; i++)
                Systems[i].Init(state);
        }

        //Do not remove. il2cpp stripping cheat!
        [Preserve]
        public void AOTCodeGenerationHelper()
        {
            var serializer = new PongNetworkSerializer(null);
            var writer = new BinaryWriter();

            var connectionRequest = new MatchConnectionRequest();
            serializer.Serialize(ref writer, ref connectionRequest);

            var connectionResponse = new MatchConnectionResponse();
            serializer.Serialize(ref writer, ref connectionResponse);

            var inputReplication = new InputReplicationCommand();
            serializer.Serialize(ref writer, ref inputReplication);

            var worldReplication = new WorldStateReplicationCommand();
            serializer.Serialize(ref writer, ref worldReplication);

            var roundResult = new RoundResultCommand();
            serializer.Serialize(ref writer, ref roundResult);

            throw new InvalidOperationException("Code generation only. Don't call it at runtime");
        }
    }
}
