﻿namespace Pong
{
    public class PlayerInfo
    {
        public string Name;

        public string PreviousName;

        public PlayerInfo(string name, string previousName)
        {
            Name = name;
            PreviousName = previousName;
        }
    }
}
