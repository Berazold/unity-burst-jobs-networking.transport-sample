﻿using System.Runtime.CompilerServices;
using Pong.Network;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Networking.Transport;

namespace Pong
{
    public class NetworkState
    {
        public byte IsLittleEndian;

        public NetworkDriver Driver;

        public INetworkSerializationProvider Provider;

        public NativeList<NetworkConnection> ActiveConnections;

        public NativeList<byte> Disconnect;

        public NativeQueue<TransportMessage> ActiveInput;

        public NativeQueue<TransportMessage> InactiveInput;

        public NativeQueue<TransportMessage> ActiveOutput;

        public NativeQueue<TransportMessage> InactiveOutput;

        public bool IsActive
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => Driver.IsCreated && ActiveConnections.IsCreated && ActiveConnections.Length > 0;
        }

        public NetworkConnection ServerConnection
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => ActiveConnections[0];
        }

        public void InitResources()
        {
            const int maxConnections = 2;

            if (!ActiveConnections.IsCreated)
                ActiveConnections = new NativeList<NetworkConnection>(maxConnections, Allocator.Persistent);

            if (!Disconnect.IsCreated)
                Disconnect = new NativeList<byte>(2, Allocator.Persistent);

            if (!ActiveInput.IsCreated)
                ActiveInput = new NativeQueue<TransportMessage>(Allocator.Persistent);
            if (!InactiveInput.IsCreated)
                InactiveInput = new NativeQueue<TransportMessage>(Allocator.Persistent);
            if (!ActiveOutput.IsCreated)
                ActiveOutput = new NativeQueue<TransportMessage>(Allocator.Persistent);
            if (!InactiveOutput.IsCreated)
                InactiveOutput = new NativeQueue<TransportMessage>(Allocator.Persistent);
        }

        public void SwapInOutBuffers()
        {
            (ActiveInput, InactiveInput) = (InactiveInput, ActiveInput);
            (ActiveOutput, InactiveOutput) = (InactiveOutput, ActiveOutput);
        }

        public void DisconnectPlayers()
        {
            if (!Driver.IsCreated)
                return;
            if (!ActiveConnections.IsCreated || ActiveConnections.Length == 0)
                return;
            for (int i = 0; i < ActiveConnections.Length; i++)
            {
                var player = ActiveConnections[i];
                if (player != default && Driver.GetConnectionState(player) != NetworkConnection.State.Disconnected)
                    Driver.Disconnect(player);
            }

            ActiveConnections.Clear();
        }

        private static unsafe void ClearMessageQueue(ref NativeQueue<TransportMessage> queue,
            Allocator allocator = Allocator.TempJob)
        {
            if (!queue.IsCreated)
                return;
            while (queue.Count > 0)
            {
                if (queue.TryDequeue(out var message) && (void*) message.Data != null)
                    UnsafeUtility.Free((void*) message.Data, allocator);
            }

            //clears internal state
            queue.Clear();
        }

        public static void DisconnecConnectedClients(NetworkDriver driver, NativeList<NetworkConnection> connections)
        {
            if (!driver.IsCreated)
                return;
            for (int i = 0; i < connections.Length; i++)
            {
                if (driver.GetConnectionState(connections[i]) != NetworkConnection.State.Disconnected)
                    driver.Disconnect(connections[i]);
            }
        }

        public void Clear()
        {
            if (ActiveConnections.IsCreated)
            {
                DisconnecConnectedClients(Driver, ActiveConnections);
                ActiveConnections.Clear();
            }

            if (Disconnect.IsCreated)
                Disconnect.Clear();

            ClearMessageQueue(ref ActiveInput);
            ClearMessageQueue(ref InactiveInput);
            ClearMessageQueue(ref ActiveOutput);
            ClearMessageQueue(ref InactiveOutput);
        }
    }
}
