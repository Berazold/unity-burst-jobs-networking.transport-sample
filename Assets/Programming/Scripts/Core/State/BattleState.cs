﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.Framework;
using CrazyRam.Core.Helpers;
using Pong.Systems.Battle.Physics;
using Unity.Collections;
using UnityEngine;

namespace Pong.Systems.Battle
{
    public class BattleState : ILevelState
    {
        public NativeHashMap<int, int> PhysicalIdToPlayerId;

        public NativeHashMap<int, int> NetworkToPlayerId;

        public NativeArray<PlatformParams> PlatformParams;

        private MatchState _state;

        public MatchState State
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _state;
            set
            {
                _state = value;
                LastStateSwapTime = Time.realtimeSinceStartup;
            }
        }

        public WorldPhase Phase;

        public float LastStateSwapTime;

        public uint Tick;

        public int MyId;

        public NativeArray<int> Scores;

        public NativeList<int> AIPlayers;

        public PongPhysicsWorld World;

        public VisualWorld VisualWorld;

        public readonly BattleType Type;

        public int EnvironmentId;

        public BattleState(BattleType type)
        {
            Type = type;
            PhysicalIdToPlayerId = new NativeHashMap<int, int>(2, Allocator.Persistent);
            NetworkToPlayerId = new NativeHashMap<int, int>(2, Allocator.Persistent);
            AIPlayers = new NativeList<int>(1, Allocator.Persistent);
        }

        public void Dispose()
        {
            if (PhysicalIdToPlayerId.IsCreated)
                PhysicalIdToPlayerId.Dispose();
            if (NetworkToPlayerId.IsCreated)
                NetworkToPlayerId.Dispose();
            PlatformParams.SafeRelease();
            Scores.SafeRelease();
            AIPlayers.SafeRelease();
            World?.Dispose();
            VisualWorld?.Dispose();
        }
    }
}
