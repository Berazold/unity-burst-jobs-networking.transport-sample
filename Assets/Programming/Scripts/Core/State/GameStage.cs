﻿namespace Pong
{
    public enum GameStage
    {
        Menu = 0,
        NetworkScan = 1,
        WaitingForPlayers = 2,
        Match = 3
    }
}
