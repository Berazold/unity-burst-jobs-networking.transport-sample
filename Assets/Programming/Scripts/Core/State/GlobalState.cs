﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.Framework;
using CrazyRam.Core.Utils;
using Pong.Network;
using Pong.Systems.Battle;
using Pong.Utils.Serialization;

namespace Pong
{
    public class GlobalState : ILevelState
    {
        public readonly DbManager DbManager;

        public GameStage Stage;

        public readonly PlayerInfo Player;

        public ScoreResultEntry[] TopScores;

        public readonly SceneSetup SceneSetup;

        public int PreferredEnvironmentType;

        public int PreferredBallType;

        public readonly int BaseScoreIncrement;

        public BattleState BattleState;

        public readonly NetworkState NetworkState;

        public int MaxScore
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => TopScores.Length > 0 ? TopScores[0].Score : 0;
        }

        public GlobalState(ref SaveData data, DbManager dbManager, INetworkSerializationProvider provider,
            int baseScoreIncrement, GameStage stage = GameStage.Menu)
        {
            DbManager = dbManager;
            BaseScoreIncrement = baseScoreIncrement;
            Player = new PlayerInfo(string.Empty, data.LastName);
            PreferredEnvironmentType = data.PreferredEnvironmentType;
            PreferredBallType = data.PreferredBallType;

            Stage = stage;

            TopScores = data.TopScores;
            SceneSetup = new SceneSetup();
            NetworkState = new NetworkState
            {
                IsLittleEndian = ByteIO.IsLittleEndian ? ByteAction.On : ByteAction.Off,
                Provider = provider,
            };
            NetworkState.InitResources();
        }
    }
}
