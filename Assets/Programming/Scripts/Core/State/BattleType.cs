﻿namespace Pong
{
    public enum BattleType
    {
        SinglePlayer = 0,
        TwoPlayers = 1,
        Multiplayer = 2,
        Server = 3
    }
}
