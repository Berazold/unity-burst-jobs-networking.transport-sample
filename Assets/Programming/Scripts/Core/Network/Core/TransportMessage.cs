﻿using System;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Networking.Transport;

namespace Pong.Network
{
    public readonly struct TransportMessage
    {
        public readonly NetworkConnection Connection;

        public readonly long Data;

        public readonly uint Tick;

        public readonly ushort Size;

        public readonly byte IsLittleEndian;

        public readonly byte CommandType;

        public TransportMessage(NetworkConnection connection, long data, uint tick, ushort size, byte isLittleEndian,
            byte commandType)
        {
            Connection = connection;
            Data = data;
            Tick = tick;
            Size = size;
            IsLittleEndian = isLittleEndian;
            CommandType = commandType;
        }

        public static unsafe TransportMessage Clone(in TransportMessage target)
        {
            long data = (long) IntPtr.Zero;
            if ((void*) target.Data != null)
            {
                var memory = UnsafeUtility.Malloc(target.Size, UnsafeUtility.AlignOf<byte>(), Allocator.TempJob);
                UnsafeUtility.MemCpy(memory, (void*) target.Data, target.Size);
                data = (long) memory;
            }

            return new TransportMessage(target.Connection, data, target.Tick, target.Size, target.IsLittleEndian,
                target.CommandType);
        }
    }
}
