﻿using System;
using Pong.Utils.Serialization;
using Unity.Networking.Transport;

namespace Pong.Network
{
    public interface INetworkSerializationProvider
    {
        (IntPtr, int) Serialize<T, TSerializer>(ref TSerializer writer, ref T command, int reservedBytes = 0)
            where T : struct, INetworkCommand
            where TSerializer : struct, IBinaryWriter;

        void Deserialize<TSerializer>(ref TSerializer reader, int type, IntPtr buffer, NetworkConnection conn,
            uint tick)
            where TSerializer : struct, IBinaryReader;
    }
}
