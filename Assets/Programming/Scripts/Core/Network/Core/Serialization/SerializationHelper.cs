﻿using System;
using CrazyRam.Core.MessageBus;
using CrazyRam.Core.Utils;
using Pong.Events;
using Pong.Utils.Serialization;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Networking.Transport;

namespace Pong.Network
{
    public static class SerializationHelper
    {
        public static void ClearSerializedCommandQueues()
        {
            ref var inputQueue = ref ClientCommutator<NetworkCommandEvent<InputReplicationCommand>>.Default;
            for (int i = 0; i < inputQueue.Tail; i++)
                inputQueue.Array[i].Command.Dispose();
            inputQueue.ResetCounter();

            ref var worldStateQueue = ref ClientCommutator<NetworkCommandEvent<WorldStateReplicationCommand>>.Default;
            for (int i = 0; i < worldStateQueue.Tail; i++)
                worldStateQueue.Array[i].Command.Dispose();
            worldStateQueue.ResetCounter();
        }

        public static void ParseIncomingMessages(GlobalState state, NativeQueue<TransportMessage> queue)
        {
            var clientLittleEndian = state.NetworkState.IsLittleEndian;
            while (queue.IsCreated && queue.TryDequeue(out var message))
            {
                var shouldReverse = (clientLittleEndian == ByteAction.On) ^
                                    (message.IsLittleEndian == ByteAction.On);

                if (shouldReverse)
                {
                    var binaryReader = new ReversedBinaryReader();
                    Deserialize(ref binaryReader, message.CommandType,
                        (IntPtr) message.Data, state.NetworkState.Provider, message.Connection, message.Tick);
                }
                else
                {
                    var binaryReader = new BinaryReader();

                    Deserialize(ref binaryReader, message.CommandType,
                        (IntPtr) message.Data, state.NetworkState.Provider, message.Connection, message.Tick);
                }
            }
        }

        public static SerializedMessage SerializeCommand<T>(ref T request, INetworkSerializationProvider provider)
            where T : struct, INetworkCommand
        {
            var writer = new BinaryWriter();
            var (buffer, size) = provider.Serialize(ref writer, ref request);
            request.Dispose();

            var message = new SerializedMessage((long) buffer, (ushort) size, (byte) request.Type);
            return message;
        }

        public static unsafe void Deserialize<TBinaryReader>(ref TBinaryReader reader, byte command, IntPtr buffer,
            INetworkSerializationProvider provider, NetworkConnection conn, uint tick)
            where TBinaryReader : struct, IBinaryReader
        {
            if (buffer == IntPtr.Zero)
                return;
            provider.Deserialize(ref reader, command, buffer, conn, tick);
            UnsafeUtility.Free((void*) buffer, Allocator.TempJob);
        }
    }
}
