﻿namespace Pong.Network
{
    public readonly struct SerializedMessage
    {
        public readonly long Data;

        public readonly ushort Size;

        public readonly byte CommandType;

        public SerializedMessage(long data, ushort size, byte commandType)
        {
            Data = data;
            Size = size;
            CommandType = commandType;
        }
    }
}
