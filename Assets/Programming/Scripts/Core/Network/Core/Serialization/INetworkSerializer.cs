﻿using System;
using Pong.Utils.Serialization;
using Unity.Networking.Transport;

namespace Pong.Network
{
    public unsafe interface INetworkSerializer
    {
        ushort Serialize<TSerializer>(ref TSerializer writer, void* data, IntPtr buffer)
            where TSerializer : struct, IBinaryWriter;

        ushort Deserialize<TSerializer>(ref TSerializer reader, IntPtr buffer, NetworkConnection conn, uint tick)
            where TSerializer : struct, IBinaryReader;
    }
}
