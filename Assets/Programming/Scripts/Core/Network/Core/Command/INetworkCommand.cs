﻿namespace Pong.Network
{
    public interface INetworkCommand
    {
        int BinarySize { get; }

        CommandType Type { get; }

        void Dispose();
    }
}
