﻿namespace Pong.Network
{
    public readonly struct CommandHeader
    {
        public readonly byte IsLittleEndian;

        public readonly byte CommandType;

        public readonly ushort Size;

        public readonly ushort SessionMagic;

        public readonly uint Tick;

        public const int BinarySize = sizeof(byte) + sizeof(byte) + sizeof(ushort) + sizeof(ushort) + sizeof(uint);

        public CommandHeader(byte isLittleEndian, byte commandType, ushort size, ushort sessionMagic, uint tick)
        {
            IsLittleEndian = isLittleEndian;
            CommandType = commandType;
            Size = size;
            SessionMagic = sessionMagic;
            Tick = tick;
        }
    }
}
