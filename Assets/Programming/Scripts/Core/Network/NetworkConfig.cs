﻿using System;

namespace Pong
{
    [Serializable]
    public class NetworkConfig
    {
        public ushort Port;

        public float ServerConnectionTimeout;
    }
}
