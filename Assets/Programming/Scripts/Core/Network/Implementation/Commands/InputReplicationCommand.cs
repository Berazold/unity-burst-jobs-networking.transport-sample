﻿using System.Runtime.CompilerServices;
using Unity.Mathematics;

namespace Pong.Network
{
    public readonly struct InputReplicationCommand : INetworkCommand
    {
        public readonly half2 Direction;

        private const int InternalBinarySize = sizeof(ushort) * 2;

        public int BinarySize
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => InternalBinarySize;
        }

        public CommandType Type
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => CommandType.InputReplication;
        }

        public void Dispose()
        {
        }

        public override string ToString()
        {
            return $"{nameof(Direction)}: {Direction}";
        }

        public InputReplicationCommand(float2 direction) : this()
        {
            Direction = new half2(direction);
        }

        public InputReplicationCommand(half2 direction) : this()
        {
            Direction = direction;
        }
    }
}
