﻿using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
#if UNITY_EDITOR
using System.Text;
#endif

namespace Pong.Network
{
    public readonly struct MatchConnectionResponse : INetworkCommand
    {
        public readonly FixedString128 ServerName;

        public readonly uint MaxServerScore;

        //ball size
        // NativeArray<half>
        public readonly long BallParameters;

        public readonly byte BallParametersLength;

        public readonly byte LocationType;

        public readonly byte BallType;

        public readonly byte PlatformId;

        public readonly Allocator BallParametersAllocator;

        private const int InternalBinarySize =
            //max score
            sizeof(uint) +
            //ball properties length
            sizeof(byte) +
            //location
            sizeof(byte) +
            //ball type
            sizeof(byte) +
            //platform id
            sizeof(byte);

        public int BinarySize
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => InternalBinarySize + FixedString128.UTF8MaxLengthInBytes + BallParametersLength * sizeof(ushort);
        }

        public CommandType Type
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => CommandType.MatchConnectionResponse;
        }

        public unsafe void Dispose()
        {
            if ((void*) BallParameters != null)
                UnsafeUtility.Free((void*) BallParameters, BallParametersAllocator);
        }

#if UNITY_EDITOR
        public override unsafe string ToString()
        {
            var sb = new StringBuilder();
            for (int i = 0; i < BallParametersLength; i++)
            {
                sb.Append(UnsafeUtility.ReadArrayElement<half>((void*) BallParameters, i));
                sb.Append(',');
                sb.Append(' ');
            }

            return
                $"{nameof(ServerName)}: {ServerName}, {nameof(MaxServerScore)}: {MaxServerScore}, {nameof(BallParameters)}: {sb} {nameof(BallParametersLength)}: {BallParametersLength}, {nameof(LocationType)}: {LocationType}, {nameof(BallType)}: {BallType}, {nameof(PlatformId)}: {PlatformId}, {nameof(BallParametersAllocator)}: {BallParametersAllocator}";
        }
#endif

        public MatchConnectionResponse(FixedString128 serverName, uint maxServerScore, long ballParameters,
            byte ballParametersLength, byte locationType, byte ballType, byte platformId,
            Allocator ballParametersAllocator)
        {
            ServerName = serverName;
            MaxServerScore = maxServerScore;
            BallParameters = ballParameters;
            BallParametersLength = ballParametersLength;
            LocationType = locationType;
            BallType = ballType;
            PlatformId = platformId;
            BallParametersAllocator = ballParametersAllocator;
        }
    }
}
