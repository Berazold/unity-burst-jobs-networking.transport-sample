﻿using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Pong.Network
{
    public readonly struct RoundResultCommand : INetworkCommand
    {
        public readonly uint Score;

        public readonly long BallParameters;

        public readonly byte BallParametersLength;

        public readonly Allocator BallParametersAllocator;

        private const int InternalBinarySize = sizeof(uint) + sizeof(byte);

        public int BinarySize
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => InternalBinarySize + sizeof(ushort) * BallParametersLength;
        }

        public CommandType Type
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => CommandType.RoundResult;
        }

        public unsafe void Dispose()
        {
            UnsafeUtility.Free((void*) BallParameters, BallParametersAllocator);
        }

        public override string ToString()
        {
            return $"{nameof(Score)}: {Score}";
        }

        public RoundResultCommand(uint score, byte ballParametersLength, long ballParameters,
            Allocator ballParametersAllocator)
        {
            Score = score;
            BallParameters = ballParameters;
            BallParametersLength = ballParametersLength;
            BallParametersAllocator = ballParametersAllocator;
        }
    }
}
