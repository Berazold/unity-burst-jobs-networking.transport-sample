﻿using System.Runtime.CompilerServices;
using Unity.Collections;

namespace Pong.Network
{
    public readonly struct MatchConnectionRequest : INetworkCommand
    {
        public readonly FixedString128 Name;

        public readonly uint MaxScore;

        //utf8 string length + Score
        private const int InternalByteSize = sizeof(ushort) + sizeof(uint);

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name.ToString()}, {nameof(MaxScore)}: {MaxScore}";
        }

        public int BinarySize
        {
            //defensive-copy? todo check IL
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => InternalByteSize + FixedString128.UTF8MaxLengthInBytes;
        }

        public CommandType Type
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => CommandType.MatchConnectionRequest;
        }

        public void Dispose()
        {
        }

        public MatchConnectionRequest(FixedString128 name, uint maxScore)
        {
            Name = name;
            MaxScore = maxScore;
        }

        public MatchConnectionRequest(string name, uint maxScore)
        {
            Name = new FixedString128(name);
            MaxScore = maxScore;
        }
    }
}
