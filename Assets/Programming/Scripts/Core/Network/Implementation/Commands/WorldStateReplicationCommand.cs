﻿using System.Runtime.CompilerServices;
using Pong.Systems.Battle;
#if UNITY_EDITOR
using System;
using System.Text;
#endif
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Pong.Network
{
    public readonly struct WorldStateReplicationCommand : INetworkCommand
    {
        //sizeof half2
        public readonly long PlatformPositions;

        //sizeof half2
        public readonly long BallPositions;

        //sizeof 2 byte + half2 position + half2 normal
        public readonly long Contacts;

        public readonly Allocator Allocator;

        public readonly byte PlatformPositionsLength;

        public readonly byte BallPositionsLength;

        public readonly byte ContactsLength;

        public readonly WorldPhase WorldPhase;

        private const int InternalBinarySize = sizeof(byte) * 4;

        private const int InternalPlatformDataSize = sizeof(ushort) * 2;

        private const int InternalBallDataSize = sizeof(ushort) * 2;

        private const int InternalContactDataSize = sizeof(byte) * 2 + sizeof(ushort) * 2 + sizeof(ushort) * 2;

        public int BinarySize
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => InternalBinarySize +
                   InternalPlatformDataSize * PlatformPositionsLength +
                   InternalBallDataSize * BallPositionsLength +
                   InternalContactDataSize * ContactsLength;
        }

        public CommandType Type
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => CommandType.WorldStateReplication;
        }

        public unsafe void Dispose()
        {
            if ((void*) PlatformPositionsLength != null)
                UnsafeUtility.Free((void*) PlatformPositionsLength, Allocator);
            if ((void*) BallPositionsLength != null)
                UnsafeUtility.Free((void*) BallPositionsLength, Allocator);
            if ((void*) Contacts != null)
                UnsafeUtility.Free((void*) Contacts, Allocator);
        }

#if UNITY_EDITOR
        public override unsafe string ToString()
        {
            var platforms = new StringBuilder();
            for (int i = 0; i < PlatformPositionsLength; i++)
            {
                platforms.Append(UnsafeUtility.ReadArrayElement<Unity.Mathematics.half2>((void*) PlatformPositions, i));
                platforms.Append(',');
                platforms.Append(' ');
            }

            var balls = new StringBuilder();
            for (int i = 0; i < BallPositionsLength; i++)
            {
                balls.Append(UnsafeUtility.ReadArrayElement<Unity.Mathematics.half2>((void*) BallPositions, i));
                balls.Append(',');
                balls.Append(' ');
            }

            var contacts = new StringBuilder();
            if (ContactsLength > 0 && Contacts != 0)
            {
                var contactPtr = (IntPtr) (void*) Contacts;

                var reader = new Utils.Serialization.BinaryReader();
                for (int i = 0; i < ContactsLength; i++)
                {
                    var offset = i * InternalContactDataSize;
                    var readPtr = contactPtr + offset;

                    var id0 = reader.ReadUint8(readPtr, out var readed);
                    readPtr += readed;

                    var id1 = reader.ReadUint8(readPtr, out readed);
                    readPtr += readed;

                    var posX = reader.ReadFloat16(readPtr, out readed);
                    readPtr += readed;

                    var posY = reader.ReadFloat16(readPtr, out readed);
                    readPtr += readed;

                    var normalX = reader.ReadFloat16(readPtr, out readed);
                    readPtr += readed;

                    var normalY = reader.ReadFloat16(readPtr, out readed);

                    contacts.Append($"id0: {id0} id1: {id1} pos: {posX} {posY} normal: {normalX} {normalY}, ");
                }
            }

            return
                $"{nameof(PlatformPositions)}: {platforms} {nameof(BallPositions)}: {balls} {nameof(Contacts)}: {contacts}, {nameof(Allocator)}: {Allocator}, {nameof(WorldPhase)}: {WorldPhase}";
        }
#endif

        public WorldStateReplicationCommand(
            long platformPositions, byte platformPositionsLength,
            long ballPositions, byte ballPositionsLength,
            long contacts, byte contactsLength,
            WorldPhase worldPhase, Allocator allocator)
        {
            PlatformPositions = platformPositions;
            BallPositions = ballPositions;
            Contacts = contacts;
            Allocator = allocator;
            PlatformPositionsLength = platformPositionsLength;
            BallPositionsLength = ballPositionsLength;
            ContactsLength = contactsLength;
            WorldPhase = worldPhase;
        }
    }
}
