﻿namespace Pong.Network
{
    public enum CommandType : byte
    {
        MatchConnectionRequest = 0,
        MatchConnectionResponse = 1,
        InputReplication = 2,
        WorldStateReplication = 3,
        RoundResult = 4
    }
}
