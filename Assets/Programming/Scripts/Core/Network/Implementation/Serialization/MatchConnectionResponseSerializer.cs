﻿using System;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Utils.Serialization;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
using Unity.Networking.Transport;

namespace Pong.Network
{
    public class MatchConnectionResponseSerializer : INetworkSerializer
    {
        public unsafe ushort Serialize<TSerializer>(ref TSerializer writer, void* data, IntPtr buffer)
            where TSerializer : struct, IBinaryWriter
        {
            var ptr = (MatchConnectionResponse*) data;
            var start = (long) buffer;

            buffer += sizeof(ushort);
            (&ptr->ServerName)->CopyTo((byte*) buffer, out var writedNameLength, FixedString128.UTF8MaxLengthInBytes);
            writer.WriteUint16(writedNameLength, buffer - sizeof(ushort));
            buffer += ptr->ServerName.UTF8LengthInBytes;

            buffer += writer.WriteUint32(ptr->MaxServerScore, buffer);

            buffer += writer.WriteUint8(ptr->BallParametersLength, buffer);
            int parametersLength = ptr->BallParametersLength * sizeof(ushort);
            UnsafeUtility.MemCpy((void*) buffer, (void*) ptr->BallParameters, parametersLength);
            buffer += parametersLength;

            buffer += writer.WriteUint8(ptr->LocationType, buffer);
            buffer += writer.WriteUint8(ptr->BallType, buffer);
            buffer += writer.WriteUint8(ptr->PlatformId, buffer);

            return (ushort) ((long) buffer - start);
        }

        public unsafe ushort Deserialize<TSerializer>(ref TSerializer reader, IntPtr buffer, NetworkConnection conn,
            uint tick)
            where TSerializer : struct, IBinaryReader
        {
            var start = (long) buffer;

            var utf8Length = reader.ReadUint16(buffer, out var readed);
            buffer += readed;

            var serverName = new FixedString128();
            (&serverName)->CopyFrom((byte*) buffer, utf8Length);
            buffer += utf8Length;

            var maxServerScore = reader.ReadUint32(buffer, out readed);
            buffer += readed;

            var ballParametersLength = reader.ReadUint8(buffer, out readed);
            buffer += readed;

            var ballParameters = UnsafeUtility.Malloc(ballParametersLength * sizeof(ushort),
                UnsafeUtility.AlignOf<half>(), Allocator.Temp);
            for (int i = 0; i < ballParametersLength; i++)
            {
                var value = reader.ReadFloat16(buffer, out readed);
                buffer += readed;
                UnsafeUtility.WriteArrayElement(ballParameters, i, value);
            }

            var locationType = reader.ReadUint8(buffer, out readed);
            buffer += readed;

            var ballType = reader.ReadUint8(buffer, out readed);
            buffer += readed;

            var platformId = reader.ReadUint8(buffer, out readed);
            buffer += readed;

            var response = new MatchConnectionResponse(serverName, maxServerScore, (long) ballParameters,
                ballParametersLength, locationType, ballType, platformId, Allocator.Temp);
            var command = new NetworkCommandEvent<MatchConnectionResponse>(conn, tick, response);
            command.RaiseEvent();

            return (ushort) ((long) buffer - start);
        }
    }
}
