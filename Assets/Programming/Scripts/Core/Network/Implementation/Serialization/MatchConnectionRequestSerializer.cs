﻿using System;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Utils.Serialization;
using Unity.Collections;
using Unity.Networking.Transport;

namespace Pong.Network
{
    public class MatchConnectionRequestSerializer : INetworkSerializer
    {
        public unsafe ushort Serialize<TSerializer>(ref TSerializer writer, void* data, IntPtr buffer)
            where TSerializer : struct, IBinaryWriter
        {
            var ptr = (MatchConnectionRequest*) data;
            var start = (long) buffer;

            buffer += sizeof(ushort);
            (&ptr->Name)->CopyTo((byte*) buffer, out var writedNameLength, FixedString128.UTF8MaxLengthInBytes);

            writer.WriteUint16(writedNameLength, buffer - sizeof(ushort));
            buffer += ptr->Name.UTF8LengthInBytes;
            buffer += writer.WriteUint32(ptr->MaxScore, buffer);

            return (ushort) ((long) buffer - start);
        }

        public unsafe ushort Deserialize<TSerializer>(ref TSerializer reader, IntPtr buffer, NetworkConnection conn,
            uint tick) where TSerializer : struct, IBinaryReader
        {
            var start = (long) buffer;
            var utf8Length = reader.ReadUint16(buffer, out var readed);
            buffer += readed;

            var name = new FixedString128();
            (&name)->CopyFrom((byte*) buffer, utf8Length);
            buffer += utf8Length;

            var maxScore = reader.ReadUint32(buffer, out readed);
            buffer += readed;

            var request = new MatchConnectionRequest(name, maxScore);
            var command = new NetworkCommandEvent<MatchConnectionRequest>(conn, tick, request);
            command.RaiseEvent();

            return (ushort) ((long) buffer - start);
        }
    }
}
