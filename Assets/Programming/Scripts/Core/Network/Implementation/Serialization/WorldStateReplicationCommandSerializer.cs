﻿using System;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Network;
using Pong.Systems.Battle;
using Pong.Systems.Battle.Physics;
using Pong.Utils.Serialization;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
using Unity.Networking.Transport;

namespace Pong.Utils
{
    public class WorldStateReplicationCommandSerializer : INetworkSerializer
    {
        public unsafe ushort Serialize<TSerializer>(ref TSerializer writer, void* data, IntPtr buffer)
            where TSerializer : struct, IBinaryWriter
        {
            var ptr = (WorldStateReplicationCommand*) data;
            var start = (long) buffer;

            const int platformDataSize = sizeof(ushort) * 2;
            const int ballDataSize = sizeof(ushort) * 2;
            const int contactDataSize = sizeof(byte) * 2 + sizeof(ushort) * 2 + sizeof(ushort) * 2;

            buffer += writer.WriteUint8(ptr->PlatformPositionsLength, buffer);
            int bufferWriteSize = ptr->PlatformPositionsLength * platformDataSize;
            UnsafeUtility.MemCpy((void*) buffer, (void*) ptr->PlatformPositions, bufferWriteSize);
            buffer += bufferWriteSize;

            buffer += writer.WriteUint8(ptr->BallPositionsLength, buffer);
            bufferWriteSize = ptr->BallPositionsLength * ballDataSize;
            UnsafeUtility.MemCpy((void*) buffer, (void*) ptr->BallPositions, bufferWriteSize);
            buffer += bufferWriteSize;

            buffer += writer.WriteUint8(ptr->ContactsLength, buffer);
            bufferWriteSize = ptr->ContactsLength * contactDataSize;
            UnsafeUtility.MemCpy((void*) buffer, (void*) ptr->Contacts, bufferWriteSize);
            buffer += bufferWriteSize;

            buffer += writer.WriteUint8((byte) ptr->WorldPhase, buffer);

            return (ushort) ((long) buffer - start);
        }

        public unsafe ushort Deserialize<TSerializer>(ref TSerializer reader, IntPtr buffer, NetworkConnection conn,
            uint tick)
            where TSerializer : struct, IBinaryReader
        {
            var start = (long) buffer;

            const int platformDataSize = sizeof(ushort) * 2;
            const int ballDataSize = sizeof(ushort) * 2;

            var platformPositionsLength = reader.ReadUint8(buffer, out var readed);
            buffer += readed;

            var platformPositions = UnsafeUtility.Malloc(platformPositionsLength * platformDataSize,
                UnsafeUtility.AlignOf<half2>(), Allocator.Temp);
            for (int i = 0; i < platformPositionsLength; i++)
            {
                var x = reader.ReadFloat16(buffer, out readed);
                buffer += readed;
                var y = reader.ReadFloat16(buffer, out readed);
                buffer += readed;
                UnsafeUtility.WriteArrayElement(platformPositions, i, new half2(x, y));
            }

            var ballPositionsLength = reader.ReadUint8(buffer, out readed);
            buffer += readed;

            var ballPositions = UnsafeUtility.Malloc(ballPositionsLength * ballDataSize,
                UnsafeUtility.AlignOf<half2>(), Allocator.Temp);
            for (int i = 0; i < ballPositionsLength; i++)
            {
                var x = reader.ReadFloat16(buffer, out readed);
                buffer += readed;
                var y = reader.ReadFloat16(buffer, out readed);
                buffer += readed;
                UnsafeUtility.WriteArrayElement(ballPositions, i, new half2(x, y));
            }

            var contactsLength = reader.ReadUint8(buffer, out readed);
            buffer += readed;

            if (contactsLength > 0)
            {
                for (int i = 0; i < contactsLength; i++)
                {
                    var id0 = (int) reader.ReadUint8(buffer, out readed);
                    buffer += readed;

                    var id1 = (int) reader.ReadUint8(buffer, out readed);
                    buffer += readed;

                    var posX = reader.ReadFloat16(buffer, out readed);
                    buffer += readed;

                    var posY = reader.ReadFloat16(buffer, out readed);
                    buffer += readed;

                    var normalX = reader.ReadFloat16(buffer, out readed);
                    buffer += readed;

                    var normalY = reader.ReadFloat16(buffer, out readed);
                    buffer += readed;

                    ClientCommutator<PhysicsContactEvent>.Default += new PhysicsContactEvent(id0, id1,
                        new float2(posX, posY), new float2(normalX, normalY));
                }
            }

            var worldPhase = (WorldPhase) reader.ReadUint8(buffer, out readed);
            buffer += readed;

            var world = new WorldStateReplicationCommand((long) platformPositions, platformPositionsLength,
                (long) ballPositions, ballPositionsLength, 0, contactsLength, worldPhase,
                Allocator.Temp);
            var command = new NetworkCommandEvent<WorldStateReplicationCommand>(conn, tick, world);
            ClientCommutator<NetworkCommandEvent<WorldStateReplicationCommand>>.Default += command;

            return (ushort) ((long) buffer - start);
        }
    }
}
