﻿using System;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Utils.Serialization;
using Unity.Mathematics;
using Unity.Networking.Transport;

namespace Pong.Network
{
    public class InputReplicationCommandSerializer : INetworkSerializer
    {
        public unsafe ushort Serialize<TSerializer>(ref TSerializer writer, void* data, IntPtr buffer)
            where TSerializer : struct, IBinaryWriter
        {
            var ptr = (InputReplicationCommand*) data;
            var start = (long) buffer;

            buffer += writer.WriteFloat16(ptr->Direction.x, buffer);
            buffer += writer.WriteFloat16(ptr->Direction.y, buffer);

            return (ushort) ((long) buffer - start);
        }

        public ushort Deserialize<TSerializer>(ref TSerializer reader, IntPtr buffer, NetworkConnection conn, uint tick)
            where TSerializer : struct, IBinaryReader
        {
            var start = (long) buffer;

            var x = reader.ReadFloat16(buffer, out var readed);
            buffer += readed;

            var y = reader.ReadFloat16(buffer, out readed);
            buffer += readed;


            var input = new InputReplicationCommand(new half2(x, y));
            var command = new NetworkCommandEvent<InputReplicationCommand>(conn, tick, input);
            ClientCommutator<NetworkCommandEvent<InputReplicationCommand>>.Default += command;

            return (ushort) ((long) buffer - start);
        }
    }
}
