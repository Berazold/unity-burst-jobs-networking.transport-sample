﻿using System;
using CrazyRam.Core.Helpers;
using Pong.Utils.Serialization;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Networking.Transport;

namespace Pong.Network
{
    public class PongNetworkSerializer : INetworkSerializationProvider
    {
        private readonly INetworkSerializer[] _serializers;

        public PongNetworkSerializer(INetworkSerializer[] serializers)
        {
            _serializers = serializers;
        }

        public unsafe (IntPtr, int) Serialize<T, TSerializer>(ref TSerializer writer, ref T command,
            int reservedBytes = 0)
            where T : struct, INetworkCommand
            where TSerializer : struct, IBinaryWriter
        {
            var size = command.BinarySize;
            var buffer = (IntPtr) UnsafeUtility.Malloc(size + reservedBytes, UnsafeUtility.AlignOf<byte>(),
                Allocator.TempJob);

            var type = (byte) command.Type;
            if (!_serializers.InBounds(type))
                return (IntPtr.Zero, 0);

            var ptr = System.Runtime.CompilerServices.Unsafe.AsPointer(ref command);
            _serializers[type].Serialize(ref writer, ptr, buffer + reservedBytes);

            return (buffer, size);
        }

        public void Deserialize<TSerializer>(ref TSerializer reader, int type, IntPtr buffer, NetworkConnection conn,
            uint tick)
            where TSerializer : struct, IBinaryReader
        {
            if (_serializers.InBounds(type))
                _serializers[type].Deserialize(ref reader, buffer, conn, tick);
        }
    }
}
