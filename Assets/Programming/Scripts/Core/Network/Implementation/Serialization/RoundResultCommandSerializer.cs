﻿using System;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Utils.Serialization;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
using Unity.Networking.Transport;

namespace Pong.Network
{
    public class RoundResultCommandSerializer : INetworkSerializer
    {
        public unsafe ushort Serialize<TSerializer>(ref TSerializer writer, void* data, IntPtr buffer)
            where TSerializer : struct, IBinaryWriter
        {
            var ptr = (RoundResultCommand*) data;
            var start = (long) buffer;

            buffer += writer.WriteUint32(ptr->Score, buffer);

            buffer += writer.WriteUint8(ptr->BallParametersLength, buffer);
            int parametersLength = ptr->BallParametersLength * sizeof(ushort);
            UnsafeUtility.MemCpy((void*) buffer, (void*) ptr->BallParameters, parametersLength);
            buffer += parametersLength;

            return (ushort) ((long) buffer - start);
        }

        public unsafe ushort Deserialize<TSerializer>(ref TSerializer reader, IntPtr buffer, NetworkConnection conn,
            uint tick) where TSerializer : struct, IBinaryReader
        {
            var start = (long) buffer;

            var score = reader.ReadUint32(buffer, out var readed);
            buffer += readed;

            var ballParametersLength = reader.ReadUint8(buffer, out readed);
            buffer += readed;

            var ballParameters = UnsafeUtility.Malloc(ballParametersLength * sizeof(ushort),
                UnsafeUtility.AlignOf<half>(), Allocator.Temp);
            for (int i = 0; i < ballParametersLength; i++)
            {
                var value = reader.ReadFloat16(buffer, out readed);
                buffer += readed;
                UnsafeUtility.WriteArrayElement(ballParameters, i, value);
            }

            var result = new RoundResultCommand(score, ballParametersLength, (long) ballParameters, Allocator.Temp);
            var command = new NetworkCommandEvent<RoundResultCommand>(conn, tick, result);
            command.RaiseEvent();

            return (ushort) ((long) buffer - start);
        }
    }
}
