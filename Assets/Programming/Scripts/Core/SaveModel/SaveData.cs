﻿using System;

namespace Pong
{
    [Serializable]
    public struct SaveData
    {
        public string LastName;

        public ScoreResultEntry[] TopScores;

        public int PreferredEnvironmentType;

        public int PreferredBallType;

        public static SaveData Default => new SaveData
        {
            TopScores = new ScoreResultEntry[0]
        };

        public override string ToString()
        {
            return
                $"{nameof(LastName)}: {LastName}, {nameof(TopScores)}: {TopScores}, {nameof(PreferredEnvironmentType)}: {PreferredEnvironmentType}, {nameof(PreferredBallType)}: {PreferredBallType}";
        }
    }
}
