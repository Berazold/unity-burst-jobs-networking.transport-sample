﻿using System;

namespace Pong
{
    [Serializable]
    public struct ScoreResultEntry
    {
        public string Name;

        public int Score;

        public long TimeStamp;
    }
}
