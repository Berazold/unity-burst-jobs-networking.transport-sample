﻿namespace Pong.Systems.Battle
{
    public enum WorldPhase
    {
        WaitingForStart = 0,
        WaitingForRound = 1,
        InProgress = 2,
        Pause = 3
    }
}
