﻿using Unity.Mathematics;

namespace Pong.Systems.Battle
{
    public readonly struct PlatformParams
    {
        public readonly float2 Forward;

        public readonly float2 DirectionMask;

        public PlatformParams(float2 forward, float2 directionMask)
        {
            Forward = forward;
            DirectionMask = directionMask;
        }
    }
}
