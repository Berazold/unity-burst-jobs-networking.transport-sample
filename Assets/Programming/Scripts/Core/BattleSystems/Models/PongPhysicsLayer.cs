﻿namespace Pong.Systems.Battle
{
    public enum PongPhysicsLayer
    {
        Platform = 0,
        Ball = 1,
        Wall = 2,
        GameOverZone = 3
    }
}
