﻿namespace Pong.Systems.Battle
{
    public enum MatchState
    {
        Waiting = 0,
        InProgress = 1,
        Pause = 2,
        Ending = 3,
        Simulate = 4
    }
}
