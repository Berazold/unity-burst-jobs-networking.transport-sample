﻿using System;
using CrazyRam.Core.Framework;
using CrazyRam.Core.Helpers;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Network;
using Pong.Systems.Battle.Physics;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;

namespace Pong.Systems.Battle
{
    public class WorldReplicationSystem : IGameSystem, IMessageReceiver<BattleForceResetPhysics>
    {
        private BattleState _state;

        private uint _lastWorldTick;

        public void Init(ILevelState state)
        {
            _state = ((GlobalState) state).BattleState;
            _lastWorldTick = 0;
            ClientCommutator<NetworkCommandEvent<WorldStateReplicationCommand>>.Default.ResetCounter();
            ClientDispatcher<BattleForceResetPhysics>.Default += this;
        }

        public void Dispose()
        {
            ClientDispatcher<BattleForceResetPhysics>.Default -= this;
        }

        public unsafe void Update()
        {
            var world = _state.World;
            ref var channel = ref ClientCommutator<NetworkCommandEvent<WorldStateReplicationCommand>>.Default;
            if (!channel.HasNext)
                return;

            while (channel.HasNext)
            {
                ref readonly var data = ref channel.DrainRef();
                if (data.Tick < _lastWorldTick)
                {
                    data.Command.Dispose();
                    continue;
                }

                _lastWorldTick = data.Tick;
                var command = data.Command;

                int platformsCount = math.min(world.Platforms.Length, command.PlatformPositionsLength);
                var platformSize = (long) UnsafeUtility.SizeOf<DynamicBox>();
                var platformsPtr = (long) (IntPtr) world.Platforms.GetUnsafePtr();

                var platformData = UnityHelper.ExistingDataToNativeArray<half2>((void*) command.PlatformPositions,
                    command.PlatformPositionsLength);
                for (int i = 0; i < platformsCount; i++)
                {
                    var platform = (DynamicBox*) math.mad(i, platformSize, platformsPtr);
                    (&platform->Box)->Position = platformData[i];
                }

                int ballsLength = math.min(world.Spheres.Length, command.BallPositionsLength);
                var ballSize = (long) UnsafeUtility.SizeOf<Sphere>();
                var ballsPtr = (long) (IntPtr) world.Spheres.GetUnsafePtr();

                var ballData =
                    UnityHelper.ExistingDataToNativeArray<half2>((void*) command.BallPositions,
                        command.BallPositionsLength);
                for (int i = 0; i < ballsLength; i++)
                {
                    var ball = (Sphere*) math.mad(i, ballSize, ballsPtr);
                    ball->Position = ballData[i];
                }

#if UNITY_EDITOR
                platformData.ReleaseSafetyHandle();
                ballData.ReleaseSafetyHandle();
#endif

                _state.Phase = command.WorldPhase;
                data.Command.Dispose();
            }

            channel.ResetCounter();
        }

        public void LateUpdate()
        {
        }

        public void PhysicsUpdate()
        {
        }

        public void OnDrawGizmos()
        {
        }

        public unsafe void OnMessage(in BattleForceResetPhysics data)
        {
            ref var channel = ref ClientCommutator<NetworkCommandEvent<WorldStateReplicationCommand>>.Default;
            for (int i = 0; i < channel.Tail; i++)
                channel.Array[i].Command.Dispose();

            channel.ResetCounter();

            var world = _state.World;
            var platformModels = data.PlatformModels;
            var ballModels = data.BallModels;

            var platformSize = (long) UnsafeUtility.SizeOf<DynamicBox>();
            var platformsPtr = (long) (IntPtr) world.Platforms.GetUnsafePtr();

            for (int i = 0; i < world.Platforms.Length; i++)
            {
                var platform = (DynamicBox*) math.mad(i, platformSize, platformsPtr);
                (&platform->Box)->Position = platformModels[i].Position;
            }

            var sphereSize = (long) UnsafeUtility.SizeOf<Sphere>();
            var spheresPtr = (long) world.Spheres.GetUnsafePtr();
            for (int i = 0; i < world.Spheres.Length; i++)
            {
                var sphere = (Sphere*) math.mad(i, sphereSize, spheresPtr);
                sphere->Direction = data.BallDirections[i].xy;
                sphere->Radius = data.BallDirections[i].z;
                sphere->Position = ballModels[i].Position;
            }

            data.BallDirections.Dispose();
        }
    }
}
