﻿using System;
using CrazyRam.Core.Framework;
using CrazyRam.Core.MessageBus;
using Pong.Network;
using Pong.Systems.Battle.Physics;
using Pong.Utils.Serialization;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
using UnityEngine;

namespace Pong.Systems.Battle
{
    public class WorldSerializationSystem : IGameSystem
    {
        private GlobalState _state;

        private const float WorldSendRate = 1f / 61f;

        private float _lastSendTime;

        public void Init(ILevelState state)
        {
            _state = (GlobalState) state;
            _lastSendTime = Time.realtimeSinceStartup - WorldSendRate;
        }

        public void Dispose()
        {
        }

        public void Update()
        {
        }

        public unsafe void LateUpdate()
        {
            var state = _state.BattleState.State;
            var networkState = _state.NetworkState;
            if (!networkState.IsActive || (state != MatchState.InProgress && state != MatchState.Pause))
                return;
            if (Time.realtimeSinceStartup - _lastSendTime < WorldSendRate)
                return;

            _lastSendTime = Time.realtimeSinceStartup;

            var world = _state.BattleState.World;

#region Platforms

            var platformData = new NativeArray<half2>(world.Platforms.Length, Allocator.Temp);
            var platformSize = (long) UnsafeUtility.SizeOf<DynamicBox>();
            var platformsPtr = (long) (IntPtr) world.Platforms.GetUnsafePtr();

            for (int i = 0; i < platformData.Length; i++)
            {
                var platform = (DynamicBox*) math.mad(i, platformSize, platformsPtr);
                platformData[i] = new half2(platform->Box.Position);
            }

#endregion

#region Balls

            var ballsData = new NativeArray<half2>(world.Spheres.Length, Allocator.Temp);
            var ballSize = (long) UnsafeUtility.SizeOf<Sphere>();
            var ballsPtr = (long) (IntPtr) world.Spheres.GetUnsafePtr();

            for (int i = 0; i < ballsData.Length; i++)
            {
                var ball = (Sphere*) math.mad(i, ballSize, ballsPtr);
                ballsData[i] = new half2(ball->Position);
            }

#endregion

#region Conacts

            ref var contactsChannel = ref ClientCommutator<PhysicsContactEvent>.Default;
            var originalContacts = contactsChannel.Array;
            var contactsCount = contactsChannel.Tail;

            const int packedContactSize = sizeof(byte) * 2 + sizeof(ushort) * 2 + sizeof(ushort) * 2;
            var contacts = (IntPtr) UnsafeUtility.Malloc(packedContactSize * contactsCount,
                UnsafeUtility.AlignOf<byte>(), Allocator.Temp);
            var contactsHead = contacts;

            for (int i = 0; i < contactsCount; i++)
            {
                ref readonly var contact = ref originalContacts[i];

                contacts += ByteIO.WriteUnmanaged(contacts, (byte) contact.Body0);
                contacts += ByteIO.WriteUnmanaged(contacts, (byte) contact.Body1);

                contacts += ByteIO.WriteUnmanaged(contacts, new half(contact.ContactPoint.x));
                contacts += ByteIO.WriteUnmanaged(contacts, new half(contact.ContactPoint.y));

                contacts += ByteIO.WriteUnmanaged(contacts, new half(contact.Normal.x));
                contacts += ByteIO.WriteUnmanaged(contacts, new half(contact.Normal.y));
            }

#endregion

            var replication = new WorldStateReplicationCommand(
                (long) platformData.GetUnsafePtr(), (byte) platformData.Length,
                (long) ballsData.GetUnsafePtr(), (byte) ballsData.Length,
                (long) contactsHead, (byte) contactsCount, _state.BattleState.Phase,
                Allocator.Temp
            );
            for (int i = 0; i < networkState.ActiveConnections.Length; i++)
            {
                var serializedMessage =
                    SerializationHelper.SerializeCommand(ref replication, _state.NetworkState.Provider);
                var connection = networkState.ActiveConnections[i];
                var transportMessage = new TransportMessage(
                    connection, serializedMessage.Data, _state.BattleState.Tick, serializedMessage.Size,
                    networkState.IsLittleEndian,
                    serializedMessage.CommandType
                );
                networkState.ActiveOutput.Enqueue(transportMessage);
            }
        }

        public void PhysicsUpdate()
        {
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
        }
#endif
    }
}
