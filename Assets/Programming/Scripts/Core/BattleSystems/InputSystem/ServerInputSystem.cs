﻿using CrazyRam.Core.Framework;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Network;
using Unity.Collections;
using Unity.Mathematics;

namespace Pong.Systems.Battle
{
    public class ServerInputSystem : InputSystem
    {
        private NativeHashMap<int, NetworkInput> _lastNetworkInputs;

        private readonly struct NetworkInput
        {
            public readonly uint Tick;

            public readonly float2 Direction;

            public NetworkInput(uint tick, float2 direction)
            {
                Tick = tick;
                Direction = direction;
            }
        }

        public override void Init(ILevelState state)
        {
            base.Init(state);
            _lastNetworkInputs = new NativeHashMap<int, NetworkInput>(2, Allocator.Persistent);
        }

        public override void Dispose()
        {
            base.Dispose();
            _lastNetworkInputs.Dispose();
        }

        public override void Update()
        {
            ClearInput();
            ReadInput();
            ReadNetworkInput();
            NormalizeInput();
            PushPhysicsEvents();
        }

        private void ReadNetworkInput()
        {
            var networkState = State.NetworkState;
            var battleState = State.BattleState;
            if (!networkState.IsActive)
                return;

            ref var channel = ref ClientCommutator<NetworkCommandEvent<InputReplicationCommand>>.Default;

            if (!channel.HasNext)
                return;
            int readId = 0;
            while (channel.CanDrainQueue(readId))
            {
                //todo store per-user last input tick
                ref readonly var message = ref channel.DrainQueueRef(ref readId);

                var connId = message.Connection.InternalId;

                if (battleState.NetworkToPlayerId.TryGetValue(connId, out var id))
                {
                    _lastNetworkInputs.TryGetValue(connId, out var lastInput);
                    var direction = message.Tick >= lastInput.Tick ? message.Command.Direction : lastInput.Direction;
                    Input[id] = direction;
                    _lastNetworkInputs[connId] = new NetworkInput(message.Tick, direction);
                }

                message.Command.Dispose();
            }

            channel.ResetCounter();
        }
    }
}
