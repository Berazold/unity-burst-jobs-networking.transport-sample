﻿using CrazyRam.Core;
using CrazyRam.Core.Framework;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Network;
using Unity.Mathematics;

namespace Pong.Systems.Battle
{
    public class ClientInputSystem : IGameSystem
    {
        private float2 _input;

        private GlobalState _state;

        public void Init(ILevelState state)
        {
            _state = (GlobalState) state;
        }

        public void Dispose()
        {
        }

        public void Update()
        {
            _input = float2.zero;
            ReadInput();
            SendInput();
        }

        private void ReadInput()
        {
            ref var channel = ref ClientCommutator<UIInputRequestEvent>.Default;

            int readId = 0;
            Option<UIInputRequestEvent> result;
            while ((result = channel.DrainQueue(ref readId)).HasValue)
            {
                _input += result.Value.Direction;
            }

            channel.ResetCounter();
            if (math.all(math.abs(_input) > new float2(float.Epsilon, float.Epsilon)))
                _input = math.normalize(_input);
        }

        private void SendInput()
        {
            var networkState = _state.NetworkState;
            if (!networkState.IsActive)
                return;

            var input = new InputReplicationCommand(_input);

            var serializedMessage = SerializationHelper.SerializeCommand(ref input, _state.NetworkState.Provider);
            var transportMessage = new TransportMessage(networkState.ServerConnection, serializedMessage.Data,
                _state.BattleState.Tick, serializedMessage.Size, _state.NetworkState.IsLittleEndian,
                serializedMessage.CommandType);
            networkState.ActiveOutput.Enqueue(transportMessage);
        }

        public void LateUpdate()
        {
        }

        public void PhysicsUpdate()
        {
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
        }
#endif
    }
}
