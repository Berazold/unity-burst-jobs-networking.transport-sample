﻿using CrazyRam.Core;
using CrazyRam.Core.Framework;
using CrazyRam.Core.Helpers;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Unity.Collections;
using Unity.Mathematics;

namespace Pong.Systems.Battle
{
    public class InputSystem : IGameSystem
    {
        protected GlobalState State;

        protected NativeArray<float2> Input;

        public virtual void Init(ILevelState state)
        {
            State = (GlobalState) state;
            Input = new NativeArray<float2>(State.BattleState.World.Platforms.Length, Allocator.Persistent);
        }

        public virtual void Dispose()
        {
            Input.SafeRelease();
        }

        public virtual void Update()
        {
            ClearInput();
            ReadInput();
            NormalizeInput();
            PushPhysicsEvents();
        }

        protected void ClearInput()
        {
            for (int i = 0; i < Input.Length; i++)
                Input[i] = float2.zero;
        }

        protected void ReadInput()
        {
            ref var channel = ref ClientCommutator<UIInputRequestEvent>.Default;

            int readId = 0;
            Option<UIInputRequestEvent> result;
            while ((result = channel.DrainQueue(ref readId)).HasValue)
            {
                var id = (int) result.Value.Side;
                if (id < Input.Length)
                    Input[id] = result.Value.Direction;
            }

            channel.ResetCounter();
        }

        protected void NormalizeInput()
        {
            for (int i = 0; i < Input.Length; i++)
            {
                var input = Input[i] * State.BattleState.PlatformParams[i].DirectionMask;
                Input[i] = math.any(input) ? math.normalize(input) : float2.zero;
            }
        }

        protected void PushPhysicsEvents()
        {
            for (int i = 0; i < Input.Length; i++)
                ClientCommutator<BattleChangeBoxDirectionEvent>.Default +=
                    new BattleChangeBoxDirectionEvent(i, Input[i]);
        }

        public void LateUpdate()
        {
        }

        public void PhysicsUpdate()
        {
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
        }
#endif
    }
}
