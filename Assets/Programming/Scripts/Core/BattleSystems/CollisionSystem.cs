﻿using System;
using CrazyRam.Core.Framework;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Systems.Battle.Physics;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;

namespace Pong.Systems.Battle
{
    public static class PhysicalLayerExtension
    {
        public static bool Is(this in PhysicalProperties body, PongPhysicsLayer reference)
        {
            return body.Layer == reference;
        }

        public static bool Is(this PongPhysicsLayer layer, PongPhysicsLayer reference)
        {
            return layer == reference;
        }

        public static bool Any(PongPhysicsLayer layer0, PongPhysicsLayer layer1, PongPhysicsLayer reference)
        {
            return layer0.Is(reference) || layer1.Is(reference);
        }
    }

    public class CollisionSystem : IGameSystem
    {
        private GlobalState _globalState;

        private BattleState _state;

        public void Init(ILevelState state)
        {
            _globalState = (GlobalState) state;
            _state = _globalState.BattleState;
        }

        public void Dispose()
        {
        }

        public void Update()
        {
        }

        private void ReflectBall(in PhysicsContactEvent contact, int physicalId)
        {
            var spheres = _state.World.Spheres;
            for (int i = 0; i < spheres.Length; i++)
            {
                if (spheres[i].PhysicsId != physicalId)
                    continue;
                var sphere = spheres[i];

                ClientCommutator<BattleChangeBallDirectionEvent>.Default +=
                    new BattleChangeBallDirectionEvent(i, math.reflect(sphere.Direction, contact.Normal));
                return;
            }
        }

        private static float Clockwise(float2 left, float2 right)
        {
            return -left.x * right.y + left.y * right.x > 0 ? 1 : -1;
        }

        private unsafe void ReflectPlatform(in PhysicsContactEvent contact, int ballPhysicalId, int platformPhysicalId)
        {
            var world = _state.World;
            var spheres = world.Spheres;
            var spheresPtr = (IntPtr) spheres.GetUnsafePtr();
            var spheresSize = UnsafeUtility.SizeOf<Sphere>();

            Sphere* sphere = null;
            int sphereId = -1;
            for (int i = 0; i < spheres.Length; i++, spheresPtr += spheresSize)
            {
                if (((Sphere*) spheresPtr)->PhysicsId != ballPhysicalId)
                    continue;
                sphere = (Sphere*) spheresPtr;
                sphereId = i;
                break;
            }

            if (sphere == null)
                return;

            if (!_state.PhysicalIdToPlayerId.TryGetValue(platformPhysicalId, out var platformId))
                return;
            var platform = world.Platforms[platformId];

            var angleDistanceFactor =
                math.abs(contact.ContactPoint.x - platform.Box.Position.x) / platform.Box.Extents.x;
            angleDistanceFactor = math.saturate(angleDistanceFactor);

            var angle = math.radians(world.MaxReflectAngle * angleDistanceFactor);
            var angleSign = Clockwise(math.normalize(contact.ContactPoint - platform.Box.Position), platform.Forward);
            angle *= angleSign;

            var direction = platform.Forward;
            math.sincos(angle, out var sin, out var cos);
            direction = new float2(
                direction.x * cos - direction.y * sin,
                direction.x * sin + direction.y * cos);
            direction = math.normalize(direction);

            ClientCommutator<BattleChangeBallDirectionEvent>.Default +=
                new BattleChangeBallDirectionEvent(sphereId, direction);
        }

        private static void SendLostEvent()
        {
            var ballLost = new BattleBallLostEvent();
            ballLost.RaiseEvent();
        }

        public void LateUpdate()
        {
            ref var channel = ref ClientCommutator<PhysicsContactEvent>.Default;
            if (!channel.HasNext)
            {
                if (CheckBallOutOfBounds())
                    SendLostEvent();
                return;
            }

            var world = _state.World;
            var ballIsLost = false;

            int readId = 0;
            while (channel.CanDrainQueue(readId))
            {
                ref readonly var message = ref channel.DrainQueueRef(ref readId);
                if (!world.Properties.TryGetValue(message.Body0, out var body0) ||
                    !world.Properties.TryGetValue(message.Body1, out var body1))
                    continue;

                body0.CallbackReceived?.Collide(body1.Layer, message.ContactPoint, message.Normal);
                body1.CallbackReceived?.Collide(body0.Layer, message.ContactPoint, message.Normal);

                if (!body0.Is(PongPhysicsLayer.Ball) && !body1.Is(PongPhysicsLayer.Ball))
                    continue;

                var collider = !body1.Is(PongPhysicsLayer.Ball) ? body1 : body0;
                var (ballId, colliderId) = (
                    body0.Is(PongPhysicsLayer.Ball) ? message.Body0 : message.Body1,
                    !body1.Is(PongPhysicsLayer.Ball) ? message.Body1 : message.Body0
                );

                if (collider.Is(PongPhysicsLayer.Platform))
                {
                    if (_state.PhysicalIdToPlayerId.TryGetValue(colliderId, out var playerId))
                    {
                        _state.Scores[playerId] += _globalState.BaseScoreIncrement;
                        if (playerId == _state.MyId)
                            ClientCommutator<BattleScoreUpdateEvent>.Default +=
                                new BattleScoreUpdateEvent(_state.Scores[playerId]);
                    }

                    ReflectPlatform(in message, ballId, colliderId);
                    continue;
                }

                if (collider.Is(PongPhysicsLayer.GameOverZone))
                {
                    ballIsLost = true;
                    continue;
                }

                if (collider.Is(PongPhysicsLayer.Wall))
                    ReflectBall(in message, ballId);
            }

            channel.ResetCounter();
            if (ballIsLost || CheckBallOutOfBounds())
                SendLostEvent();
        }

        private bool CheckBallOutOfBounds()
        {
            var world = _state.World;
            var spheres = world.Spheres;
            var bounds = world.Bounds;
            for (int i = 0; i < spheres.Length; i++)
            {
                var pos = spheres[i].Position;
                if (pos.x < bounds.x || pos.x > bounds.z || pos.y < bounds.y || pos.y > bounds.w)
                    return true;
            }

            return false;
        }

        public void PhysicsUpdate()
        {
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
        }
#endif
    }
}
