﻿using CrazyRam.Core.Framework;

namespace Pong.Systems.Battle
{
    public class AIPlaceholderSystem : IGameSystem
    {
        public void Init(ILevelState state)
        {
        }

        public void Dispose()
        {
        }

        public void Update()
        {
        }

        public void LateUpdate()
        {
        }

        public void PhysicsUpdate()
        {
        }

#if UNITY_EDITOR

        public void OnDrawGizmos()
        {
        }

#endif
    }
}
