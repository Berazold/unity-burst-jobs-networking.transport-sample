﻿using System.Runtime.CompilerServices;
using Pong.Systems.Battle;
using Pong.Systems.Battle.Physics;
using Unity.Mathematics;
using UnityEngine;

namespace Pong.Visual
{
    public abstract class VisualModelBase : MonoBehaviour
    {
        [SerializeField]
        protected ICollisionCallbackReceived CollisionCallback;

        public ICollisionCallbackReceived CallbackReceived
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => CollisionCallback;
        }

        public virtual void Dispose()
        {
            Destroy(gameObject);
        }

        public virtual void Place(float2 position)
        {
            transform.position = (Vector2) position;
        }

        public virtual void Refresh(VisualWorld world)
        {
        }
    }
}
