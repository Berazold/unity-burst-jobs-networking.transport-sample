﻿using System.Runtime.CompilerServices;
using Unity.Mathematics;
using UnityEngine;

namespace Pong.Visual
{
    public class BallVisualBase : VisualModelBase
    {
        protected float2 Forward;

        private float _radius;

        public float Radius
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _radius;
        }

        public virtual void Init(float2 position)
        {
            transform.position = (Vector2) position;
        }

        public virtual void FitSize(float radius)
        {
            _radius = radius;
            transform.localScale = Vector3.one * radius;
        }

        public virtual void Face(float2 direction)
        {
            Forward = direction;
        }
    }
}
