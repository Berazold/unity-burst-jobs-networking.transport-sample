﻿using Pong.Systems.Battle;
using Unity.Mathematics;
using UnityEngine;

namespace Pong.Visual
{
    public class InkTrailsBall : BallVisualBase
    {
        [SerializeField]
        private Transform _root;

        [SerializeField]
        private GpuInkTrail _trail;

        public override void Init(float2 position)
        {
            base.Init(position);
            _trail.Init(_root);
        }

        public override void Refresh(VisualWorld world)
        {
            base.Refresh(world);
            _trail.Refresh();
        }

        public override void Dispose()
        {
            base.Dispose();
            _trail.Dispose();
        }
    }
}
