﻿using Pong.Visual;
using UnityEngine;

namespace Pong.Systems.Battle
{
    public class VisualWorld
    {
        public readonly PlatformBase[] Platforms;

        public readonly BallVisualBase[] BallVisuals;

        public readonly ObstacleBase[] Obstacles;

        public readonly EnvironmentBase Environment;

        public VisualWorld(int platforms, int balls, int obstacles, EnvironmentBase environment)
        {
            Environment = Object.Instantiate(environment);
            Environment.Init();

            Platforms = new PlatformBase[platforms];
            BallVisuals = new BallVisualBase[balls];
            Obstacles = new ObstacleBase[obstacles];
        }

        public void Dispose()
        {
            for (int i = 0; i < Platforms.Length; i++)
            {
                if (Platforms[i])
                    Platforms[i].Dispose();
            }

            for (int i = 0; i < BallVisuals.Length; i++)
            {
                if (BallVisuals[i])
                    BallVisuals[i].Dispose();
            }

            for (int i = 0; i < Obstacles.Length; i++)
            {
                if (Obstacles[i])
                    Obstacles[i].Dispose();
            }

            if (Environment)
                Environment.Dispose();
        }
    }
}
