﻿using Unity.Mathematics;

namespace Pong.Systems.Battle.Physics
{
    public interface ICollisionCallbackReceived
    {
        void Collide(PongPhysicsLayer layer, float2 contactPoint, float2 normal);
    }
}
