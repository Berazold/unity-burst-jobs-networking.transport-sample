﻿namespace Pong.Systems.Battle.Physics
{
    public readonly struct PhysicalProperties
    {
        public readonly PongPhysicsLayer Layer;

        public readonly ICollisionCallbackReceived CallbackReceived;

        public PhysicalProperties(PongPhysicsLayer layer, ICollisionCallbackReceived callbackReceived)
        {
            Layer = layer;
            CallbackReceived = callbackReceived;
        }
    }
}
