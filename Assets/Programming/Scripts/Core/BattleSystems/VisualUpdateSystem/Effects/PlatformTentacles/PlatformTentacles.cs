﻿using System;
using CrazyRam.Core.Helpers;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

public class PlatformTentacles : MonoBehaviour
{
    [Serializable]
    private struct CommonSetup
    {
        public int2 Tentacles;

        public float2 LocalArea;

        public float AreaRadius;

        public float2 DirectionAngles;

        public int Segments;

        public float2 MinMaxLength;

        public float2 MinMaxWidth;
    }

    [Serializable]
    private struct MotionSetup
    {
        public float2 MotionScale;

        public float2 Amplitude;

        public float MaxSinOffset;

        public AnimationCurve MotionResistance;
    }

    private readonly struct TentacleVertex
    {
        // xy - position z - motion scale, w - amplitude
        public readonly half4 Position;

        // xy - uv, z - movement factor, w - sin offset
        public readonly half4 Uv;

        public TentacleVertex(float2 position, float2 uv, float2 motion, float movementFactor, float sinOffset)
        {
            Position = new half4((half2) position, (half2) motion);
            Uv = new half4((half2) uv, new half2((half) movementFactor, (half) sinOffset));
        }
    }

    [SerializeField]
    private MeshFilter _filter;

    [SerializeField]
    private CommonSetup _commonSetup;

    [SerializeField]
    private MotionSetup _motionSetup;

    [SerializeField]
    private AnimationCurve _sizeCurve;

    private Mesh _mesh;

    private const MeshUpdateFlags UpdateFlags = MeshUpdateFlags.DontRecalculateBounds |
                                                MeshUpdateFlags.DontValidateIndices |
                                                MeshUpdateFlags.DontResetBoneBounds;

    public void Init()
    {
        _mesh = BuildTentacleMesh();
        _filter.sharedMesh = _mesh;
    }

    public void Dispose()
    {
        if (_mesh)
            Destroy(_mesh);
    }

    private void PushTentacle(NativeArray<TentacleVertex> vertices, int segments, float2 start, float2 size,
        float2 direction, float directionSide, int vertexOffset, float sinOffset)
    {
        var end = start + direction * size.x;

        var tangent = new float2(-direction.y, direction.x) * size.y;

        var motionScale = math.lerp(_motionSetup.MotionScale.x, _motionSetup.MotionScale.y, Random.value);
        var amplitude = math.lerp(_motionSetup.Amplitude.x, _motionSetup.Amplitude.y, Random.value);
        var motionParams = new float2(motionScale, amplitude);
        sinOffset += Random.value * _motionSetup.MaxSinOffset;
        for (int i = 0; i < segments; i++)
        {
            float factor = i / (float) segments;
            var pos = math.lerp(start, end, factor);
            var height = _sizeCurve.Evaluate(1 - factor);

            var movementFactor = _motionSetup.MotionResistance.Evaluate(factor) * directionSide;

            vertices[(i << 1) + vertexOffset] = new TentacleVertex(
                math.mad(-height, tangent, pos),
                new float2(factor, 0),
                motionParams,
                movementFactor,
                sinOffset
            );

            vertices[(i << 1) + 1 + vertexOffset] = new TentacleVertex(
                math.mad(height, tangent, pos),
                new float2(factor, 1),
                motionParams,
                movementFactor,
                sinOffset
            );
        }
    }

    private static float2 RandomDirection(float2 angleBounds, float2 forward)
    {
        var angle = math.radians(math.lerp(angleBounds.x, angleBounds.y, Random.value));

        math.sincos(angle, out var sin, out var cos);
        var direction = new float2(
            cos * forward.x - sin * forward.y,
            sin * forward.x + cos * forward.y);

        direction = math.normalizesafe(direction);

        return direction;
    }

    private static void RandomizeTentacleParams(float2 forward, ref CommonSetup setup, out float2 pos,
        out float2 direction, out float2 size)
    {
        direction = RandomDirection(setup.DirectionAngles, forward);
        pos = math.mad(Random.insideUnitCircle, setup.AreaRadius, setup.LocalArea * forward);

        size = new float2(
            math.lerp(setup.MinMaxLength.x, setup.MinMaxLength.y, Random.value),
            math.lerp(setup.MinMaxWidth.x, setup.MinMaxWidth.y, Random.value)
        );
    }

    private Mesh BuildTentacleMesh()
    {
        var mesh = new Mesh();

        var tentacles = Random.Range(_commonSetup.Tentacles.x, _commonSetup.Tentacles.y);

        var oneInstanceVertexCount = _commonSetup.Segments * 2;
        var vertexCount = oneInstanceVertexCount * tentacles * 2;
        var vertices = MemoryHelper.Temporal<TentacleVertex>(vertexCount, NativeArrayOptions.UninitializedMemory);

        for (int i = 0; i < tentacles; i++)
        {
            int offset = (i << 1) * oneInstanceVertexCount;

            var sinOffset = i / (float) tentacles;
            sinOffset = math.PI * 2 * sinOffset;

            RandomizeTentacleParams(new float2(1, 0), ref _commonSetup, out var pos, out var direction, out var size);
            PushTentacle(vertices, _commonSetup.Segments, pos, size, direction, -1, offset, sinOffset);

            RandomizeTentacleParams(new float2(-1, 0), ref _commonSetup, out pos, out direction, out size);
            PushTentacle(vertices, _commonSetup.Segments, pos, size, direction, 1, offset + oneInstanceVertexCount,
                sinOffset);
        }

        mesh.SetVertexBufferParams(vertexCount,
            new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float16, 4),
            new VertexAttributeDescriptor(VertexAttribute.TexCoord0, VertexAttributeFormat.Float16, 4));
        mesh.SetVertexBufferData(vertices, 0, 0, vertexCount);

        var oneInstanceIndicesCount = (_commonSetup.Segments - 1) * 6;
        var indicesCount = oneInstanceIndicesCount * tentacles * 2;
        var indices = MemoryHelper.Temporal<ushort>(indicesCount, NativeArrayOptions.ClearMemory);
        mesh.SetIndexBufferParams(indicesCount, IndexFormat.UInt16);

        var indexId = 0;
        int vertexOffset = 0;
        for (int tentacleId = 0; tentacleId < tentacles * 2; tentacleId++)
        {
            for (int i = 0; i < _commonSetup.Segments - 1; i++)
            {
                int vertIndex = vertexOffset + (i << 1);
                // int vertIndex = vertexOffset + (i * 2);

                indices[indexId++] = (ushort) vertIndex;
                indices[indexId++] = (ushort) (vertIndex + 2);
                indices[indexId++] = (ushort) (vertIndex + 1);

                indices[indexId++] = (ushort) (vertIndex + 1);
                indices[indexId++] = (ushort) (vertIndex + 2);
                indices[indexId++] = (ushort) (vertIndex + 3);
            }

            vertexOffset += oneInstanceVertexCount;
        }

        mesh.SetIndexBufferData(indices, 0, 0, indices.Length, UpdateFlags);
        mesh.SetSubMesh(0, new SubMeshDescriptor(0, indices.Length), UpdateFlags);
        mesh.UploadMeshData(true);

        vertices.Dispose();
        indices.Dispose();

        mesh.bounds = new Bounds(Vector3.zero, Vector3.one * 100);
        return mesh;
    }
}
