﻿using System;
using System.Runtime.CompilerServices;
using CrazyRam.Core.Helpers;
using CrazyRam.Core.MessageBus;
using Pong.Visual;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;
using RenderQueue = Pong.Visual.RenderQueue;

public class GpuInkTrail : MonoBehaviour, IRenderingExtension
{
    [Serializable]
    private struct ParticleParams
    {
        public float Lifetime;

        public float Speed;

        public Vector2 Size;

        public Color Color;

        //8, 16, 32
        public int SpawnOnce;

        public int Frames;

        public float LineSize;

        public Vector3 NoiseScale;

        [NonSerialized]
        public float InvSpawnCount;
    }

    private readonly struct KernelProperties
    {
        public readonly int Init;

        public readonly int Spawn;

        public readonly int Update;

        private const string InitKernelName = "Init";

        private const string SpawnKernelName = "Spawn";

        private const string UpdateKernelName = "Update";

        public KernelProperties(ComputeShader shader)
        {
            Init = shader ? shader.FindKernel(InitKernelName) : 0;
            Spawn = shader ? shader.FindKernel(SpawnKernelName) : 0;
            Update = shader ? shader.FindKernel(UpdateKernelName) : 0;
        }

        public KernelProperties(ComputeShader init, ComputeShader spawn, ComputeShader update)
        {
            Init = init ? init.FindKernel(InitKernelName) : 0;
            Spawn = spawn ? spawn.FindKernel(SpawnKernelName) : 0;
            Update = update ? update.FindKernel(UpdateKernelName) : 0;
        }
    }

    [SerializeField]
    private ParticleParams _particleParams;

    [SerializeField]
    private ComputeShader _initShader;

    [SerializeField]
    private ComputeShader _spawnShader;

    [SerializeField]
    private ComputeShader _updateShader;

    [SerializeField]
    private float _spawnDistance;

    [SerializeField]
    private Material _renderer;

    private Transform _spawner;

    private Vector2 _lastPos;

    private KernelProperties _kernels;

    private int _activeSpawnLine;

    private RenderTexture _particles;

    private RenderTexture _lastParticles;

    private int _updateState;

    private (RenderTexture active, RenderTexture history) ActualParticleBuffers
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        get => (_updateState & 1) == 0
            ? (_particles, _lastParticles)
            : (_lastParticles, _particles);
    }

    private MaterialPropertyBlock _properties;

    private static class ShaderProperties
    {
        public static readonly int LastParticleBuffer = Shader.PropertyToID("LastParticleBuffer");

        public static readonly int ParticleBuffer = Shader.PropertyToID("ParticleBuffer");

        public static readonly int SpawnDir = Shader.PropertyToID("SpawnDir");

        public static readonly int SpawnLine = Shader.PropertyToID("SpawnLine");

        public static readonly int SpawnParams = Shader.PropertyToID("SpawnParams");

        public static readonly int LifeTimeInv = Shader.PropertyToID("_LifeTimeInv");

        public static readonly int DeltaTime = Shader.PropertyToID("DeltaTime");

        public static readonly int NoiseScale = Shader.PropertyToID("NoiseScale");

        public static readonly int Speed = Shader.PropertyToID("Speed");

        public static readonly int RendererParticles = Shader.PropertyToID("_ParticleBuffer");

        public static readonly int Color = Shader.PropertyToID("_Color");

        public static readonly int PointsPerLine = Shader.PropertyToID("_PointsPerLine");

        public static readonly int LinesCount = Shader.PropertyToID("LinesCount");

        public static readonly int InvMapSize = Shader.PropertyToID("_InvMapSize");

        public static readonly int Size = Shader.PropertyToID("_Size");
    }

    private const float InitGroups = 4;

    private const float SpawnGroups = 8;

    private const float UpdateGroups = 4;

    public void Init(Transform target)
    {
        _spawner = target;
        _activeSpawnLine = 0;

        _kernels = new KernelProperties(_initShader, _spawnShader, _updateShader);

        _particleParams.InvSpawnCount = 1f / _particleParams.SpawnOnce;

        _particles = new RenderTexture(_particleParams.SpawnOnce, _particleParams.Frames, 0,
            GraphicsFormat.R16G16B16A16_SFloat)
        {
            enableRandomWrite = true,
            wrapMode = TextureWrapMode.Clamp,
            filterMode = FilterMode.Point,
            autoGenerateMips = false,
            isPowerOfTwo = true
        };
        _particles.Create();

        _lastPos = _spawner.position;

        _lastParticles = new RenderTexture(_particles.descriptor);
        _lastParticles.Create();

        _properties = new MaterialPropertyBlock();

        Init();

        var register = new RegisterRenderExtension(this, RenderQueue.AfterOpaque);
        register.RaiseEvent();
    }


    public void Dispose()
    {
        _particles.SafeRelease();
        _lastParticles.SafeRelease();

        var unregister = new UnregisterRenderExtension(this, RenderQueue.AfterOpaque);
        unregister.RaiseEvent();
    }

    public void Dispose(Camera cam)
    {
        Dispose();
    }

    public void Refresh()
    {
        if (!_spawner)
            return;
        var pos = (Vector2) _spawner.position;
        if ((pos - _lastPos).magnitude > _spawnDistance)
            Spawn();

        UpdateParticles();
    }

    private void Init()
    {
        var kernel = _kernels.Init;
        var (groupsX, groupsY) = (
            Mathf.CeilToInt(_particles.width / InitGroups),
            Mathf.CeilToInt(_particles.height / InitGroups)
        );
        _initShader.SetTexture(kernel, ShaderProperties.ParticleBuffer, _particles);
        _initShader.Dispatch(kernel, groupsX, groupsY, 1);

        _initShader.SetTexture(kernel, ShaderProperties.ParticleBuffer, _lastParticles);
        _initShader.Dispatch(kernel, groupsX, groupsY, 1);
    }

    private void Spawn()
    {
        int kernel = _kernels.Spawn;

        var (_, history) = ActualParticleBuffers;

        var pos = (Vector2) _spawner.position;
        var direction = -(pos - _lastPos).normalized;
        var tangent = new Vector2(-direction.y, direction.x);
        _lastPos = pos;

        var spawnerScale = _spawner.localScale;
        var scale = math.max(spawnerScale.x, spawnerScale.y) * 0.5f * _particleParams.LineSize;
        var spawnFrom = pos - tangent * scale;
        var spawnTo = pos + tangent * scale;

        var spawnDirection = new Vector4(spawnFrom.x, spawnFrom.y, spawnTo.x, spawnTo.y);
        var groups = Mathf.CeilToInt(_particles.width / SpawnGroups);

        _spawnShader.SetTexture(kernel, ShaderProperties.ParticleBuffer, history);
        _spawnShader.SetVector(ShaderProperties.SpawnDir, spawnDirection);
        _spawnShader.SetVector(ShaderProperties.SpawnParams, new Vector4(
            _particleParams.Lifetime,
            _particleParams.InvSpawnCount,
            direction.x,
            direction.y
        ));
        _spawnShader.SetInt(ShaderProperties.SpawnLine, _activeSpawnLine);

        _spawnShader.Dispatch(kernel, groups, 1, 1);

        _activeSpawnLine = (_activeSpawnLine + 1) % _particleParams.Frames;
    }

    private void UpdateParticles()
    {
        if (!_particles || !_lastParticles)
            return;

        var kernel = _kernels.Update;
        var (groupsX, groupsY) = (
            Mathf.CeilToInt(_particles.width / UpdateGroups),
            Mathf.CeilToInt(_particles.height / UpdateGroups)
        );
        var (active, history) = ActualParticleBuffers;

        _updateShader.SetTexture(kernel, ShaderProperties.ParticleBuffer, active);
        _updateShader.SetTexture(kernel, ShaderProperties.LastParticleBuffer, history);
        _updateShader.SetFloat(ShaderProperties.DeltaTime, Time.deltaTime);
        _updateShader.SetFloat(ShaderProperties.Speed, _particleParams.Speed);
        _updateShader.SetVector(ShaderProperties.NoiseScale, _particleParams.NoiseScale);

        _updateShader.Dispatch(kernel, groupsX, groupsY, 1);

        _updateState++;
    }

    public void Populate(CommandBuffer buffer)
    {
        var (_, history) = ActualParticleBuffers;
        _properties.SetTexture(ShaderProperties.RendererParticles, history);

        _properties.SetColor(ShaderProperties.Color, _particleParams.Color);
        _properties.SetInt(ShaderProperties.PointsPerLine, _particleParams.SpawnOnce);
        _properties.SetFloat(ShaderProperties.LifeTimeInv, 1f / _particleParams.Lifetime);
        _properties.SetInt(ShaderProperties.LinesCount, _particleParams.Frames);
        _properties.SetVector(ShaderProperties.InvMapSize, new Vector4(1f / history.width, 1f / history.height));
        _properties.SetVector(ShaderProperties.Size, _particleParams.Size);

        var instances = _particles.width * _particles.height;

        buffer.DrawProcedural(Matrix4x4.identity, _renderer, 0, MeshTopology.Lines, 2, instances, _properties);
    }
}
