﻿using CrazyRam.Core.Helpers;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.UI;

public class InkLevelBounds : MonoBehaviour
{
    private struct UpdateJob : IJobParallelFor
    {
        public NativeArray<byte> Data;

        public float2 Scale;

        public float2 Offset;

        public float2 Tiling;

        public int Width;

        public int Height;

        public void Execute(int index)
        {
            var pos = new float2(index % Width, (int) (index / Height));
            pos = math.mad(pos, Scale, Offset);
            Data[index] = (byte) (noise.cnoise(pos) * byte.MaxValue);
        }
    }

    [SerializeField]
    private int2 _size;

    private Texture2D _texture;

    private NativeArray<byte> _tmpData;

    [SerializeField]
    private RawImage _image;

    [SerializeField]
    private float2 _noiseScale;

    [SerializeField]
    private float2 _noiseOffset;

    [SerializeField]
    private float2 _noiseTiling;

    private void Start()
    {
        _texture = new Texture2D(_size.x, _size.y, GraphicsFormat.R8_UNorm, 1, TextureCreationFlags.MipChain);

        _image.texture = _texture;
    }

    public void Update()
    {
        _tmpData = new NativeArray<byte>(_size.x * _size.y, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
        var job = new UpdateJob
        {
            Data = _tmpData,
            Width = _size.x,
            Height = _size.y,
            Offset = _noiseOffset,
            Scale = _noiseScale,
            Tiling = _noiseTiling
        };
        job.Run(_tmpData.Length);

        _texture.LoadRawTextureData(_tmpData);
        // _texture.Compress(true);
        _texture.Apply();
        _tmpData.Dispose();
    }

    private void OnDestroy()
    {
        _texture.SafeRelease();
        _tmpData.SafeRelease();
    }
}
