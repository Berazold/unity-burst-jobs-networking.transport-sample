﻿using Unity.Mathematics;
using UnityEngine;

namespace Pong.Visual
{
    public class ObstacleBase : VisualModelBase
    {
        public virtual void Init(float2 position, float2 forward)
        {
            var result = math.normalize(forward);
            var angle = math.atan2(result.y, result.x);
            angle = math.isnan(angle) ? 0 : angle;
            var rotation = Quaternion.Euler(0, 0, math.degrees(angle) - 90);

            transform.SetPositionAndRotation((Vector2) position, rotation);
        }
    }
}
