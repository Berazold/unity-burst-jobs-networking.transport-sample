﻿using System;
using CrazyRam.Core.Collections;
using CrazyRam.Core.Helpers;
using CrazyRam.Core.MessageBus;
using JetBrains.Annotations;
using Pong.Systems.Battle;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

namespace Pong.Visual
{
    public class TentacleWall : ObstacleBase, IRenderingExtension
    {
        [Serializable]
        private struct SinParams
        {
            public float2 MotionScale;

            public float2 Amplitude;

            public float RandomMotion => math.lerp(MotionScale.x, MotionScale.y, Random.value);
        }

        [Serializable]
        private struct PointsParams
        {
            public int Lines;

            public int PointsPerLine;

            public float2 MinMaxLength;

            public float2 SpawnXOffset;

            public float2 WidthFromTo;

            public float2 SpawnYDisplacement;

            public float SpawnStopLine;

            public float RandomXOffset => math.lerp(SpawnXOffset.x, SpawnXOffset.y, Random.value);

            public float RandomWidth => math.lerp(WidthFromTo.x, WidthFromTo.y, Random.value);

            public float RandomYOffset => math.lerp(SpawnYDisplacement.x, SpawnYDisplacement.y, Random.value);
        }

        private readonly struct Tentacle
        {
            public readonly NativeArray<float4> ControlPoints;

            public readonly float2 Center;

            public readonly float HalfLength;

            public Tentacle(NativeArray<float4> controlPoints, float2 center, float halfLength)
            {
                ControlPoints = controlPoints;
                Center = center;
                HalfLength = halfLength;
            }
        }

        private readonly struct TentacleVertex
        {
            [UsedImplicitly]
            public readonly half2 Position;

            [UsedImplicitly]
            public readonly half2 Uv;

            public TentacleVertex(float2 position, float2 uv)
            {
                Position = new half2(position);
                Uv = new half2(uv);
            }
        }

        [SerializeField]
        private PointsParams _pointsParams;

        [SerializeField]
        private float2 _bounds;

        [SerializeField]
        private SinParams _sinParams;

        [SerializeField]
        private Color _from;

        [SerializeField]
        private Color _to;

        [SerializeField]
        private Material _renderer;

        [SerializeField]
        private int _segments;

        [SerializeField]
        private float2 _activateDistance;

        [SerializeField]
        private float2 _tentacleActivateDistance;

        [SerializeField]
        private float2 _nodeBounds;

        [SerializeField]
        private float2 _nodeActivateDistance;

        [SerializeField]
        private float _nodeDecreaseRate;

        private Texture2D _positionMap;

        private ListChunk<int> _activeTentacles;

        private ListChunk<Tentacle> _tentacles;

        private Mesh _mesh;

        private MaterialPropertyBlock _properties;


        private const MeshUpdateFlags UpdateFlags = MeshUpdateFlags.DontRecalculateBounds |
                                                    MeshUpdateFlags.DontValidateIndices |
                                                    MeshUpdateFlags.DontResetBoneBounds;

        private static class ShaderProperties
        {
            public static readonly int ControlsMap = Shader.PropertyToID("_ControlsMap");

            public static readonly int InvControlsSize = Shader.PropertyToID("_InvControlsSize");

            public static readonly int ColorFrom = Shader.PropertyToID("_ColorFrom");

            public static readonly int ColorTo = Shader.PropertyToID("_ColorTo");
        }

        public override void Init(float2 position, float2 forward)
        {
            base.Init(position, forward);
            transform.SetPositionAndRotation((Vector2) position, Quaternion.identity);
            _mesh = BuildTentacleMesh(_segments, _pointsParams.PointsPerLine);

            const int tentaclesCapacity = 20;
            _activeTentacles = new ListChunk<int>(tentaclesCapacity);

            _tentacles = BuildTentacles(ref _pointsParams, ref _sinParams, ((float3) transform.position).xy, _bounds);
            _properties = new MaterialPropertyBlock();

            _positionMap = BuildPositinMap(_pointsParams.PointsPerLine, _tentacles.Count);

            var register = new RegisterRenderExtension(this, RenderQueue.AfterOpaque);
            register.RaiseEvent();
        }

        private static Texture2D BuildPositinMap(int width, int height)
        {
            var positionMap =
                new Texture2D(width, height, GraphicsFormat.R32G32B32A32_SFloat, TextureCreationFlags.None)
                {
                    wrapMode = TextureWrapMode.Clamp,
                    filterMode = FilterMode.Point
                };
            return positionMap;
        }

        private static ListChunk<Tentacle> BuildTentacles(ref PointsParams parameters, ref SinParams sinParams,
            float2 pos,
            float2 bounds)
        {
            const int tentaclesCapacity = 20;
            var tentacles = new ListChunk<Tentacle>(tentaclesCapacity);

            var minMaxLength = parameters.MinMaxLength;

            var xBounds = new float2(pos.x - bounds.x, pos.x + bounds.x);
            var yBounds = new float2(pos.y - bounds.y, pos.y + bounds.y);

            var lines = parameters.Lines;
            for (int i = 0; i < lines; i++)
            {
                var xPos = math.lerp(xBounds.x, xBounds.y, i / (lines - 1f)) + parameters.RandomXOffset;

                var height = yBounds.x - parameters.RandomYOffset;
                var width = parameters.RandomWidth;
                while (height < yBounds.y - parameters.SpawnStopLine)
                {
                    var factor = Random.value;
                    var length = math.lerp(minMaxLength.x, minMaxLength.y, factor);

                    var controls = MemoryHelper.Persistent<float4>(parameters.PointsPerLine,
                        NativeArrayOptions.UninitializedMemory);

                    var amplitude = math.lerp(sinParams.Amplitude.x, sinParams.Amplitude.y,
                        math.saturate(factor + (Random.value - 0.5f) * 0.25f));
                    var motionScale = sinParams.RandomMotion;
                    var sinOffset = math.PI * Random.value;

                    for (int id = 0; id < controls.Length; id++)
                    {
                        var y = height + length * (id / (controls.Length - 1f));
                        float x = xPos + math.sin(y * motionScale + sinOffset + Time.realtimeSinceStartup) * amplitude;
                        controls[id] = new float4(x, y, 0, width);
                    }

                    tentacles.Push(new Tentacle(controls, controls[controls.Length / 2].xy, length * 0.5f));
                    height = controls[controls.Length - 1].y - parameters.RandomYOffset;
                }
            }

            return tentacles;
        }

        private static Mesh BuildTentacleMesh(int segments, int controlPoints)
        {
            var mesh = new Mesh();

            var vertexCount = segments * 2;
            var vertices = MemoryHelper.Temporal<TentacleVertex>(vertexCount, NativeArrayOptions.UninitializedMemory);

            for (int i = 0; i < segments; i++)
            {
                float norm = i / (float) segments;

                float interval = norm * (controlPoints - 3);
                float subInterval = interval - (int) interval;
                int index = (int) interval;

                vertices[i << 1] = new TentacleVertex(
                    new float2(subInterval, index),
                    new float2(norm, 0)
                );
                vertices[(i << 1) + 1] = new TentacleVertex(
                    new float2(subInterval, index),
                    new float2(norm, 1)
                );
            }

            mesh.SetVertexBufferParams(vertexCount,
                new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float16, 2),
                new VertexAttributeDescriptor(VertexAttribute.TexCoord0, VertexAttributeFormat.Float16, 2));
            mesh.SetVertexBufferData(vertices, 0, 0, vertexCount);

            var indicesCount = (segments - 1) * 6;
            mesh.SetIndexBufferParams(indicesCount, IndexFormat.UInt16);
            var indices = MemoryHelper.Temporal<ushort>(indicesCount, NativeArrayOptions.UninitializedMemory);
            var indexId = 0;
            for (int i = 0; i < segments - 1; i++)
            {
                int vertIndex = i * 2;

                indices[indexId++] = (ushort) vertIndex;
                indices[indexId++] = (ushort) (vertIndex + 2);
                indices[indexId++] = (ushort) (vertIndex + 1);

                indices[indexId++] = (ushort) (vertIndex + 1);
                indices[indexId++] = (ushort) (vertIndex + 2);
                indices[indexId++] = (ushort) (vertIndex + 3);
            }

            mesh.SetIndexBufferData(indices, 0, 0, indices.Length, UpdateFlags);
            mesh.SetSubMesh(0, new SubMeshDescriptor(0, indices.Length), UpdateFlags);
            mesh.UploadMeshData(true);

            vertices.Dispose();
            indices.Dispose();

            mesh.bounds = new Bounds(Vector3.zero, Vector3.one * 100);
            return mesh;
        }

        public override void Dispose()
        {
            base.Dispose();
            var unregister = new UnregisterRenderExtension(this, RenderQueue.AfterOpaque);
            unregister.RaiseEvent();

            if (_mesh)
                Destroy(_mesh);
            for (int i = 0; i < _tentacles.Count; i++)
                _tentacles[i].ControlPoints.Dispose();
            _positionMap.SafeRelease();
        }

        public override void Refresh(VisualWorld world)
        {
            var bounds = _bounds;
            var pos = ((float3) transform.position).xy;
            var localBounds = new float4(pos - bounds - _activateDistance, pos + bounds + _activateDistance);

            //todo use jobs
            UpdateActiveTentacles();
            var spheres = world.BallVisuals;
            for (int i = 0; i < spheres.Length; i++)
            {
                var point = ((float3) spheres[i].transform.position).xy;
                ProcessInputPoint(point, localBounds);
            }

            var platforms = world.Platforms;
            for (int i = 0; i < platforms.Length; i++)
            {
                var platfrom = platforms[i];
                var point = platfrom.Left;
                var right = platfrom.Right;

                if (math.distancesq(pos, right) < math.distancesq(pos, point))
                    point = right;
                ProcessInputPoint(point, localBounds);
            }

            RemoveDeadTentacles();
            UpdateRenderParams();
        }

        private void UpdateActiveTentacles()
        {
            for (int i = 0; i < _activeTentacles.Count; i++)
            {
                var tentacleId = _activeTentacles[i];
                ref readonly var tentacle = ref _tentacles.PointerTo(tentacleId);
                DecreaseTentacleSize(tentacle.ControlPoints);
            }
        }

        private void ProcessInputPoint(float2 position, float4 bounds)
        {
            if (!PointInsideBounds(position, bounds))
                return;
            for (int i = 0; i < _tentacles.Count; i++)
            {
                ref readonly var tentacle = ref _tentacles.PointerTo(i);
                UpdateTentacleAt(i, in tentacle, position);
            }
        }

        private void RemoveDeadTentacles()
        {
            for (int i = 0; i < _activeTentacles.Count; i++)
            {
                var tentacleId = _activeTentacles[i];
                ref readonly var tentacle = ref _tentacles.PointerTo(tentacleId);
                if (!CheckTentacleDead(tentacle.ControlPoints))
                    continue;
                _activeTentacles.FastRemoveAt(i);
                i--;
            }
        }

        private void UpdateRenderParams()
        {
            var tentaclesCount = _activeTentacles.Count;
            if (_activeTentacles.Count == 0)
                return;

            unsafe
            {
                const int elements = 4;
                const int stride = sizeof(float) * elements;
                var bufferByteSize = stride * _pointsParams.PointsPerLine;

                var rawPoints = MemoryHelper.Temporal<float4>(_pointsParams.PointsPerLine * _tentacles.Count,
                    NativeArrayOptions.UninitializedMemory);
                var pointsPtr = rawPoints.GetUnsafePtr();
                for (int aliveId = 0; aliveId < tentaclesCount; aliveId++)
                {
                    var tentacleId = _activeTentacles[aliveId];
                    var buffer = _tentacles[tentacleId].ControlPoints;
                    var offset = bufferByteSize * aliveId;

                    UnsafeUtility.MemCpy((void*) ((IntPtr) pointsPtr + offset), buffer.GetUnsafeReadOnlyPtr(),
                        bufferByteSize);
                }

                _positionMap.LoadRawTextureData(rawPoints);
                _positionMap.Apply();
                rawPoints.Dispose();
            }
        }

        private bool CheckTentacleDead(NativeArray<float4> controls)
        {
            for (int i = 2; i < controls.Length - 2; i++)
            {
                if (controls[i].z > float.Epsilon)
                    return false;
            }

            return true;
        }

        private void DecreaseTentacleSize(NativeArray<float4> controls)
        {
            var decreaseRate = Time.deltaTime * _nodeDecreaseRate;
            for (int i = 2; i < controls.Length - 2; i++)
            {
                var point = controls[i];
                point.z = math.saturate(point.z - decreaseRate);
                controls[i] = point;
            }
        }

        private void UpdateTentacleAt(int id, in Tentacle tentacle, float2 position)
        {
            var rootUp = tentacle.Center + new float2(tentacle.HalfLength);
            var rootBottom = tentacle.Center - new float2(tentacle.HalfLength);

            var bounds = new float4(
                rootBottom - _tentacleActivateDistance,
                rootUp + _tentacleActivateDistance
            );

            if (!PointInsideBounds(position, bounds) || !UpdateTentacle(tentacle.ControlPoints, position))
                return;
            if (_activeTentacles.IndexOf(id) < 0)
                _activeTentacles.Push(id);
        }

        private bool UpdateTentacle(NativeArray<float4> controls, float2 pos)
        {
            var activateDistance = _nodeActivateDistance;
            var activateLength = _nodeActivateDistance.y - _nodeActivateDistance.x;
            var active = false;
            for (int i = 2; i < controls.Length - 2; i++)
            {
                var point = controls[i];

                var bounds = new float4(point.xy - _nodeBounds, point.xy + _nodeBounds);
                if (!PointInsideBounds(pos, bounds))
                    continue;
                var distance = math.lengthsq(point.xy - pos);
                var factor = (math.clamp(distance, activateDistance.x, activateDistance.y)
                              - activateDistance.x) / activateLength;
                factor = 1 - factor;

                point.z = math.max(point.z, factor);
                controls[i] = point;
                active = true;
            }

            return active;
        }

        private static bool PointInsideBounds(float2 position, float4 bounds)
        {
            return position.x >= bounds.x && position.x <= bounds.z &&
                   position.y >= bounds.y && position.y <= bounds.w;
        }

        public void Dispose(Camera cam)
        {
        }

        public void Populate(CommandBuffer buffer)
        {
            var texel = new float2(1f / _positionMap.width, 1f / _positionMap.height);
            var invSize = new Vector4(texel.x, texel.y, texel.x * 0.5f, texel.y * 0.5f);
            _properties.SetTexture(ShaderProperties.ControlsMap, _positionMap);
            _properties.SetVector(ShaderProperties.InvControlsSize, invSize);
            _properties.SetColor(ShaderProperties.ColorFrom, _from);
            _properties.SetColor(ShaderProperties.ColorTo, _to);

            if (_activeTentacles.Count > 0)
                buffer.DrawMeshInstancedProcedural(_mesh, 0, _renderer, 0, _activeTentacles.Count, _properties);
        }

#if UNITY_EDITOR

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            var position = transform.position;
            Gizmos.DrawWireCube(position, (Vector2) _bounds * 2);
            Gizmos.DrawWireCube(position, (Vector2) (_bounds + _activateDistance) * 2);


            Gizmos.color = Color.green;
            var pos = ((float3) position).xy;
            var xFromTo = new float2(pos.x - _bounds.x, pos.x + _bounds.x);

            var lines = _pointsParams.Lines;
            for (int i = 0; i < lines; i++)
            {
                var factor = i / (lines - 1f);

                var center = new Vector3(
                    math.lerp(xFromTo.x, xFromTo.y, factor),
                    pos.y
                );
                Gizmos.DrawWireSphere(center, 0.1f);
            }

            if (_tentacles == null || _tentacles.Count == 0)
                return;

            for (int tentacleId = 0; tentacleId < _tentacles.Count; tentacleId++)
            {
                var points = _tentacles[tentacleId].ControlPoints;

                for (int i = 0; i < points.Length - 1; i++)
                    Gizmos.DrawLine((Vector2) points[i].xy, (Vector2) points[i + 1].xy);
            }
        }

#endif
    }
}
