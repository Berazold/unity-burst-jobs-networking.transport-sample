﻿using CrazyRam.Core.Framework;
using CrazyRam.Core.Helpers;

namespace Pong.Systems.Battle
{
    public class VisualUpdateSystem : IGameSystem
    {
        private BattleState _state;

        public void Init(ILevelState state)
        {
            _state = ((GlobalState) state).BattleState;
        }

        public void Dispose()
        {
        }

        public void Update()
        {
        }

        public void LateUpdate()
        {
            var world = _state.World;
            var visualWorld = _state.VisualWorld;

            for (int i = 0; i < visualWorld.BallVisuals.Length; i++)
            {
                var ball = visualWorld.BallVisuals[i];
                if (!ball)
                    continue;
                ball.Place(world.Spheres[i].Position);
                ball.Face(world.Spheres[i].Direction);
                ball.Refresh(visualWorld);
            }

            for (int i = 0; i < visualWorld.Platforms.Length; i++)
            {
                var platform = visualWorld.Platforms[i];
                if (!platform)
                    continue;
                platform.Place(world.Platforms[i].Box.Position);
                platform.Refresh(visualWorld);
            }

            for (int i = 0; i < visualWorld.Obstacles.Length; i++)
                visualWorld.Obstacles[i].Ok()?.Refresh(visualWorld);

            visualWorld.Environment.Refresh(visualWorld);
        }

        public void PhysicsUpdate()
        {
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
        }
#endif
    }
}
