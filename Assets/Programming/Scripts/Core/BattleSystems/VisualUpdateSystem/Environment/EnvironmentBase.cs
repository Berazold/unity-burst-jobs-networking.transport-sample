﻿using Pong.Systems.Battle;
using UnityEngine;

namespace Pong.Visual
{
    public class EnvironmentBase : MonoBehaviour
    {
        public virtual void Init()
        {
        }

        public virtual void Dispose()
        {
            Destroy(gameObject);
        }

        public virtual void Refresh(VisualWorld world)
        {
        }
    }
}
