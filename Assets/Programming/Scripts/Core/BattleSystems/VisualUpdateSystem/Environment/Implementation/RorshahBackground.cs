﻿using CrazyRam.Core.MessageBus;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace Pong.Visual
{
    public class RorshahBackground : MonoBehaviour
    {
        [SerializeField]
        private MeshRenderer _renderer;

        [SerializeField]
        private Vector2Int _size;

        [SerializeField]
        private TextureSynthesisPipeline _pipeline;

        [SerializeField]
        private RenderTexture _texture;

        [SerializeField]
        private RorshahTextureData _data;

        [SerializeField]
        private Color _foregroundColor;

        private MaterialPropertyBlock _properties;

        private float _localTime;

        private float _nextTime;

        private const int MaxStartTimeOffset = 30;

        private static class ShaderProperties
        {
            public static readonly int Period = Shader.PropertyToID("_Period");

            public static readonly int Foreground = Shader.PropertyToID("_Foreground");

            public static readonly int Frame = Shader.PropertyToID("_Frame");
        }

        public void Init()
        {
            _texture = new RenderTexture(_size.x, _size.y, 0, GraphicsFormat.R8G8B8A8_UNorm)
            {
                antiAliasing = 1,
                filterMode = FilterMode.Bilinear,
                useMipMap = false
            };
            _texture.Create();

            _data.Time = Random.Range(1, MaxStartTimeOffset);

            _properties = new MaterialPropertyBlock();
            _properties.SetTexture(ShaderProperties.Frame, _texture);

            PushRequest();
        }

        private void OnDestroy()
        {
            if (_texture)
                _texture.Release();
        }

        private void PushRequest()
        {
            ClientCommutator<TextureSynthesisRequest>.Default +=
                new TextureSynthesisRequest(new TextureSynthesisBag(_texture, _data), _pipeline);
        }

        public void Refresh()
        {
            _localTime = _nextTime;
            _nextTime = (_localTime + Time.deltaTime * _data.Speed) % _data.Period;
            if (_nextTime < _localTime)
            {
                _data.Time += _data.Period;
                PushRequest();
            }

            _localTime = _nextTime;
            _properties.SetFloat(ShaderProperties.Period, _localTime);
            _properties.SetColor(ShaderProperties.Foreground, _foregroundColor);

            _renderer.SetPropertyBlock(_properties);
        }
    }
}
