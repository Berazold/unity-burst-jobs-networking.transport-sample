﻿using Pong.Systems.Battle;
using UnityEngine;

namespace Pong.Visual
{
    public class InkEnvironment : EnvironmentBase
    {
        [SerializeField]
        private RorshahBackground _rorshahBackground;

        private TextureSynthesisExtension _textureSynthesis;

        public override void Init()
        {
            base.Init();
            _textureSynthesis = new TextureSynthesisExtension();
            _rorshahBackground.Init();
        }

        public override void Dispose()
        {
            base.Dispose();
            _textureSynthesis.Dispose();
            _textureSynthesis = null;
        }

        public override void Refresh(VisualWorld world)
        {
            base.Refresh(world);
            _rorshahBackground.Refresh();
        }
    }
}
