﻿using System.Runtime.CompilerServices;
using Unity.Mathematics;
using UnityEngine;

namespace Pong.Visual
{
    public class PlatformBase : VisualModelBase
    {
        public virtual float2 Left
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => ((float3) transform.TransformPoint(new Vector3(-1, 0, 0))).xy;
        }

        public virtual float2 Right
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => ((float3) transform.TransformPoint(new Vector3(1, 0, 0))).xy;
        }

        public virtual void Init(float2 position, float2 forward)
        {
            var root = transform;
            var result = math.normalize(forward);
            var angle = math.atan2(result.y, result.x);
            angle = math.isnan(angle) ? 0 : angle;
            var rotation = Quaternion.Euler(0, 0, math.degrees(angle) - 90);
            root.SetPositionAndRotation((Vector2) position, rotation);
        }
    }
}
