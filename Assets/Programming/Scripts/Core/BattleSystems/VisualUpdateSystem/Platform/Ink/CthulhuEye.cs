﻿using Pong.Systems.Battle;
using Unity.Mathematics;
using UnityEngine;

public class CthulhuEye : MonoBehaviour
{
    [SerializeField]
    private MeshFilter _filter;

    [SerializeField]
    private MeshRenderer _renderer;

    [SerializeField]
    private int _segCount;

    [SerializeField]
    private float _radius;

    [SerializeField]
    private AnimationCurve _heightDynamic;

    [SerializeField]
    private int _platformSegments;

    [SerializeField]
    private float2 _scale;

    private Mesh _mesh;

    public void Init(MaterialPropertyBlock properties)
    {
        _mesh = GenerateShape(_segCount, _radius);
        _filter.sharedMesh = _mesh;
    }

    public void Dispose()
    {
        if (_mesh)
            Destroy(_mesh);
    }

    private Mesh GenerateShape(int segments, float radius)
    {
        var circleVertices = segments * 3;
        var platformSegments = _platformSegments * 6;
        var vertices = new Vector3[2 * circleVertices + platformSegments];
        var uvs = new Vector2[vertices.Length];
        var triangles = new int[vertices.Length];

        float offset = 0;
        float step = 2 * Mathf.PI / segments;

        int vertexId = 0;
        int uvsId = 0;

        var center = new Vector3(0, 0, 0);

        for (int i = 0; i < segments; i++)
        {
            float x = radius * Mathf.Cos(offset) + center.x;
            float y = radius * Mathf.Sin(offset);

            offset += step;

            var factor = x / (2 * radius) + 0.5f;
            var heightScale = _heightDynamic.Evaluate(1 - factor);

            vertices[vertexId++] = center;

            vertices[vertexId++] = new Vector3(x * _scale.x, y * heightScale, 0);

            uvs[uvsId++] = new Vector2(0.5f, 0.5f);
            uvs[uvsId++] = new Vector2(factor, y / (2 * radius) + 0.5f);

            x = radius * Mathf.Cos(offset) + center.x;
            y = radius * Mathf.Sin(offset);

            factor = x / (2 * radius) + 0.5f;
            heightScale = _heightDynamic.Evaluate(1 - factor);

            vertices[vertexId++] = new Vector3(x * _scale.x, y * heightScale, 0);
            uvs[uvsId++] = new Vector2(factor, y / (2 * radius) + 0.5f);
        }

        for (int i = 0; i < triangles.Length; i++)
            triangles[i] = i;

        var mesh = new Mesh();
        mesh.SetVertices(vertices);
        mesh.SetUVs(0, uvs);
        mesh.SetTriangles(triangles, 0);
        return mesh;
    }

    public void Refresh(VisualWorld world)
    {
    }

    public void SetProperties(MaterialPropertyBlock properties)
    {
        _renderer.SetPropertyBlock(properties);
    }
}
