﻿using Unity.Mathematics;
using UnityEngine;

public class CthulhuController : MonoBehaviour
{
    [SerializeField]
    private Material _material;

    [SerializeField]
    public Transform _target;

    [SerializeField]
    private float _eyeRotateRadius;

    [SerializeField]
    private float2 _closeDistance;

    [SerializeField]
    private float2 _blinkRange;

    [SerializeField]
    private float2 _blinkTime;

    [SerializeField]
    private float2 _followDistance;

    private State _state;

    private float _lastBlinkTime;

    private float _nextBlinkTime;

    private enum State
    {
        Concentrate = 0,
        Blink = 1
    }

    private static class ShaderProperties
    {
        public static readonly int ViewDir = Shader.PropertyToID("_ViewDir");

        public static readonly int Blink = Shader.PropertyToID("_Blink");
    }

    public void Update()
    {
        var direction = transform.position - _target.position;
        var distance = direction.magnitude;

        var eyeDir = direction.normalized * _eyeRotateRadius;

        Debug.Log(distance);

        var blink = (math.clamp(distance, _closeDistance.x, _closeDistance.y) - _closeDistance.x) /
                    (_closeDistance.y - _closeDistance.x);
        blink = math.lerp(_blinkRange.x, _blinkRange.y, blink);
        _material.SetFloat(ShaderProperties.Blink, blink);

        var follow = (math.clamp(distance, _followDistance.x, _followDistance.y) - _followDistance.x) /
                     (_followDistance.y - _followDistance.x);
        eyeDir *= (1 - follow);

        _material.SetVector(ShaderProperties.ViewDir, new Vector4(-eyeDir.x, eyeDir.y));
    }
}
