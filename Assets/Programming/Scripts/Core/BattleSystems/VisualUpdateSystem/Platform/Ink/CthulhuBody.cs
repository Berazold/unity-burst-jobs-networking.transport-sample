﻿using System.Runtime.CompilerServices;
using UnityEngine;

namespace Pong.Visual
{
    public class CthulhuBody : MonoBehaviour
    {
        [SerializeField]
        private MeshFilter _filter;

        [SerializeField]
        private int _segCount;

        [SerializeField]
        private float _height;

        [SerializeField]
        private AnimationCurve _heightDynamic;

        [SerializeField]
        private int _platformSegments;

        [SerializeField]
        private float _width;

        [SerializeField, Range(0, 1)]
        private float _breathingParts;

        private Mesh _mesh;

        public float Width
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _width;
        }

        public void Init()
        {
            _mesh = GenerateShape(_segCount, _height, _width);
            _filter.sharedMesh = _mesh;
        }

        public void Dispose()
        {
            if (_mesh)
                Destroy(_mesh);
        }

        private Mesh GenerateShape(int segments, float height, float width)
        {
            var circleVertices = segments * 3;
            var platformSegments = _platformSegments * 6;
            var vertices = new Vector3[2 * circleVertices + platformSegments];
            var triangles = new int[vertices.Length];

            float offset = -Mathf.PI / 2;
            float step = Mathf.PI / segments;

            int vertexId = 0;

            var radius = 0.5f * height;

            var realWidth = width - height;
            var center = new Vector3(realWidth / 2, 0, 0);

            for (int i = 0; i < segments; i++)
            {
                float x = radius * Mathf.Cos(offset) + center.x;
                float y = radius * Mathf.Sin(offset);

                offset += step;

                float factor = (x + _width * 0.5f) / _width;
                float heightOffset = _heightDynamic.Evaluate(1 - factor);

                var breath = factor <= _breathingParts || factor > (1 - _breathingParts) ? 1 : 0;

                vertices[vertexId++] = center;
                vertices[vertexId++] = new Vector3(x, y * heightOffset, breath);

                x = radius * Mathf.Cos(offset) + center.x;
                y = radius * Mathf.Sin(offset);

                factor = (x + _width * 0.5f) / _width;
                heightOffset = _heightDynamic.Evaluate(1 - factor);
                breath = factor <= _breathingParts || factor > (1 - _breathingParts) ? 1 : 0;

                vertices[vertexId++] = new Vector3(x, y * heightOffset, breath);
            }

            var segmentStep = realWidth / _platformSegments;
            for (int i = 0; i < _platformSegments; i++)
            {
                var oldX = center.x;

                var factor = (oldX + _width * 0.5f) / _width;
                var oldOffset = _heightDynamic.Evaluate(1 - factor);
                var oldBreath = factor <= _breathingParts || factor > (1 - _breathingParts) ? 1 : 0;

                vertices[vertexId++] = new Vector3(center.x, -radius * oldOffset, oldBreath);
                vertices[vertexId++] = new Vector3(center.x, radius * oldOffset, oldBreath);

                center.x -= segmentStep;

                factor = (center.x + _width * 0.5f) / _width;
                var heightOffset = _heightDynamic.Evaluate(1 - factor);
                var breath = factor <= _breathingParts || factor > (1 - _breathingParts) ? 1 : 0;

                vertices[vertexId++] = new Vector3(center.x, -radius * heightOffset, breath);

                vertices[vertexId++] = new Vector3(center.x, -radius * heightOffset, breath);
                vertices[vertexId++] = new Vector3(oldX, radius * oldOffset, oldBreath);
                vertices[vertexId++] = new Vector3(center.x, radius * heightOffset, breath);
            }

            offset = Mathf.PI / 2;
            center = new Vector3(-realWidth / 2, 0, 0);

            for (int i = 0; i < segments; i++)
            {
                float x = radius * Mathf.Cos(offset) + center.x;
                float y = radius * Mathf.Sin(offset);

                offset += step;

                float factor = (x + _width * 0.5f) / _width;
                float heightOffset = _heightDynamic.Evaluate(1 - factor);
                var breath = factor <= _breathingParts || factor > (1 - _breathingParts) ? 1 : 0;

                vertices[vertexId++] = center;
                vertices[vertexId++] = new Vector3(x, y * heightOffset, breath);

                x = radius * Mathf.Cos(offset) + center.x;
                y = radius * Mathf.Sin(offset);

                factor = (x + _width * 0.5f) / _width;
                heightOffset = _heightDynamic.Evaluate(1 - factor);
                breath = factor <= _breathingParts || factor > (1 - _breathingParts) ? 1 : 0;

                vertices[vertexId++] = new Vector3(x, y * heightOffset, breath);
            }

            for (int i = 0; i < triangles.Length; i++)
                triangles[i] = i;

            var mesh = new Mesh();
            mesh.SetVertices(vertices);
            mesh.SetTriangles(triangles, 0);
            return mesh;
        }
    }
}
