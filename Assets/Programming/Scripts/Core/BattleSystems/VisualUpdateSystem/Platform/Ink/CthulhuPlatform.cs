﻿using System.Runtime.CompilerServices;
using Pong.Systems.Battle;
using Unity.Mathematics;
using UnityEngine;

namespace Pong.Visual
{
    public class CthulhuPlatform : PlatformBase
    {
        [System.Serializable]
        private struct EyeParams
        {
            public float RotateRadius;

            public float2 CloseDistance;

            public float2 BlinkRange;

            public float2 FollowDistance;
        }

        [SerializeField]
        private CthulhuEye _eye;

        [SerializeField]
        private CthulhuBody _body;

        [SerializeField]
        private EyeParams _eyeParams;

        [SerializeField]
        private PlatformTentacles _tentacles;

        private MaterialPropertyBlock _eyeProperties;

        public override float2 Left
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => ((float3) transform.position - new float3(_body.Width * 0.5f, 0, 0)).xy;
        }

        public override float2 Right
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => ((float3) transform.position + new float3(_body.Width * 0.5f, 0, 0)).xy;
        }

        private static class ShaderProperties
        {
            public static readonly int ViewDir = Shader.PropertyToID("_ViewDir");

            public static readonly int Blink = Shader.PropertyToID("_Blink");
        }

        public override void Init(float2 position, float2 forward)
        {
            transform.position = (Vector2) position;
            _eyeProperties = new MaterialPropertyBlock();
            _eye.Init(_eyeProperties);
            _body.Init();
            _tentacles.Init();
        }

        public override void Refresh(VisualWorld world)
        {
            base.Refresh(world);
            _eye.Refresh(world);

            var ball = FindNearestBall(world);
            UpdateEye(ball);
        }

        public override void Dispose()
        {
            base.Dispose();
            _body.Dispose();
            _eye.Dispose();
            _tentacles.Dispose();
        }

        private BallVisualBase FindNearestBall(VisualWorld world)
        {
            int id = 0;
            float minDistance = float.MaxValue;
            var balls = world.BallVisuals;
            var pos = transform.position;
            for (int i = 0; i < balls.Length; i++)
            {
                var magnitude = (balls[i].transform.position - pos).sqrMagnitude;
                if (!(magnitude < minDistance))
                    continue;
                minDistance = magnitude;
                id = i;
            }

            return world.BallVisuals[id];
        }

        private void UpdateEye(BallVisualBase ball)
        {
            if (!ball)
                return;

            var direction = transform.position - ball.transform.position;
            var distance = direction.magnitude;

            var eyeDir = direction.normalized * _eyeParams.RotateRadius;

            var closeDist = _eyeParams.CloseDistance;
            var blink = (math.clamp(distance, closeDist.x, closeDist.y) - closeDist.x) / (closeDist.y - closeDist.x);
            blink = math.lerp(_eyeParams.BlinkRange.x, _eyeParams.BlinkRange.y, blink);

            var followDistance = _eyeParams.FollowDistance;
            var follow = (math.clamp(distance, followDistance.x, followDistance.y) - followDistance.x) /
                         (followDistance.y - followDistance.x);
            eyeDir *= (1 - follow);

            _eyeProperties.SetFloat(ShaderProperties.Blink, blink);
            _eyeProperties.SetVector(ShaderProperties.ViewDir, new Vector4(-eyeDir.x, eyeDir.y));

            _eye.SetProperties(_eyeProperties);
        }
    }
}
