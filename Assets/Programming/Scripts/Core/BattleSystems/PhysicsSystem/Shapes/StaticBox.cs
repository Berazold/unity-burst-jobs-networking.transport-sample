﻿using System.Runtime.InteropServices;

namespace Pong.Systems.Battle.Physics
{
    [StructLayout(LayoutKind.Sequential, Size = sizeof(uint) * 6)]
    public struct StaticBox
    {
        public Box Box;

        public bool IsTrigger;
    }
}
