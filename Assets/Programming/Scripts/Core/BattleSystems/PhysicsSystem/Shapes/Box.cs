﻿using Unity.Mathematics;

namespace Pong.Systems.Battle.Physics
{
    public struct Box
    {
        public float2 Position;

        public float2 Extents;

        public int PhysicsId;
    }
}
