﻿using Unity.Mathematics;

namespace Pong.Systems.Battle.Physics
{
    public struct Sphere
    {
        public float2 Position;

        public float2 Direction;

        public float Radius;

        public float Speed;

        public int PhysicsId;
    }
}
