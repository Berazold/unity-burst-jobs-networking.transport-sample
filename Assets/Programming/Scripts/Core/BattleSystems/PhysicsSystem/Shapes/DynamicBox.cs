﻿using Unity.Mathematics;

namespace Pong.Systems.Battle.Physics
{
    public struct DynamicBox
    {
        public Box Box;

        public float2 Direction;

        public float2 Forward;
    }
}
