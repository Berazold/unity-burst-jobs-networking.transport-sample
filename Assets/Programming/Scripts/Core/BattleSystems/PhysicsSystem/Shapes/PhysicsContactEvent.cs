﻿using Unity.Mathematics;

namespace Pong.Systems.Battle.Physics
{
    public readonly struct PhysicsContactEvent
    {
        public readonly int Body0;

        public readonly int Body1;

        public readonly float2 ContactPoint;

        public readonly float2 Normal;

        public PhysicsContactEvent(int body0, int body1, float2 contactPoint, float2 normal = default)
        {
            Body0 = body0;
            Body1 = body1;
            ContactPoint = contactPoint;
            Normal = normal;
        }

        public override string ToString()
        {
            return
                $"Contact info: {nameof(Body0)}: {Body0}, {nameof(Body1)}: {Body1}, {nameof(ContactPoint)}: {ContactPoint}, {nameof(Normal)}: {Normal}";
        }
    }
}
