﻿using System.Runtime.CompilerServices;
using Pong.Utils;
using Unity.Burst;
using Unity.Mathematics;

namespace Pong.Systems.Battle.Physics
{
    [BurstCompile]
    public static class BurstPhysicsUtils
    {
        [BurstCompile]
        public static bool BoxToBoxIntersection(float l0, float b0, float r0, float t0, float l1, float b1,
            float r1, float t1)
        {
            return math.all(new float4(l0, l1, b0, b1) < new float4(r1, r0, t1, t0));
        }
    }

    public static class PhysicsUtils
    {
        public static class MathematicsUtils
        {
            public static float2 Left(float2 v)
            {
                return new float2(-v.y, v.x);
            }

            public static float Cross(float2 a, float2 b)
            {
                return a.x * b.y - a.y * b.x;
            }
        }

        private static float3 VerticesToAxis(float2 a, float2 b)
        {
            var normal = math.normalize(MathematicsUtils.Left(b - a));
            return new float3(normal, math.dot(normal, b));
        }

        private static float LineCircleIntersection(float2 circlePos, float radius, float2 rayOrigin,
            float2 rayDirection)
        {
            var m = rayOrigin - circlePos;
            var b = math.dot(m, rayDirection);
            var c = math.dot(m, m) - radius * radius;

            float det = b * b - c;
            if (det < 0)
                return 0;
            var t = -b + math.sqrt(det);
            return t;
        }

        public static float2 RayToRayIntersection(float2 pos0, float2 direction0, float2 pos1, float2 direction1)
        {
            var delta = pos1 - pos0;
            var det = direction1.x * direction0.y - direction1.y * direction0.x;

            var u = (delta.y * direction1.x - delta.x * direction1.y) / det;
            return pos0 + direction0 * u;
        }

        private static unsafe void CircleToCircleCollision(Sphere* circle, float2 origin, float radius, int colliderId,
            bool isTrigger, NativeFixedList<PhysicsContactEvent> contacts)
        {
            var r = origin - circle->Position;
            float min = circle->Radius + radius;
            float distSq = math.lengthsq(r);

            if (distSq >= min * min)
                return;

            float dist = math.sqrt(distSq);
            float distInv = math.rcp(dist);

            var normal = distInv * r;
            var factor = LineCircleIntersection(circle->Position, circle->Radius, origin, circle->Direction);

            if (!isTrigger)
                circle->Position = circle->Position - circle->Direction * factor;
            var contact = new PhysicsContactEvent(circle->PhysicsId, colliderId, origin, -normal);
            contacts.Add(contact);
        }

        private static unsafe bool CalculatePenetration(Sphere* circle, Box* box, out float2 axisResult,
            out float2 axisFrom, out float2 axisTo)
        {
            var extents = new float4(box->Extents, -box->Extents);

            const int vertexCount = 4;
            var vertices = stackalloc float2[vertexCount];
            vertices[0] = box->Position + extents.zw;
            vertices[1] = box->Position + extents.zy;
            vertices[2] = box->Position + extents.xy;
            vertices[3] = box->Position + extents.xw;

            axisResult = default;
            var penetration = float.NegativeInfinity;
            axisFrom = default;
            axisTo = default;

            for (int i = 0; i < vertexCount; i++)
            {
                var a = vertices[i];
                var b = vertices[(i + 1) % vertexCount];
                var axis = VerticesToAxis(a, b);
                var dot = math.dot(axis.xy, circle->Position);
                var dist = dot - axis.z - circle->Radius;

                if (dist > 0)
                    return false;

                if (dist <= penetration)
                    continue;

                axisResult = axis.xy;
                penetration = dist;
                axisFrom = a;
                axisTo = b;
            }

            return true;
        }

        public static unsafe void CricleToBoxCollision(Sphere* circle, Box* box, bool isTrigger,
            NativeFixedList<PhysicsContactEvent> contacts)
        {
            if (!CalculatePenetration(circle, box, out var axis, out var u, out var v))
                return;

            float d = MathematicsUtils.Cross(axis, circle->Position);
            if (d > MathematicsUtils.Cross(axis, u))
            {
                CircleToCircleCollision(circle, u, 0, box->PhysicsId, isTrigger, contacts);
                return;
            }

            if (d < MathematicsUtils.Cross(axis, v))
            {
                CircleToCircleCollision(circle, v, 0, box->PhysicsId, isTrigger, contacts);
                return;
            }

            var edge = math.normalize(v - u);
            const float parallelDotFactor = 0.9999f;
            if (math.abs(math.dot(circle->Direction, edge)) > parallelDotFactor)
                return;

            var pointOnCircle = circle->Position - axis * circle->Radius;
            var contactPoint = RayToRayIntersection(pointOnCircle, circle->Direction, u, edge);

            if (!isTrigger)
                circle->Position = contactPoint + axis * circle->Radius;

            var contact = new PhysicsContactEvent(circle->PhysicsId, box->PhysicsId, contactPoint, axis);
            contacts.Add(contact);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static unsafe void DynamicBoxToStaticBoxCollision(DynamicBox* box, Box* obstacle, bool isTrigger,
            NativeFixedList<PhysicsContactEvent> contacts)
        {
            var box0Min = box->Box.Position - box->Box.Extents;
            var box0Max = box->Box.Position + box->Box.Extents;

            var box1Min = obstacle->Position - obstacle->Extents;
            var box1Max = obstacle->Position + obstacle->Extents;

            if (!BurstPhysicsUtils.BoxToBoxIntersection(
                box0Min.x, box0Min.y, box0Max.x, box0Max.y,
                box1Min.x, box1Min.y, box1Max.x, box1Max.y))
                return;

            var intersectMin = math.max(box0Min, box1Min);
            var intersectMax = math.min(box0Max, box1Max);

            var extents = 0.5f * (intersectMax - intersectMin);
            var pos = intersectMin + extents;

            var contactPoint = pos - box->Direction * extents;

            if (!isTrigger)
                box->Box.Position -= box->Direction * extents * 2;
            else
                return;

            var contact = new PhysicsContactEvent((&box->Box)->PhysicsId, obstacle->PhysicsId, contactPoint,
                -box->Direction);
            contacts.Add(contact);
        }
    }
}
