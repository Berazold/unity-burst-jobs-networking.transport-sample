﻿using System;
using CrazyRam.Core;
using CrazyRam.Core.Framework;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace Pong.Systems.Battle.Physics
{
    //todo SOA architecture -> better SIMD utilization
    public class PhysicsSystem : IGameSystem, IMessageReceiver<BattleForceResetPhysics>
    {
        private BattleState _state;

        private Option<JobHandle> _handle;

        private bool IsValid => _state != null && _state.World.IsValid;

        public void Init(ILevelState state)
        {
            _state = ((GlobalState) state).BattleState;

            ClientCommutator<BattleChangeBoxDirectionEvent>.Default.ResetCounter();
            ClientCommutator<BattleChangeBallDirectionEvent>.Default.ResetCounter();
            ClientDispatcher<BattleForceResetPhysics>.Default += this;
        }

        public void Dispose()
        {
            CompleteJobStep();
            ClientDispatcher<BattleForceResetPhysics>.Default -= this;
        }

        public unsafe void Update()
        {
            if (!IsValid || _state.State != MatchState.InProgress)
                return;

            var world = _state.World;
            var dt = Time.deltaTime;
            world.Contacts.Reset();

            ReadBoxInput();
            ReadSpheresInput();

            var boxForces = new ApplyBoxForceJob
            {
                Bodies = world.Platforms,
                Stride = sizeof(DynamicBox),
                DeltaTime = dt * world.PlatformSpeed,
            };

            var sphereForces = new ApplySphereForces
            {
                Bodies = world.Spheres,
                Stride = sizeof(Sphere),
                DeltaTime = dt
            };

            var boxToBoxContact = new DynamicBoxToStaticBoxContactDetectionJob
            {
                DynamicBoxes = world.Platforms,
                StaticBoxes = world.Obstacles,
                Contacts = world.Contacts
            };

            var sphereToObstacle = new SphereToStaticBoxContactDetectionJob
            {
                Boxes = world.Obstacles,
                Spheres = world.Spheres,
                Contacts = world.Contacts
            };

            var sphereToPlatform = new SphereToDynamicBoxContactDetectionJob
            {
                Boxes = world.Platforms,
                Spheres = world.Spheres,
                Contacts = world.Contacts
            };

            var handle = boxForces.Schedule(world.Platforms.Length, default);
            handle = sphereForces.Schedule(world.Spheres.Length, handle);
            handle = boxToBoxContact.Schedule(handle);
            handle = sphereToObstacle.Schedule(handle);
            handle = sphereToPlatform.Schedule(handle);

            _handle = handle.AsOption();
        }

        public void LateUpdate()
        {
            CompleteJobStep();
            if (!IsValid || _state.State != MatchState.InProgress)
                return;

            var world = _state.World;
            var contacts = world.Contacts;
            for (int i = 0; i < contacts.Count; i++)
                ClientCommutator<PhysicsContactEvent>.Default += contacts[i];
        }

        private void CompleteJobStep()
        {
            var (valid, handle) = _handle;
            if (valid && !handle.IsCompleted)
                handle.Complete();
            _handle = Option<JobHandle>.None;
        }

        private unsafe void ReadBoxInput()
        {
            var world = _state.World;
            ref var channel = ref ClientCommutator<BattleChangeBoxDirectionEvent>.Default;

            if (!channel.HasNext)
                return;
            int readId = 0;
            while (channel.CanDrainQueue(readId))
            {
                ref readonly var input = ref channel.DrainQueueRef(ref readId);
                if (input.Id < world.Platforms.Length)
                {
                    var size = UnsafeUtility.SizeOf<DynamicBox>();
                    var ptr = (DynamicBox*) math.mad(input.Id, size, (long) world.Platforms.GetUnsafePtr());
                    ptr->Direction = input.Direction;
                }
            }

            channel.ResetCounter();
        }

        private unsafe void ReadSpheresInput()
        {
            var world = _state.World;
            ref var channel = ref ClientCommutator<BattleChangeBallDirectionEvent>.Default;

            if (!channel.HasNext)
                return;
            ref readonly var input = ref channel.DrainRef();

            if (input.Id < world.Spheres.Length)
            {
                var size = UnsafeUtility.SizeOf<Sphere>();
                var ptr = (Sphere*) math.mad(input.Id, size, (long) world.Spheres.GetUnsafePtr());
                ptr->Direction = input.Direction;
            }

            channel.ResetCounter();
        }

        public void PhysicsUpdate()
        {
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
            if (!IsValid)
                return;

            var world = _state.World;

            Gizmos.color = Color.red;
            for (int i = 0; i < world.Spheres.Length; i++)
            {
                var sphere = world.Spheres[i];
                Gizmos.DrawWireSphere((Vector2) sphere.Position, sphere.Radius);

                Gizmos.DrawLine((Vector2) sphere.Position, (Vector2) (sphere.Position + sphere.Direction));
            }

            for (int i = 0; i < world.Obstacles.Length; i++)
            {
                var obstacle = world.Obstacles[i];
                Gizmos.color = obstacle.IsTrigger ? Color.cyan : Color.blue;
                Gizmos.DrawWireCube((Vector2) obstacle.Box.Position, (Vector2) obstacle.Box.Extents * 2);
            }

            Gizmos.color = Color.green;
            for (int i = 0; i < world.Platforms.Length; i++)
            {
                var platform = world.Platforms[i];
                Gizmos.DrawWireCube((Vector2) platform.Box.Position, (Vector2) platform.Box.Extents * 2);
            }
        }
#endif
        public unsafe void OnMessage(in BattleForceResetPhysics data)
        {
            CompleteJobStep();

            var world = _state.World;
            var platformModels = data.PlatformModels;
            var ballModels = data.BallModels;

            var platformSize = (long) UnsafeUtility.SizeOf<DynamicBox>();
            var platformsPtr = (long) (IntPtr) world.Platforms.GetUnsafePtr();

            for (int i = 0; i < world.Platforms.Length; i++)
            {
                var platform = (DynamicBox*) math.mad(i, platformSize, platformsPtr);
                (&platform->Box)->Position = platformModels[i].Position;
            }

            var sphereSize = (long) UnsafeUtility.SizeOf<Sphere>();
            var spheresPtr = (long) world.Spheres.GetUnsafePtr();
            for (int i = 0; i < world.Spheres.Length; i++)
            {
                var sphere = (Sphere*) math.mad(i, sphereSize, spheresPtr);
                sphere->Direction = data.BallDirections[i].xy;
                sphere->Radius = data.BallDirections[i].z;
                sphere->Position = ballModels[i].Position;
            }

            data.BallDirections.Dispose();
        }
    }
}
