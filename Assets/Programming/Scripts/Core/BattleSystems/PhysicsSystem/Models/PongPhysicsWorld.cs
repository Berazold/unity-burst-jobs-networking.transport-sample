﻿using System.Collections.Generic;
using CrazyRam.Core.Helpers;
using Pong.Utils;
using Unity.Collections;
using Unity.Mathematics;

namespace Pong.Systems.Battle.Physics
{
    public class PongPhysicsWorld
    {
        public NativeArray<DynamicBox> Platforms;

        public NativeArray<StaticBox> Obstacles;

        public NativeArray<Sphere> Spheres;

        public NativeFixedList<PhysicsContactEvent> Contacts;

        public Dictionary<int, PhysicalProperties> Properties;

        public float PlatformSpeed;

        public float MaxReflectAngle;

        public float4 Bounds;

        public bool IsValid => Platforms.IsCreated && Obstacles.IsCreated && Spheres.IsCreated && Contacts.IsCreated;

        public void Dispose()
        {
            Platforms.SafeRelease();
            Obstacles.SafeRelease();
            Spheres.SafeRelease();
            Contacts.SafeRelease();
        }
    }
}
