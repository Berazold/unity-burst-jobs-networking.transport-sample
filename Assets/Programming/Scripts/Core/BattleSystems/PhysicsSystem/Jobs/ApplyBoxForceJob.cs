﻿using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Mathematics;

namespace Pong.Systems.Battle.Physics
{
    [BurstCompile(DisableSafetyChecks = true)]
    public struct ApplyBoxForceJob : IJobFor
    {
        public NativeArray<DynamicBox> Bodies;

        public long Stride;

        public float DeltaTime;

        public unsafe void Execute(int index)
        {
            //todo x86 version?
            var body = (DynamicBox*) math.mad(index, Stride, (long) (IntPtr) Bodies.GetUnsafePtr());
            body->Box.Position = math.mad(body->Direction, DeltaTime, body->Box.Position);
        }
    }
}
