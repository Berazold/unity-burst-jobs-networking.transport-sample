﻿using System;
using Pong.Utils;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Mathematics;

namespace Pong.Systems.Battle.Physics
{
    [BurstCompile(DisableSafetyChecks = true)]
    public struct SphereToStaticBoxContactDetectionJob : IJob
    {
        [ReadOnly]
        public NativeArray<StaticBox> Boxes;

        public NativeArray<Sphere> Spheres;

        public NativeFixedList<PhysicsContactEvent> Contacts;

        public unsafe void Execute()
        {
            long sphereSize = sizeof(Sphere);
            long boxSize = sizeof(StaticBox);
            for (int sphereId = 0; sphereId < Spheres.Length; sphereId++)
            {
                var sphere = (Sphere*) math.mad(sphereId, sphereSize, (long) (IntPtr) Spheres.GetUnsafePtr());
                for (int boxId = 0; boxId < Boxes.Length; boxId++)
                {
                    var box = (StaticBox*) math.mad(boxId, boxSize, (long) (IntPtr) Boxes.GetUnsafeReadOnlyPtr());
                    PhysicsUtils.CricleToBoxCollision(sphere, &box->Box, box->IsTrigger, Contacts);
                }
            }
        }
    }
}
