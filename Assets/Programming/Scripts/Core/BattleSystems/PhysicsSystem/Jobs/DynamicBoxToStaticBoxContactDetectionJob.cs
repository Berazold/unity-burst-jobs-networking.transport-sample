﻿using System;
using Pong.Utils;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Mathematics;

namespace Pong.Systems.Battle.Physics
{
    [BurstCompile(DisableSafetyChecks = true)]
    public struct DynamicBoxToStaticBoxContactDetectionJob : IJob
    {
        [ReadOnly]
        public NativeArray<StaticBox> StaticBoxes;

        public NativeArray<DynamicBox> DynamicBoxes;

        public NativeFixedList<PhysicsContactEvent> Contacts;

        public unsafe void Execute()
        {
            long staticSize = sizeof(StaticBox);
            long dynamicSize = sizeof(DynamicBox);
            var dynamicBoxesPtr = (long) DynamicBoxes.GetUnsafePtr();
            var staticBoxesPtr = (long) StaticBoxes.GetUnsafeReadOnlyPtr();
            for (int dynamicId = 0; dynamicId < DynamicBoxes.Length; dynamicId++)
            {
                var dynamic = (DynamicBox*) math.mad(dynamicId, dynamicSize, dynamicBoxesPtr);
                for (int boxId = 0; boxId < StaticBoxes.Length; boxId++)
                {
                    var obstacle = (StaticBox*) math.mad(boxId, staticSize, staticBoxesPtr);
                    PhysicsUtils.DynamicBoxToStaticBoxCollision(dynamic, &obstacle->Box, obstacle->IsTrigger, Contacts);
                }
            }
        }
    }
}
