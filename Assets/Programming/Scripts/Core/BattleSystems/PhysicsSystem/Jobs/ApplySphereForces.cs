﻿using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Mathematics;

namespace Pong.Systems.Battle.Physics
{
    [BurstCompile(DisableSafetyChecks = true)]
    public struct ApplySphereForces : IJobFor
    {
        public NativeArray<Sphere> Bodies;

        public long Stride;

        public float DeltaTime;

        public unsafe void Execute(int index)
        {
            var body = (Sphere*) math.mad(index, Stride, (long) (IntPtr) Bodies.GetUnsafePtr());
            float dt = DeltaTime * body->Speed;
            body->Position = math.mad(body->Direction, dt, body->Position);
        }
    }
}
