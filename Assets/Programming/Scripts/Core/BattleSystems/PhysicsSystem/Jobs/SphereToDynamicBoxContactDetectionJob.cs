﻿using System;
using Pong.Utils;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Mathematics;

namespace Pong.Systems.Battle.Physics
{
    [BurstCompile(DisableSafetyChecks = true)]
    public struct SphereToDynamicBoxContactDetectionJob : IJob
    {
        [ReadOnly]
        public NativeArray<DynamicBox> Boxes;

        public NativeArray<Sphere> Spheres;

        public NativeFixedList<PhysicsContactEvent> Contacts;

        public unsafe void Execute()
        {
            long sphereSize = sizeof(Sphere);
            long boxSize = sizeof(DynamicBox);
            for (int sphereId = 0; sphereId < Spheres.Length; sphereId++)
            {
                var sphere = (Sphere*) math.mad(sphereId, sphereSize, (long) (IntPtr) Spheres.GetUnsafePtr());
                var radiusSq = sphere->Radius * sphere->Radius;
                for (int boxId = 0; boxId < Boxes.Length; boxId++)
                {
                    var box = (DynamicBox*) math.mad(boxId, boxSize, (long) (IntPtr) Boxes.GetUnsafeReadOnlyPtr());
                    PhysicsUtils.CricleToBoxCollision(sphere, &box->Box, false, Contacts);
                }
            }
        }
    }
}
