﻿using System.Collections.Generic;
using CrazyRam.Core.Framework;
using CrazyRam.Core.Helpers;
using CrazyRam.Core.MessageBus;
using UnityEngine.Rendering;

namespace Pong.Visual
{
    public class RenderExtensionSystem : IGameSystem, IMessageReceiver<RegisterRenderExtension>,
        IMessageReceiver<UnregisterRenderExtension>
    {
        private GlobalState _state;

        private List<IRenderingExtension>[] _extensions;

        private CommandBuffer[] _buffers;

        private const int BuffersCapacity = 4;

        private static readonly CameraEvent[] QueueToEvent =
        {
            CameraEvent.BeforeForwardOpaque,
            CameraEvent.AfterForwardOpaque,
            CameraEvent.BeforeForwardAlpha,
            CameraEvent.AfterForwardAlpha
        };

        public void Init(ILevelState state)
        {
            _state = (GlobalState) state;
            var camera = _state.SceneSetup.Camera;
            if (!camera)
                return;

            _buffers = new CommandBuffer[BuffersCapacity];
            for (int i = 0; i < _buffers.Length; i++)
            {
                var buffer = new CommandBuffer {name = ((RenderQueue) i).ToString()};
                camera.AddCommandBuffer(QueueToEvent[i], buffer);
                _buffers[i] = buffer;
            }

            _extensions = new List<IRenderingExtension>[BuffersCapacity];
            for (int i = 0; i < _extensions.Length; i++)
                _extensions[i] = new List<IRenderingExtension>();

            ClientDispatcher<RegisterRenderExtension>.Default += this;
            ClientDispatcher<UnregisterRenderExtension>.Default += this;
        }

        public void Dispose()
        {
            var camera = _state.SceneSetup.Camera;
            camera.Ok()?.RemoveAllCommandBuffers();
            FreeBuffers();
            _extensions = null;
            _buffers = null;

            ClientDispatcher<RegisterRenderExtension>.Default -= this;
            ClientDispatcher<UnregisterRenderExtension>.Default -= this;
        }

        public void Update()
        {
        }

        public void LateUpdate()
        {
            ClearBuffers();
            FillBuffers();
        }

        public void PhysicsUpdate()
        {
        }

        public void OnDrawGizmos()
        {
        }

        private void FreeBuffers()
        {
            for (int i = 0; i < _buffers.Length; i++)
                _buffers[i].Dispose();
        }

        private void ClearBuffers()
        {
            for (int i = 0; i < _buffers.Length; i++)
                _buffers[i].Clear();
        }

        private void FillBuffers()
        {
            for (int i = 0; i < _extensions.Length; i++)
            {
                var extensions = _extensions[i];
                var buffer = _buffers[i];
                for (int id = 0; id < extensions.Count; id++)
                    extensions[id].Populate(buffer);
            }
        }

        public void OnMessage(in RegisterRenderExtension data)
        {
            int queue = (int) data.Queue;
            if (_buffers.InBounds(queue) && _extensions.InBounds(queue))
                _extensions[queue].SafePush(data.Extension);
        }

        public void OnMessage(in UnregisterRenderExtension data)
        {
            int queue = (int) data.Queue;
            if (_buffers.InBounds(queue) && _extensions.InBounds(queue))
                _extensions[queue].SafeRemove(data.Extension);
        }
    }
}
