﻿using CrazyRam.Core.MessageBus;
using UnityEngine;
using UnityEngine.Rendering;

namespace Pong.Visual
{
    public class TextureSynthesisExtension : IRenderingExtension
    {
        public TextureSynthesisExtension()
        {
            var register = new RegisterRenderExtension(this, RenderQueue.BeforeOpaque);
            register.RaiseEvent();
        }

        public void Dispose()
        {
            var unregister = new UnregisterRenderExtension(this, RenderQueue.BeforeOpaque);
            unregister.RaiseEvent();
        }

        public void Dispose(Camera camera)
        {
        }

        public void Populate(CommandBuffer buffer)
        {
            ref var channel = ref ClientCommutator<TextureSynthesisRequest>.Default;

            if (!channel.HasNext)
                return;
            int readId = 0;
            while (channel.CanDrainQueue(readId))
            {
                ref readonly var message = ref channel.DrainQueueRef(ref readId);

                if (message.Pipeline)
                    message.Pipeline.Execute(buffer, in message.Bag);
            }

            channel.ResetCounter();
        }
    }
}
