﻿using UnityEngine.Rendering;

namespace Pong.Visual
{
    public interface ITextureSynthesisPipeline
    {
        void Execute(CommandBuffer buffer, in TextureSynthesisBag request);
    }
}
