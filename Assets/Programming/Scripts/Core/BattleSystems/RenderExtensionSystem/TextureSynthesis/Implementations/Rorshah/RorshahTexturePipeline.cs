﻿using UnityEngine;
using UnityEngine.Rendering;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Pong.Visual
{
    public class RorshahTexturePipeline : TextureSynthesisPipeline
    {
        [SerializeField]
        private Material _rorshahGenerator;

        private static class ShaderProperties
        {
            public static readonly int SimulationTime = Shader.PropertyToID("_SimulationTime");
        }

        public override void Execute(CommandBuffer buffer, in TextureSynthesisBag request)
        {
            buffer.SetRenderTarget(request.Target);

            if (!request.Target || !(request.Data is RorshahTextureData rorshahData))
                return;

            _rorshahGenerator.SetVector(ShaderProperties.SimulationTime,
                new Vector4(rorshahData.Time, rorshahData.Time + rorshahData.Period));
            buffer.DrawMesh(RenderUtils.FullscreenTriangle, Matrix4x4.identity, _rorshahGenerator, 0, 0);
        }

#if UNITY_EDITOR

        [MenuItem("Pong/Procedural/" + nameof(RorshahTexturePipeline))]
        public static void Create()
        {
            var asset = CreateInstance<RorshahTexturePipeline>();
            AssetDatabase.CreateAsset(asset, $"Assets/{nameof(RorshahTexturePipeline)}.asset");
        }

#endif
    }
}
