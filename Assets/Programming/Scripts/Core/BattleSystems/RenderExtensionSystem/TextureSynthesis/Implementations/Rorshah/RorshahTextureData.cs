﻿using System;

namespace Pong.Visual
{
    [Serializable]
    public class RorshahTextureData : ITextureSynthesisData
    {
        public float Speed;

        public float Time;

        public float Period;
    }
}
