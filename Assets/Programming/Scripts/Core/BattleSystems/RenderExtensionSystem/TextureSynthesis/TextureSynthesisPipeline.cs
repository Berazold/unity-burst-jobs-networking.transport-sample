﻿using UnityEngine;
using UnityEngine.Rendering;

namespace Pong.Visual
{
    public abstract class TextureSynthesisPipeline : ScriptableObject, ITextureSynthesisPipeline
    {
        public abstract void Execute(CommandBuffer buffer, in TextureSynthesisBag request);
    }
}
