﻿using UnityEngine;

namespace Pong.Visual
{
    public readonly struct TextureSynthesisBag
    {
        public readonly RenderTexture Target;

        public readonly ITextureSynthesisData Data;

        public TextureSynthesisBag(RenderTexture target, ITextureSynthesisData data)
        {
            Target = target;
            Data = data;
        }
    }
}
