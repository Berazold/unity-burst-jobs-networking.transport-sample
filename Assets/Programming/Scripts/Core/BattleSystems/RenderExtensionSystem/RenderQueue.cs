﻿namespace Pong.Visual
{
    public enum RenderQueue
    {
        BeforeOpaque = 0,
        AfterOpaque = 1,
        BeforeTransparent = 2,
        AfterAll = 3
    }
}
