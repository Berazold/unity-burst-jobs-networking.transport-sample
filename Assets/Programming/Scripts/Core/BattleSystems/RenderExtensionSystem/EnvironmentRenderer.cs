﻿using UnityEngine;
using UnityEngine.Rendering;

namespace Pong.Visual
{
    public class EnvironmentRenderer : IRenderingExtension
    {
        public virtual void Dispose(Camera camera)
        {
        }

        public void Populate(CommandBuffer buffer)
        {
        }

        public virtual void Populate(Plane[] planes, Camera camera)
        {
        }
    }
}
