﻿using UnityEngine;
using UnityEngine.Rendering;

namespace Pong.Visual
{
    public interface IRenderingExtension
    {
        void Dispose(Camera camera);

        void Populate(CommandBuffer buffer);
    }
}
