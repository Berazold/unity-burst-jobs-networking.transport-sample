﻿using CrazyRam.Core.Framework;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using Pong.Systems.Battle.Physics;
using Unity.Mathematics;

namespace Pong.Systems.Battle
{
    public class AISystem : IGameSystem
    {
        private BattleState _state;

        public void Init(ILevelState state)
        {
            _state = ((GlobalState) state).BattleState;
        }

        public void Dispose()
        {
        }

        public void Update()
        {
            var ai = _state.AIPlayers;
            for (int i = 0; i < ai.Length; i++)
                UpdatePlayer(ai[i]);
        }

        public void LateUpdate()
        {
        }

        public void PhysicsUpdate()
        {
        }

        private void UpdatePlayer(int id)
        {
            var world = _state.World;
            var platform = world.Platforms[id];
            var ball = FindClosestBall(platform.Box.Position);
            var bounds = world.Bounds;

            var side = math.dot(platform.Forward, ball.Direction);
            var direction = float2.zero;

            var halfFieldSize = (math.abs(bounds.y) + math.abs(bounds.w)) * 0.5f;
            if (side < 0 && math.abs(platform.Box.Position.y - ball.Position.y) <= halfFieldSize)
                direction = PredictDirection(in platform, in ball);

            var directionMask = _state.PlatformParams[id].DirectionMask;
            direction = math.normalizesafe(direction * directionMask);

            ClientCommutator<BattleChangeBoxDirectionEvent>.Default += new BattleChangeBoxDirectionEvent(id, direction);
        }

        private static float2 PredictDirection(in DynamicBox platform, in Sphere ball)
        {
            var platformTangent = PhysicsUtils.MathematicsUtils.Left(platform.Forward);
            var platformPos = new float2(platform.Box.Position);
            var collisionPoint = PhysicsUtils.RayToRayIntersection(ball.Position, ball.Direction, platformPos,
                platformTangent);

            var extents = platform.Box.Extents;
            var localPos = (collisionPoint - platformPos) * math.abs(platformTangent);
            bool insideBounds = math.abs(localPos.x) < extents.x && math.abs(localPos.y) < extents.y;
            return insideBounds ? float2.zero : collisionPoint - platformPos;
        }

        private Sphere FindClosestBall(float2 pos)
        {
            var world = _state.World;
            float minDist = float.MaxValue;
            int id = 0;
            for (int i = 0; i < world.Spheres.Length; i++)
            {
                var dist = math.lengthsq(world.Spheres[i].Position - pos);
                if (dist >= minDist)
                    continue;
                minDist = dist;
                id = i;
            }

            return world.Spheres[id];
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
        }
#endif
    }
}
