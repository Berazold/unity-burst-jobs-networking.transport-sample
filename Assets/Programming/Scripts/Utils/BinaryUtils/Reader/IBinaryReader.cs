﻿using System;
using Unity.Mathematics;

namespace Pong.Utils.Serialization
{
    public interface IBinaryReader
    {
        byte ReadUint8(IntPtr buffer, out int readed);

        sbyte ReadInt8(IntPtr buffer, out int readed);

        ushort ReadUint16(IntPtr buffer, out int readed);

        short ReadInt16(IntPtr buffer, out int readed);

        uint ReadUint32(IntPtr buffer, out int readed);

        int ReadInt32(IntPtr buffer, out int readed);

        ulong ReadUint64(IntPtr buffer, out int readed);

        long ReadInt64(IntPtr buffer, out int readed);

        half ReadFloat16(IntPtr buffer, out int readed);

        float ReadFloat32(IntPtr buffer, out int readed);

        double ReadFloat64(IntPtr buffer, out int readed);
    }
}
