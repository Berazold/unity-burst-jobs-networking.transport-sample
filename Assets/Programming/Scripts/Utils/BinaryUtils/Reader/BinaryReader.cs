﻿using System;
using System.Runtime.CompilerServices;
using Unity.Mathematics;

namespace Pong.Utils.Serialization
{
    public struct BinaryReader : IBinaryReader
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte ReadUint8(IntPtr buffer, out int readed)
        {
            return ByteIO.ReadUnmanaged<byte>(buffer, out readed);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public sbyte ReadInt8(IntPtr buffer, out int readed)
        {
            return ByteIO.ReadUnmanaged<sbyte>(buffer, out readed);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ushort ReadUint16(IntPtr buffer, out int readed)
        {
            return ByteIO.ReadUnmanaged<ushort>(buffer, out readed);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public short ReadInt16(IntPtr buffer, out int readed)
        {
            return ByteIO.ReadUnmanaged<short>(buffer, out readed);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public uint ReadUint32(IntPtr buffer, out int readed)
        {
            return ByteIO.ReadUnmanaged<uint>(buffer, out readed);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int ReadInt32(IntPtr buffer, out int readed)
        {
            return ByteIO.ReadUnmanaged<int>(buffer, out readed);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ulong ReadUint64(IntPtr buffer, out int readed)
        {
            return ByteIO.ReadUnmanaged<ulong>(buffer, out readed);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public long ReadInt64(IntPtr buffer, out int readed)
        {
            return ByteIO.ReadUnmanaged<long>(buffer, out readed);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public half ReadFloat16(IntPtr buffer, out int readed)
        {
            return ByteIO.ReadUnmanaged<half>(buffer, out readed);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public float ReadFloat32(IntPtr buffer, out int readed)
        {
            return ByteIO.ReadUnmanaged<float>(buffer, out readed);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public double ReadFloat64(IntPtr buffer, out int readed)
        {
            return ByteIO.ReadUnmanaged<double>(buffer, out readed);
        }
    }
}
