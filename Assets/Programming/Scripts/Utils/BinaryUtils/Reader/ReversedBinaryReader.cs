﻿using System;
using Unity.Mathematics;

namespace Pong.Utils.Serialization
{
    public unsafe struct ReversedBinaryReader : IBinaryReader
    {
        public byte ReadUint8(IntPtr buffer, out int readed)
        {
            readed = sizeof(byte);
            return *(byte*) buffer;
        }

        public sbyte ReadInt8(IntPtr buffer, out int readed)
        {
            readed = sizeof(sbyte);
            return *(sbyte*) buffer;
        }

        public ushort ReadUint16(IntPtr buffer, out int readed)
        {
            readed = sizeof(ushort);

            ushort result = default;
            var ptr = (byte*) &result;
            ptr[1] = *(byte*) buffer;
            ptr[0] = *(byte*) (buffer + 1);
            return *ptr;
        }

        public short ReadInt16(IntPtr buffer, out int readed)
        {
            readed = sizeof(short);

            ushort result = default;
            var ptr = (byte*) &result;
            ptr[1] = *(byte*) buffer;
            ptr[0] = *(byte*) (buffer + 1);
            return *(short*) ptr;
        }

        public uint ReadUint32(IntPtr buffer, out int readed)
        {
            readed = sizeof(uint);

            uint result = default;
            var ptr = (byte*) &result;
            ptr[3] = *(byte*) buffer;
            ptr[2] = *(byte*) (buffer + 1);
            ptr[1] = *(byte*) (buffer + 2);
            ptr[0] = *(byte*) (buffer + 3);
            return *ptr;
        }

        public int ReadInt32(IntPtr buffer, out int readed)
        {
            readed = sizeof(int);

            uint result = default;
            var ptr = (byte*) &result;
            ptr[3] = *(byte*) buffer;
            ptr[2] = *(byte*) (buffer + 1);
            ptr[1] = *(byte*) (buffer + 2);
            ptr[0] = *(byte*) (buffer + 3);
            return *(int*) ptr;
        }

        public ulong ReadUint64(IntPtr buffer, out int readed)
        {
            readed = sizeof(ulong);

            ulong result = default;
            var ptr = (byte*) &result;
            ptr[7] = *(byte*) buffer;
            ptr[6] = *(byte*) (buffer + 1);
            ptr[5] = *(byte*) (buffer + 2);
            ptr[4] = *(byte*) (buffer + 3);
            ptr[3] = *(byte*) (buffer + 4);
            ptr[2] = *(byte*) (buffer + 5);
            ptr[1] = *(byte*) (buffer + 6);
            ptr[0] = *(byte*) (buffer + 7);
            return *ptr;
        }

        public long ReadInt64(IntPtr buffer, out int readed)
        {
            readed = sizeof(ulong);

            uint result = default;
            var ptr = (byte*) &result;
            ptr[7] = *(byte*) buffer;
            ptr[6] = *(byte*) (buffer + 1);
            ptr[5] = *(byte*) (buffer + 2);
            ptr[4] = *(byte*) (buffer + 3);
            ptr[3] = *(byte*) (buffer + 4);
            ptr[2] = *(byte*) (buffer + 5);
            ptr[1] = *(byte*) (buffer + 6);
            ptr[0] = *(byte*) (buffer + 7);
            return *(long*) ptr;
        }

        public half ReadFloat16(IntPtr buffer, out int readed)
        {
            readed = sizeof(ushort);

            ushort result = default;
            var ptr = (byte*) &result;
            ptr[1] = *(byte*) buffer;
            ptr[0] = *(byte*) (buffer + 1);
            return *(half*) ptr;
        }

        public float ReadFloat32(IntPtr buffer, out int readed)
        {
            readed = sizeof(uint);

            uint result = default;
            var ptr = (byte*) &result;
            ptr[3] = *(byte*) buffer;
            ptr[2] = *(byte*) (buffer + 1);
            ptr[1] = *(byte*) (buffer + 2);
            ptr[0] = *(byte*) (buffer + 3);
            return *(float*) ptr;
        }

        public double ReadFloat64(IntPtr buffer, out int readed)
        {
            readed = sizeof(ulong);

            ulong result = default;
            var ptr = (byte*) &result;
            ptr[7] = *(byte*) buffer;
            ptr[6] = *(byte*) (buffer + 1);
            ptr[5] = *(byte*) (buffer + 2);
            ptr[4] = *(byte*) (buffer + 3);
            ptr[3] = *(byte*) (buffer + 4);
            ptr[2] = *(byte*) (buffer + 5);
            ptr[1] = *(byte*) (buffer + 6);
            ptr[0] = *(byte*) (buffer + 7);
            return *(double*) ptr;
        }
    }
}
