﻿using System;
using System.Runtime.CompilerServices;

namespace Pong.Utils.Serialization
{
    public static class ByteIO
    {
        public static unsafe bool IsLittleEndian
        {
            get
            {
                uint n = 1;
                var ptr = (byte*) &n;
                return ptr[0] == 1;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe T ReadUnmanaged<T>(IntPtr buffer, out int offset) where T : unmanaged
        {
            var result = *(T*) buffer;
            offset = sizeof(T);
            return result;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe int WriteUnmanaged<T>(IntPtr buffer, T value) where T : unmanaged
        {
            *(T*) buffer = value;
            return sizeof(T);
        }

        public static unsafe ushort ByteSwap(ushort val)
        {
            var buffer = (IntPtr) (&val);

            ushort result = default;
            var ptr = (byte*) &result;
            ptr[0] = *(byte*) (buffer + 1);
            ptr[1] = *(byte*) buffer;
            return *(ushort*) ptr;
        }

        public static unsafe uint ByteSwap(uint val)
        {
            var buffer = (IntPtr) (&val);

            uint result = default;
            var ptr = (byte*) &result;
            ptr[3] = *(byte*) buffer;
            ptr[2] = *(byte*) (buffer + 1);
            ptr[1] = *(byte*) (buffer + 2);
            ptr[0] = *(byte*) (buffer + 3);
            return *ptr;
        }

#region BitConversion

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe TOutput ConvertTo<TOutput, TInput>(TInput value)
            where TOutput : unmanaged where TInput : unmanaged => *((TOutput*) &value);

#endregion
    }
}
