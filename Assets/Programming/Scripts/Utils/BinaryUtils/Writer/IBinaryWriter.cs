﻿using System;
using Unity.Mathematics;

namespace Pong.Utils.Serialization
{
    public interface IBinaryWriter
    {
        int WriteUint8(byte value, IntPtr buffer);

        int WriteInt8(sbyte value, IntPtr buffer);

        int WriteUint16(ushort value, IntPtr buffer);

        int WriteInt16(short value, IntPtr buffer);

        int WriteUint32(uint value, IntPtr buffer);

        int WriteInt32(int value, IntPtr buffer);

        int WriteUint64(ulong value, IntPtr buffer);

        int WriteInt64(long value, IntPtr buffer);

        int WriteFloat16(half value, IntPtr buffer);

        int WriteFloat32(float value, IntPtr buffer);

        int WriteFloat64(double value, IntPtr buffer);
    }
}
