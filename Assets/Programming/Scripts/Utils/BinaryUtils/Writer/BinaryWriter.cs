﻿using System;
using System.Runtime.CompilerServices;
using Unity.Mathematics;

namespace Pong.Utils.Serialization
{
    public struct BinaryWriter : IBinaryWriter
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int WriteUint8(byte value, IntPtr buffer)
        {
            return ByteIO.WriteUnmanaged(buffer, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int WriteInt8(sbyte value, IntPtr buffer)
        {
            return ByteIO.WriteUnmanaged(buffer, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int WriteUint16(ushort value, IntPtr buffer)
        {
            return ByteIO.WriteUnmanaged(buffer, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int WriteInt16(short value, IntPtr buffer)
        {
            return ByteIO.WriteUnmanaged(buffer, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int WriteUint32(uint value, IntPtr buffer)
        {
            return ByteIO.WriteUnmanaged(buffer, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int WriteInt32(int value, IntPtr buffer)
        {
            return ByteIO.WriteUnmanaged(buffer, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int WriteUint64(ulong value, IntPtr buffer)
        {
            return ByteIO.WriteUnmanaged(buffer, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int WriteInt64(long value, IntPtr buffer)
        {
            return ByteIO.WriteUnmanaged(buffer, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int WriteFloat16(half value, IntPtr buffer)
        {
            return ByteIO.WriteUnmanaged(buffer, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int WriteFloat32(float value, IntPtr buffer)
        {
            return ByteIO.WriteUnmanaged(buffer, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int WriteFloat64(double value, IntPtr buffer)
        {
            return ByteIO.WriteUnmanaged(buffer, value);
        }
    }
}
