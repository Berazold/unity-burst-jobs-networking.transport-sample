﻿using System;
using System.IO;
using CrazyRam.Core;
using CrazyRam.Core.Helpers;
using UnityEngine;

namespace Pong.Utils
{
    public static class PongSaveUtils
    {
        private const string DefaultSaveName = "save.data";

        public static SaveData PreloadSaveData(XORCipher16BytesKey key, string saveName = default)
        {
            if (string.IsNullOrEmpty(saveName))
                saveName = DefaultSaveName;
            var path = Path.Combine(Application.persistentDataPath, saveName);
            var keyData = key.Key(ref JobAllocator<byte>.Default);
            var saveData = SaveManager.Load<SaveData>(path, keyData).Unwrap(SaveData.Default);
            keyData.Dispose();
            return saveData;
        }

        public static void SaveState(ref SaveData data, XORCipher16BytesKey key, string saveName = default)
        {
            if (string.IsNullOrEmpty(saveName))
                saveName = DefaultSaveName;
            var path = Path.Combine(Application.persistentDataPath, saveName);
            var keyData = key.Key(ref JobAllocator<byte>.Default);
            SaveManager.Save(ref data, path, keyData);
            keyData.Dispose();
        }

        public static void ClearSave(string saveName = default)
        {
            if (string.IsNullOrEmpty(saveName))
                saveName = DefaultSaveName;
            var path = Path.Combine(Application.persistentDataPath, saveName);
            if (File.Exists(path))
            {
                try
                {
                    File.Delete(path);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }
    }
}
