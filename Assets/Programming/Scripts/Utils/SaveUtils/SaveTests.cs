﻿using System.IO;
using CrazyRam.Core;
using CrazyRam.Core.Helpers;
using Pong;
using Pong.Utils;
using UnityEngine;

public class SaveTests : MonoBehaviour
{
    [SerializeField]
    private SaveData _saveData;

    [SerializeField]
    private SaveData _readedData;

    [SerializeField]
    private XORCipher16BytesKey _key;

    private void Start()
    {
        var path = Path.Combine(Application.persistentDataPath, "save.json");
        Debug.Log(path);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            var path = Path.Combine(Application.persistentDataPath, "save.json");
            var keyData = _key.Key(ref JobAllocator<byte>.Default);
            SaveManager.Save(ref _saveData, path, keyData);
            keyData.Dispose();
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            var path = Path.Combine(Application.persistentDataPath, "save.json");
            var keyData = _key.Key(ref JobAllocator<byte>.Default);
            _readedData = SaveManager.Load<SaveData>(path, keyData).Unwrap(SaveData.Default);
            keyData.Dispose();
        }
    }
}
