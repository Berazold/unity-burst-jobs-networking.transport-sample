﻿using System.IO;
using CrazyRam.Editor;
using UnityEditor;
using UnityEngine;

namespace Pong.Utils
{
    [CustomPropertyDrawer(typeof(XORCipher16BytesKey), true)]
    public class XORCipher16BytesKeyDrawer : PropertyDrawer
    {
        private static readonly float LineHeight = EditorGUIUtility.singleLineHeight;

        private static readonly float LineSpacing = EditorGUIUtility.standardVerticalSpacing;

        private static readonly float LineVerticalOffset = LineHeight + LineSpacing;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return property.isExpanded ? ((LineHeight * 5) + 4 * LineSpacing) : LineHeight;
        }

        private static bool SaveKeyDialog(string path)
        {
            return EditorUtility.DisplayDialog("Key backup",
                $"Do you really want to rewrite key at path {path}", "Yes", "Cancel");
        }

        private static bool LoadKeyDialog(string path)
        {
            return EditorUtility.DisplayDialog("Key load",
                $"Do you really want to reload key at path {path}", "Yes", "Cancel");
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            position.height = LineHeight;
            var (hasValue, key) = fieldInfo.FetchActualObject<XORCipher16BytesKey>(property);
            if (!hasValue || key == null)
            {
                EditorGUI.LabelField(position, label.text, $"No key available");
                return;
            }

            property.isExpanded = EditorGUI.Foldout(position, property.isExpanded, label);
            if (!property.isExpanded)
                return;
            EditorGUI.indentLevel++;
            position.y += LineVerticalOffset;
            position = EditorGUI.IndentedRect(position);

            var rawKey = key.RawKey;
            var keyHash = $"{rawKey.x:x8}:{rawKey.y:x8}:{rawKey.z:x8}:{rawKey.w:x8}";
            EditorGUI.LabelField(position, "Hash:", keyHash);

            position.y += LineVerticalOffset;
            EditorGUI.PropertyField(position, property.FindPropertyRelative("_backupPath"));

            var randomizeButton = new Rect(position);
            randomizeButton.y += LineVerticalOffset;

            if (GUI.Button(randomizeButton, "Randomize"))
                key.Randomize();

            const float buttonsOffset = 0.05f;
            var buttonWidth = position.width * (0.5f - buttonsOffset * 0.5f);

            var ioControls = new Rect(randomizeButton);
            ioControls.y += LineVerticalOffset;

            ioControls.width = buttonWidth;
            if (GUI.Button(ioControls, "Save"))
            {
                var keyPath = key.KeyPath;
                if (!File.Exists(keyPath) || SaveKeyDialog(keyPath))
                    key.DumpToDisk(keyPath);
            }

            ioControls.x += buttonWidth + position.width * buttonsOffset;
            if (GUI.Button(ioControls, "Load"))
            {
                var keyPath = key.KeyPath;
                if (LoadKeyDialog(keyPath))
                    key.RestoreFromDisk(keyPath);
            }
        }
    }
}
