﻿using System;
using System.IO;
using CrazyRam.Core.Helpers;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
using UnityEngine;
using Random = Unity.Mathematics.Random;

namespace Pong.Utils
{
    [Serializable]
    public class XORCipher16BytesKey
    {
        [SerializeField]
        private uint4 _key;

#if UNITY_EDITOR
        [SerializeField]
        private string _backupPath;

        public string KeyPath => string.IsNullOrEmpty(_backupPath) ? DefaultKeyBackupPath : _backupPath;

        public uint4 RawKey => _key;
#endif

#if UNITY_EDITOR
        private const string DefaultKeyBackupPath = "Configs/save_xor_key.json";

        public void Randomize()
        {
            var seed = unchecked((uint) Guid.NewGuid().GetHashCode());
            var rnd = new Random(seed);
            _key = rnd.NextUInt4();
        }

        public void DumpToDisk(string path)
        {
            if (string.IsNullOrEmpty(path))
                throw new NullReferenceException("Key path is invalid!");
            var json = JsonUtility.ToJson(this);
            var parent = Directory.GetParent(path);
            if (!Directory.Exists(parent.FullName))
                Directory.CreateDirectory(parent.FullName);
            File.WriteAllText(path, json);
        }

        public void RestoreFromDisk(string path)
        {
            if (!File.Exists(path))
                return;
            var json = File.ReadAllText(KeyPath);
            var key = JsonUtility.FromJson<XORCipher16BytesKey>(json);
            _key = key._key;
        }
#endif

        public NativeArray<byte> Key<T>(ref T allocator) where T : struct, IMemoryAllocator<byte>
        {
            var buffer = allocator.Allocate(sizeof(uint) * 4, NativeArrayOptions.UninitializedMemory);
            unsafe
            {
                fixed (void* keyPtr = &_key)
                {
                    UnsafeUtility.MemCpy(buffer.GetUnsafePtr(), keyPtr, buffer.Length);
                }
            }

            return buffer;
        }

        public override string ToString()
        {
            return $"XOR key: {_key}";
        }
    }
}
