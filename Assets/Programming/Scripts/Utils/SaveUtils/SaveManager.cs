﻿using System.IO;
using System.Text;
using CrazyRam.Core;
using CrazyRam.Core.Helpers;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace Pong.Utils
{
    public static class SaveManager
    {
        [BurstCompile]
        private struct Xor16BytesJob : IJob
        {
            [WriteOnly]
            public NativeArray<byte> Result;

            [ReadOnly]
            public NativeArray<byte> Source;

            [ReadOnly]
            public NativeArray<byte> Key;

            public unsafe void Execute()
            {
                const int vectorLength = sizeof(uint) * 4;
                var vectorizedCount = Result.Length / vectorLength;
                var keyVector = *(uint4*) Key.GetUnsafeReadOnlyPtr();
                for (int i = 0; i < vectorizedCount; i++)
                {
                    var result = Source.ReinterpretLoad<uint4>(i) ^ keyVector;
                    Result.ReinterpretStore(i, result);
                }

                int tail = Result.Length - (vectorizedCount * vectorLength);
                for (int i = tail; i < Result.Length; i++)
                    Result[i] = (byte) ((uint) Source[i] ^ Key[i % Key.Length]);
            }
        }

        [BurstCompile]
        private struct XorJob : IJob
        {
            [WriteOnly]
            public NativeArray<byte> Result;

            [ReadOnly]
            public NativeArray<byte> Source;

            [ReadOnly]
            public NativeArray<byte> Key;

            public void Execute()
            {
                const int vectorLength = sizeof(uint) * 4;
                var vectorizedCount = Result.Length / vectorLength;
                var vectorizedKeyLength = Key.Length / vectorLength;
                for (int i = 0; i < vectorizedCount; i++)
                {
                    var result = Source.ReinterpretLoad<uint4>(i) ^ Key.ReinterpretLoad<uint4>(i % vectorizedKeyLength);
                    Result.ReinterpretStore(i, result);
                }

                int tail = Result.Length - (vectorizedCount * vectorLength);
                for (int i = tail; i < Result.Length; i++)
                    Result[i] = (byte) ((uint) Source[i] ^ Key[i % Key.Length]);
            }
        }

        private static void ExecuteXor(NativeArray<byte> output, NativeArray<byte> input, NativeArray<byte> key)
        {
            var job = new XorJob
            {
                Result = output,
                Source = input,
                Key = key,
            };
            job.Schedule().Complete();
        }

        public static unsafe void Save<T>(ref T data, string path, NativeArray<byte> key)
        {
            //todo use custom serializer to avoid boxing and serialize directly into void* or NativeArray
            var text = JsonUtility.ToJson(data, false);
            var textBytes = Encoding.UTF8.GetByteCount(text);

            var inputBuffer = MemoryHelper.Job<byte>(textBytes, NativeArrayOptions.UninitializedMemory);
            var outputBuffer = MemoryHelper.Job<byte>(inputBuffer.Length, NativeArrayOptions.UninitializedMemory);

            fixed (char* jsonPtr = text)
            {
                Encoding.UTF8.GetBytes(jsonPtr, text.Length, (byte*) inputBuffer.GetUnsafePtr(), textBytes);
            }

            ExecuteXor(outputBuffer, inputBuffer, key);

            using (var encryptedSave =
                new UnmanagedMemoryStream((byte*) outputBuffer.GetUnsafeReadOnlyPtr(), outputBuffer.Length))
            {
                using (var outputFile = new FileStream(path, FileMode.Create))
                {
                    encryptedSave.CopyTo(outputFile);
                }
            }

            inputBuffer.Dispose();
            outputBuffer.Dispose();
        }

        public static unsafe Option<T> Load<T>(string path, NativeArray<byte> key)
        {
            var info = new FileInfo(path);
            if (!info.Exists || info.Length == 0)
                return Option<T>.None;

            var input = MemoryHelper.Job<byte>((int) info.Length, NativeArrayOptions.UninitializedMemory);
            var output = MemoryHelper.Job<byte>(input.Length, NativeArrayOptions.UninitializedMemory);

            using (var inputFile = new FileStream(path, FileMode.Open))
            {
                using (var encryptedSave =
                    new UnmanagedMemoryStream((byte*) input.GetUnsafePtr(), input.Length, input.Length,
                        FileAccess.Write))
                {
                    inputFile.CopyTo(encryptedSave);
                }
            }

            ExecuteXor(output, input, key);

            var json = Encoding.UTF8.GetString((byte*) output.GetUnsafeReadOnlyPtr(), output.Length);
            var result = JsonUtility.FromJson<T>(json);

            input.Dispose();
            output.Dispose();

            return result.AsOption();
        }
    }
}
