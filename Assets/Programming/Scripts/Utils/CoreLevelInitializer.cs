﻿using CrazyRam.Core.Initialization;
using CrazyRam.Core.MessageBus;
using Pong.Events;
using UnityEngine;

namespace Pong
{
    public class CoreLevelInitializer : MonoBehaviour, IInitializer
    {
        [SerializeField]
        private Camera _camera;

        [SerializeField]
        private RectTransform _rootCanvas;

        public void Init()
        {
            var message = new CoreSceneLoadedEvent(_camera, _rootCanvas);
            message.RaiseEvent();
        }
    }
}
