﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pong
{
    public abstract class LoaderBase : MonoBehaviour
    {
        [SerializeField]
        protected PongLevels LevelToLoad;

        public virtual void Load()
        {
            SceneManager.LoadSceneAsync((int) LevelToLoad);
        }
    }
}
