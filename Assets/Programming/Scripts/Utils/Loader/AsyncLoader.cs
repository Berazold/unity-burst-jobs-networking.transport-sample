﻿using System;
using System.Threading.Tasks;
using CrazyRam.Core.Initialization;
using UnityEngine;

namespace Pong
{
    public class AsyncLoader : LoaderBase, IInitializer
    {
        [SerializeField]
        private float _waitTime;

        public void Init()
        {
            WaitAndLoad(_waitTime);
        }

        private async void WaitAndLoad(float seconds)
        {
            await Task.Delay(TimeSpan.FromSeconds(seconds));
            Load();
        }
    }
}
