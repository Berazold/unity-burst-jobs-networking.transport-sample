﻿using CrazyRam.Core.Initialization;

namespace Pong
{
    public class StartupLoader : LoaderBase, IInitializer
    {
        public void Init()
        {
            Load();
        }
    }
}
