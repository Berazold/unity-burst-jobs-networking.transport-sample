﻿using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using CrazyRam.Core.MessageBus;
using CrazyRam.Core.Utils;
using Pong.Events;
using Pong.Network;
using Pong.Utils.Serialization;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Networking.Transport;
using UnityEngine;

public static class NetworkHelper
{
    private const string GoogleDns = "8.8.8.8";

    private const string Level3Dns = "4.2.2.2";

    private const float PingLifetime = 5;

    public static async Task<bool> CheckInternetConnection()
    {
        var ping = new Ping(GoogleDns);

        var startTime = Time.realtimeSinceStartup;
        while (!ping.isDone)
        {
            await Task.Yield();
            if (Time.realtimeSinceStartup - startTime >= PingLifetime)
                break;
        }

        var result = ping.isDone;
        ping.DestroyPing();
        return result;
    }

    public static async void RefreshInternetConnection()
    {
        var pingTask = CheckInternetConnection();
        await pingTask;
        var connectionEvent = new NetworkConnectionRefreshEvent(pingTask.Result);
        connectionEvent.RaiseEvent();
    }

    public static unsafe NativeList<NetworkEndPoint.RawNetworkAddress> CollectLocalLanAddresses(Allocator allocator)
    {
        var list = new NativeList<NetworkEndPoint.RawNetworkAddress>(1, allocator);

        var host = Dns.GetHostEntry(Dns.GetHostName());
        var addressList = host.AddressList;
        for (int i = 0; i < addressList.Length; i++)
        {
            var ip = addressList[i];
            if (ip.AddressFamily != AddressFamily.InterNetwork)
                continue;
            var bytes = ip.GetAddressBytes();
            var address = new NetworkEndPoint.RawNetworkAddress();
            fixed (byte* addr = bytes)
                UnsafeUtility.MemCpy(address.ipv4_bytes, addr, sizeof(uint));
            list.Add(address);
        }

        return list;
    }

    public static unsafe void WriteMessage(ref DataStreamWriter writer, ref TransportMessage message,
        byte isLittleEndian)
    {
        //start header

        writer.WriteByte(isLittleEndian);
        writer.WriteByte(message.CommandType);
        writer.WriteUShort(message.Size);
        writer.WriteUShort(0);
        writer.WriteUInt(message.Tick);

        //end header

        //message
        writer.WriteBytes((byte*) message.Data, message.Size);
        UnsafeUtility.Free((void*) message.Data, Allocator.TempJob);
    }

    public static unsafe bool ReadMessage(ref DataStreamReader reader, ref NetworkConnection connection,
        byte isLittleEndian, out TransportMessage message)
    {
        if (reader.GetBytesRead() + CommandHeader.BinarySize >= reader.Length)
        {
            message = default;
            return false;
        }

        byte littleEndian = reader.ReadByte();
        bool shouldReverse = (isLittleEndian == ByteAction.On) ^ (littleEndian == ByteAction.On);

        var commandType = reader.ReadByte();

        var size = reader.ReadUShort();
        if (shouldReverse)
            size = ByteIO.ByteSwap(size);

        var magic = reader.ReadUShort();
        if (shouldReverse)
            magic = ByteIO.ByteSwap(magic);

        var tick = reader.ReadUInt();
        if (shouldReverse)
            tick = ByteIO.ByteSwap(tick);

        if (reader.GetBytesRead() + size > reader.Length)
        {
            message = default;
            return false;
        }

        var memory = UnsafeUtility.Malloc(size, UnsafeUtility.AlignOf<byte>(), Allocator.TempJob);
        reader.ReadBytes((byte*) memory, size);
        message = new TransportMessage(connection, (long) memory, tick, size, littleEndian, commandType);
        return true;
    }
}
