﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;

namespace Pong.Utils
{
    public static class NativeFixedListHelper
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SafeRelease<T>(this NativeFixedList<T> array) where T : struct
        {
            if (array.IsCreated)
                array.Dispose();
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    [NativeContainer]
    public unsafe struct NativeFixedList<T> : IDisposable where T : struct
    {
        [NativeDisableUnsafePtrRestriction]
        private int* _counter;

        [NativeDisableUnsafePtrRestriction]
        private void* _buffer;

#if ENABLE_UNITY_COLLECTIONS_CHECKS
        //Unity3d 'magic' field
        private AtomicSafetyHandle m_Safety;

        //Unity3d 'magic' field
        [NativeSetClassTypeToNullOnSchedule]
        private DisposeSentinel m_DisposeSentinel;
#endif

        private Allocator _allocator;

        private int _capacity;

        public bool IsCreated => _buffer != null || _counter != null;

        public int Count
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => math.min((*_counter) + 1, _capacity);
        }

        public int Capacity
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _capacity;
        }

        public T this[int index]
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => UnsafeUtility.ReadArrayElement<T>(_buffer, index);
        }

        public NativeFixedList(Allocator allocator, int capacity, bool clearMemory = false)
        {
            _allocator = allocator;
            _capacity = capacity;
            _counter = (int*) UnsafeUtility.Malloc(UnsafeUtility.SizeOf<int>(), 4, allocator);
            *_counter = -1;

            var totalBytes = Marshal.SizeOf<T>() * capacity;
            _buffer = UnsafeUtility.Malloc(totalBytes, UnsafeUtility.AlignOf<T>(), _allocator);

#if ENABLE_UNITY_COLLECTIONS_CHECKS
            DisposeSentinel.Create(out m_Safety, out m_DisposeSentinel, 0, allocator);
#endif
            if (clearMemory)
                UnsafeUtility.MemClear(_buffer, totalBytes);
        }

        public void Reset()
        {
            *_counter = -1;
        }

        public void Add(T data)
        {
            int index = Interlocked.Increment(ref *_counter);
            UnsafeUtility.WriteArrayElement(_buffer, index % _capacity, data);
        }

        public void Dispose()
        {
            if (_allocator <= Allocator.None)
                return;
#if ENABLE_UNITY_COLLECTIONS_CHECKS
            DisposeSentinel.Dispose(ref m_Safety, ref m_DisposeSentinel);
#endif
            if (_counter != null)
                UnsafeUtility.Free(_counter, _allocator);
            if (_buffer != null)
                UnsafeUtility.Free(_buffer, _allocator);

            _counter = null;
            _buffer = null;
        }
    }
}
