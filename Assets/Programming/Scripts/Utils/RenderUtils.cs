﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Pong.Visual
{
    public static class RenderUtils
    {
#region Rendering

        public static readonly int MainTexId = Shader.PropertyToID("_MainTex");

        private static Mesh _fullscreenTriangle;

        /// <summary>
        /// A fullscreen triangle mesh.
        /// </summary>
        public static Mesh FullscreenTriangle
        {
            get
            {
                if (_fullscreenTriangle != null)
                    return _fullscreenTriangle;

                _fullscreenTriangle = new Mesh {name = "Fullscreen Triangle"};

                // Because we have to support older platforms (GLES2/3, DX9 etc) we can't do all of
                // this directly in the vertex shader using vertex ids :(
                _fullscreenTriangle.SetVertices(new List<Vector3>
                {
                    new Vector3(-1f, -1f, 0f),
                    new Vector3(-1f, 3f, 0f),
                    new Vector3(3f, -1f, 0f)
                });
                _fullscreenTriangle.SetIndices(new[] {0, 1, 2}, MeshTopology.Triangles, 0, false);
                _fullscreenTriangle.UploadMeshData(false);

                return _fullscreenTriangle;
            }
        }

        /// <summary>
        /// Sets the current render target using specified <see cref="RenderBufferLoadAction"/>.
        /// </summary>
        public static void SetRenderTargetWithLoadStoreAction(this CommandBuffer cmd, RenderTargetIdentifier rt,
            RenderBufferLoadAction loadAction, RenderBufferStoreAction storeAction)
        {
#if UNITY_2018_2_OR_NEWER
            cmd.SetRenderTarget(rt, loadAction, storeAction);
#else
            cmd.SetRenderTarget(rt);
#endif
        }

        /// <summary>
        /// Sets the current render target and its depth using specified <see cref="RenderBufferLoadAction"/>.
        /// </summary>
        public static void SetRenderTargetWithLoadStoreAction(this CommandBuffer cmd,
            RenderTargetIdentifier color, RenderBufferLoadAction colorLoadAction,
            RenderBufferStoreAction colorStoreAction,
            RenderTargetIdentifier depth, RenderBufferLoadAction depthLoadAction,
            RenderBufferStoreAction depthStoreAction)
        {
#if UNITY_2018_2_OR_NEWER
            cmd.SetRenderTarget(color, colorLoadAction, colorStoreAction, depth, depthLoadAction, depthStoreAction);
#else
            cmd.SetRenderTarget(color, depth);
#endif
        }

        /// <summary>
        /// Does a copy of source to destination using a fullscreen triangle.
        /// </summary>
        public static void BlitFullscreenTriangle(this CommandBuffer cmd, RenderTargetIdentifier source,
            RenderTargetIdentifier destination, Material material, bool clear = false, Rect? viewport = null)
        {
            cmd.SetGlobalTexture(MainTexId, source);
            cmd.SetRenderTargetWithLoadStoreAction(destination,
                viewport == null ? RenderBufferLoadAction.DontCare : RenderBufferLoadAction.Load,
                RenderBufferStoreAction.Store);

            if (viewport != null)
                cmd.SetViewport(viewport.Value);

            if (clear)
                cmd.ClearRenderTarget(true, true, Color.clear);

            cmd.DrawMesh(FullscreenTriangle, Matrix4x4.identity, material, 0, 0);
        }

        /// <summary>
        /// Blits a fullscreen triangle using a given material.
        /// </summary>
        public static void BlitFullscreenTriangle(this CommandBuffer cmd, RenderTargetIdentifier source,
            RenderTargetIdentifier destination, Material material, MaterialPropertyBlock properties, int pass,
            RenderBufferLoadAction loadAction, Rect? viewport = null)
        {
            cmd.SetGlobalTexture(MainTexId, source);
#if UNITY_2018_2_OR_NEWER
            bool clear = (loadAction == RenderBufferLoadAction.Clear);
            if (clear)
                loadAction = RenderBufferLoadAction.DontCare;
#else
            bool clear = false;
#endif
            cmd.SetRenderTargetWithLoadStoreAction(destination,
                viewport == null ? loadAction : RenderBufferLoadAction.Load, RenderBufferStoreAction.Store);

            if (viewport != null)
                cmd.SetViewport(viewport.Value);

            if (clear)
                cmd.ClearRenderTarget(true, true, Color.clear);

            cmd.DrawMesh(FullscreenTriangle, Matrix4x4.identity, material, 0, pass, properties);
        }

        /// <summary>
        /// Blits a fullscreen triangle using a given material.
        /// </summary>
        public static void BlitFullscreenTriangle(this CommandBuffer cmd, RenderTargetIdentifier source,
            RenderTargetIdentifier destination, Material material, MaterialPropertyBlock properties, int pass,
            bool clear = false,
            Rect? viewport = null)
        {
#if UNITY_2018_2_OR_NEWER
            cmd.BlitFullscreenTriangle(source, destination, material, properties, pass,
                clear ? RenderBufferLoadAction.Clear : RenderBufferLoadAction.DontCare, viewport);
#else
            cmd.SetGlobalTexture(ShaderIDs.MainTex, source);
            cmd.SetRenderTargetWithLoadStoreAction(destination, viewport == null ? LoadAction.DontCare : LoadAction.Load, StoreAction.Store);

            if (viewport != null)
                cmd.SetViewport(viewport.Value);

            if (clear)
                cmd.ClearRenderTarget(true, true, Color.clear);

            cmd.DrawMesh(fullscreenTriangle, Matrix4x4.identity, propertySheet.material, 0, pass, propertySheet.properties);
#endif
        }

        /// <summary>
        /// Blits a fullscreen triangle using a given material.
        /// </summary>
        public static void BlitFullscreenTriangleToTexArray(this CommandBuffer cmd, RenderTargetIdentifier source,
            RenderTargetIdentifier destination, Material material, MaterialPropertyBlock properties, int pass,
            bool clear = false, int depthSlice = -1)
        {
            cmd.SetGlobalTexture(MainTexId, source);
            cmd.SetGlobalFloat(MainTexId, depthSlice);
            cmd.SetRenderTarget(destination, 0, CubemapFace.Unknown, -1);

            if (clear)
                cmd.ClearRenderTarget(true, true, Color.clear);

            cmd.DrawMesh(FullscreenTriangle, Matrix4x4.identity, material, 0, pass, properties);
        }

        /// <summary>
        /// Blits a fullscreen triangle using a given material.
        /// </summary>
        public static void BlitFullscreenTriangle(this CommandBuffer cmd, RenderTargetIdentifier source,
            RenderTargetIdentifier destination, RenderTargetIdentifier depth, Material material,
            MaterialPropertyBlock properties, int pass, bool clear = false, Rect? viewport = null)
        {
            cmd.SetGlobalTexture(MainTexId, source);

            var loadAction = viewport == null ? RenderBufferLoadAction.DontCare : RenderBufferLoadAction.Load;
            if (clear)
            {
                cmd.SetRenderTargetWithLoadStoreAction(destination, loadAction, RenderBufferStoreAction.Store, depth,
                    loadAction, RenderBufferStoreAction.Store);
                cmd.ClearRenderTarget(true, true, Color.clear);
            }
            else
            {
                cmd.SetRenderTargetWithLoadStoreAction(destination, loadAction, RenderBufferStoreAction.Store, depth,
                    RenderBufferLoadAction.Load, RenderBufferStoreAction.Store);
            }

            if (viewport != null)
                cmd.SetViewport(viewport.Value);

            cmd.DrawMesh(FullscreenTriangle, Matrix4x4.identity, material, 0, pass, properties);
        }

        /// <summary>
        /// Blits a fullscreen triangle using a given material.
        /// </summary>
        public static void BlitFullscreenTriangle(this CommandBuffer cmd, RenderTargetIdentifier source,
            RenderTargetIdentifier[] destinations, RenderTargetIdentifier depth, Material material,
            MaterialPropertyBlock properties, int pass,
            bool clear = false, Rect? viewport = null)
        {
            cmd.SetGlobalTexture(MainTexId, source);
            cmd.SetRenderTarget(destinations, depth);

            if (viewport != null)
                cmd.SetViewport(viewport.Value);

            if (clear)
                cmd.ClearRenderTarget(true, true, Color.clear);

            cmd.DrawMesh(FullscreenTriangle, Matrix4x4.identity, material, 0, pass, properties);
        }

        /// <summary>
        /// Does a copy of source to destination using the builtin blit command.
        /// </summary>
        public static void BuiltinBlit(this CommandBuffer cmd, RenderTargetIdentifier source,
            RenderTargetIdentifier destination)
        {
#if UNITY_2018_2_OR_NEWER
            cmd.SetRenderTarget(destination, RenderBufferLoadAction.DontCare, RenderBufferStoreAction.Store);
            destination = BuiltinRenderTextureType.CurrentActive;
#endif
            cmd.Blit(source, destination);
        }

        /// <summary>
        /// Blits a fullscreen quad using the builtin blit command and a given material.
        /// </summary>
        public static void BuiltinBlit(this CommandBuffer cmd, RenderTargetIdentifier source,
            RenderTargetIdentifier destination, Material mat, int pass = 0)
        {
#if UNITY_2018_2_OR_NEWER
            cmd.SetRenderTarget(destination, RenderBufferLoadAction.DontCare, RenderBufferStoreAction.Store);
            destination = BuiltinRenderTextureType.CurrentActive;
#endif
            cmd.Blit(source, destination, mat, pass);
        }

        public static void CopyTexture(CommandBuffer cmd, RenderTargetIdentifier source,
            RenderTargetIdentifier destination, Material material)
        {
            if (SystemInfo.copyTextureSupport > CopyTextureSupport.None)
            {
                cmd.CopyTexture(source, destination);
                return;
            }

            cmd.BlitFullscreenTriangle(source, destination, material);
        }

#endregion
    }
}
