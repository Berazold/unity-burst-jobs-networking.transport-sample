﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ##CLASS_NAME## : ScriptableObject
{


#if UNITY_EDITOR

    [MenuItem("CrazyRam/Create/" + nameof(##CLASS_NAME##))]
    public static void Create()
    {
        var asset = CreateInstance<##CLASS_NAME##>();
        AssetDatabase.CreateAsset(asset, $"Assets/{nameof(##CLASS_NAME##)}.asset");
    }

#endif

}
