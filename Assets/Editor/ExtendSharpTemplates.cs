﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEditor.ProjectWindowCallback;
using UnityEngine;

public class CrazyCreateFile : EndNameEditAction
{
    public override void Action(int instanceId, string pathName, string resourceFile)
    {
        var newFile = ExtendSharpTemplates.CreateTemplate(pathName, resourceFile);
        if (newFile == null)
            return;
        AssetDatabase.ImportAsset(pathName, ImportAssetOptions.Default);
        ProjectWindowUtil.ShowCreatedAsset(newFile);
    }
}

public static class ExtendSharpTemplates
{
    private class FolderTemplate
    {
        public string Parent;

        public string FolderName;
    }

    private const string ClassName = "##CLASS_NAME##";

    private static readonly FolderTemplate[] Foldes =
    {
        new FolderTemplate {Parent = "Assets", FolderName = "ArtContent"},
        new FolderTemplate {Parent = "Assets", FolderName = "GameContent"},
        new FolderTemplate {Parent = "Assets", FolderName = "Programming"},
        new FolderTemplate {Parent = "Assets/ArtContent", FolderName = "Textures"},
        new FolderTemplate {Parent = "Assets/GameContent", FolderName = "Prefabs"},
        new FolderTemplate {Parent = "Assets/GameContent", FolderName = "Audio"},
        new FolderTemplate {Parent = "Assets/Programming", FolderName = "CrazyPlugins"},
        new FolderTemplate {Parent = "Assets/Programming", FolderName = "Scripts"},
        new FolderTemplate {Parent = "Assets/Programming", FolderName = "Shaders"},
        new FolderTemplate {Parent = "Assets/Programming", FolderName = "ScriptableObjects"},
        new FolderTemplate {Parent = "Assets/Programming/Scripts", FolderName = "UI"},
        new FolderTemplate {Parent = "Assets/Programming/Scripts", FolderName = "Core"},
        new FolderTemplate {Parent = "Assets/Programming/Scripts", FolderName = "Db"},
        new FolderTemplate {Parent = "Assets/Programming/Scripts", FolderName = "Utils"},
        new FolderTemplate {Parent = "Assets", FolderName = "Scenes"},
    };

    public static void CreateFileHierarchy()
    {
        foreach (var template in Foldes)
        {
            if (!AssetDatabase.IsValidFolder(Path.Combine(template.Parent, template.FolderName)))
                AssetDatabase.CreateFolder(template.Parent, template.FolderName);
        }
    }

    public static Object CreateTemplate(string path, string templatePath, string specialClassName = null)
    {
        string newFile = Path.GetFullPath(path);
        string fileName = specialClassName ?? Path.GetFileNameWithoutExtension(path);
        if (string.IsNullOrEmpty(fileName))
        {
            Debug.Log("FileName is null");
            return null;
        }

        string className = fileName.Replace(" ", string.Empty);
        if (!File.Exists(templatePath))
        {
            Debug.Log("Template file is null");
            return null;
        }

        string template;

        using (var sr = new StreamReader(templatePath))
        {
            template = sr.ReadToEnd();
        }

        template = template.Replace(ClassName, className);

        var encoding = new UTF8Encoding(true, false);

        using (var sw = new StreamWriter(newFile, false, encoding))
        {
            sw.Write(template);
        }

        AssetDatabase.ImportAsset(path);
        var asset = AssetDatabase.LoadAssetAtPath(path, typeof(Object));
        return asset;
    }
}
