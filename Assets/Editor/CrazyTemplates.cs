﻿using UnityEditor;
using UnityEngine;

public static class CrazyTemplates
{
    private static readonly Texture2D ScriptIcon = (EditorGUIUtility.IconContent("cs Script Icon").image as Texture2D);

    private const string MenuItemPath = "Assets/Create/Extend/";

    private const int MenuItemPriority = 60;

    private const string TemplatesPath = "Assets/Editor/CrazyTemplates/";

    [MenuItem(MenuItemPath + "C# Class", false, MenuItemPriority)]
    public static void CreateClass()
    {
        CreateSharpFile("NewClassTemplate.cs", TemplatesPath + @"Class.txt");
    }

    [MenuItem(MenuItemPath + "C# Struct", false, MenuItemPriority)]
    public static void CreateStruct()
    {
        CreateSharpFile("NewStructTemplate.cs", TemplatesPath + @"Struct.txt");
    }

    [MenuItem(MenuItemPath + "C# Event", false, MenuItemPriority)]
    public static void CreateEventTemplate()
    {
        CreateSharpFile("NewEventTemplate.cs", TemplatesPath + @"Event.txt");
    }

    [MenuItem(MenuItemPath + "C# Enum", false, MenuItemPriority)]
    public static void CreateEnum()
    {
        CreateSharpFile("NewEnumTemplate.cs", TemplatesPath + @"Enum.txt");
    }

    [MenuItem(MenuItemPath + "C# Interface", false, MenuItemPriority)]
    public static void CreateInterface()
    {
        CreateSharpFile("NewInterfaceTemplate.cs", TemplatesPath + @"Interface.txt");
    }

    [MenuItem(MenuItemPath + "C# Destroy", false, MenuItemPriority)]
    public static void CreateDestroyTemplate()
    {
        CreateSharpFile("NewDestroyTemplate.cs", TemplatesPath + @"Destroy.txt");
    }

    [MenuItem(MenuItemPath + "C# Awake", false, MenuItemPriority)]
    public static void CreateAwakeTemplate()
    {
        CreateSharpFile("NewAwakeTemplate.cs", TemplatesPath + @"Awake.txt");
    }

    [MenuItem(MenuItemPath + "C# ScriptableObject", false, MenuItemPriority)]
    public static void CreateScriptableObjectTemplate()
    {
        CreateSharpFile("NewScriptableObjectTemplate.cs", TemplatesPath + @"ScriptableObject.txt");
    }

    [MenuItem(MenuItemPath + "C# All callbacks", false, MenuItemPriority)]
    public static void CreateAllMonoBehaviourMethodsTemplate()
    {
        CreateSharpFile("NewAllCallbacksTemplate.cs", TemplatesPath + @"AllMonoBehaviourMethods.txt");
    }

    [MenuItem(MenuItemPath + "Tickable", false, MenuItemPriority)]
    public static void CreateClientTickableTemplate()
    {
        CreateSharpFile("NewClientTickableTemplate.cs", TemplatesPath + @"Tickable.txt");
    }

    [MenuItem(MenuItemPath + "GameSystem", false, MenuItemPriority)]
    public static void CreateGameSystemTemplate()
    {
        CreateSharpFile("NewGameSystemTemplate.cs", TemplatesPath + @"GameSystem.txt");
    }

    [MenuItem("Assets/Create/CreateFileHierarchy", false, MenuItemPriority)]
    public static void CreateFileHierarchy()
    {
        ExtendSharpTemplates.CreateFileHierarchy();
    }

    private static void CreateSharpFile(string newPath, string templatePath)
    {
        ProjectWindowUtil.StartNameEditingIfProjectWindowExists(0, ScriptableObject.CreateInstance<CrazyCreateFile>(),
            newPath, ScriptIcon, templatePath);
    }
}
