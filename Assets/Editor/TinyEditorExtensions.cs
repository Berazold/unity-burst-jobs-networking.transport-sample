﻿using System;
using System.Linq;
using CrazyRam.Core.Helpers;
using CrazyRam.Core.Math;
using UnityEngine;
using UnityEditor;

public static class TinyEditorExtensions
{
    [MenuItem("CrazyRam/Editor/Parent/To center")]
    public static void ParentToCenter()
    {
        var objects = Selection.gameObjects;
        if (!objects.IsNullOrEmpty())
            ParentToCenter(objects, FindCenter);
    }

    [MenuItem("CrazyRam/Editor/Parent/To XZ center ")]
    public static void ParentToCenterXz()
    {
        var objects = Selection.gameObjects;
        if (!objects.IsNullOrEmpty())
            ParentToCenter(objects, gameObjects => FindCenter(gameObjects).X0Z());
    }

    private static void ParentToCenter(GameObject[] objects, Func<GameObject[], Vector3> centerSearch)
    {
        var center = centerSearch(objects);

        var root = new GameObject($"{objects[0].name}Root");
        root.transform.position = center;
        Undo.RegisterCreatedObjectUndo(root, "Parent root creation");
        for (int i = 0; i < objects.Length; i++)
            Undo.SetTransformParent(objects[i].transform, root.transform, "Root to center");
        Selection.activeGameObject = root;
    }
    
    private static Vector3 FindCenter(GameObject[] objects)
    {
        var min = objects.Aggregate(Vector3.one * float.MaxValue,
            (storage, item) => FindValue(storage, item, Mathf.Min));
        var max = objects.Aggregate(Vector3.one * float.MinValue,
            (storage, item) => FindValue(storage, item, Mathf.Max));

        return Vector3.Lerp(min, max, 0.5f);        
    }
    
    private static Vector3 FindValue(Vector3 storage, GameObject go, Func<float, float, float> comparer)
    {
        var pos = go.transform.position;
        return new Vector3(
            comparer(storage.x, pos.x),
            comparer(storage.y, pos.y),
            comparer(storage.z, pos.z)
        );
    }
}
