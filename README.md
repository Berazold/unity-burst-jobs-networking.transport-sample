# README #

Project runs on Unity 2019.4.X
This repository uses [Git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules). Before opening the project with Unity, not only the repository itself but also these submodules have to be synced up.
I'm the only contributor of all used submodules since 2015

Project uses new Unity3d features: Burst Compiler, Job system, Unity.Networking.Transport lirary.

## Features ##
* Local network client/server with Unity.Networking.Transport library [NetworkSystem](https://bitbucket.org/Berazold/unity-burst-jobs-networking.transport-sample/src/master/Assets/Programming/Scripts/Core/Systems/Network)
* Decrypt/encrypt saves with XOR cipher. Optimized with Burst Compiler [SaveManager](https://bitbucket.org/Berazold/unity-burst-jobs-networking.transport-sample/src/master/Assets/Programming/Scripts/Utils/SaveUtils/SaveManager.cs)
* Scanning local network to discover a server [NetworkObserverSystem](https://bitbucket.org/Berazold/unity-burst-jobs-networking.transport-sample/src/master/Assets/Programming/Scripts/Core/Systems/Network/NetworkObserverSystem.cs)
* Unsafe endianness aware BinaryReader, BinaryWriter [BinaryUtils](https://bitbucket.org/Berazold/unity-burst-jobs-networking.transport-sample/src/master/Assets/Programming/Scripts/Utils/BinaryUtils/)
* Network commands binary serialization/deserialization [Network Core](https://bitbucket.org/Berazold/unity-burst-jobs-networking.transport-sample/src/master/Assets/Programming/Scripts/Core/Network/Core/)
* Sdf (Signed distance field) UI
* Simple 2D physics system powered by Job System and Burst compiler [PhysicsSystem](https://bitbucket.org/Berazold/unity-burst-jobs-networking.transport-sample/src/master/Assets/Programming/Scripts/Core/BattleSystems/PhysicsSystem/)
* Mobile friendly [GPU particles](https://bitbucket.org/Berazold/unity-burst-jobs-networking.transport-sample/src/master/Assets/Programming/Shaders/Effects/GpuInkTrail/InkTrails.compute) with compute shaders.
* Project uses only procedural graphics


E-mail: yborobkin@gmail.com


There are 3 game modes in the project: 

* Single player
* Multiplayer with split-screen
* LAN Multiplayer

## Project architecture ##
Project does not use ECS, just old school game state structure + message bus

Game state is divided by:

* GlobalState (store persistent data)
* BattleState (store temporal per-match data)

Update loop is splitted into systems, which mutates the game state
Systems send update events into spsc queues [ClientCommutator<T>(simple Message Queue)](https://bitbucket.org/crazyram/crazymessaging/src/e197bd5eadc48b1455300bdf35b8609fd01fb9b1/Core/Channel/Channel.cs) or with callback-based events [Dispatcher<T>](https://bitbucket.org/crazyram/crazymessaging/src/e197bd5eadc48b1455300bdf35b8609fd01fb9b1/Core/Dispatcher/Dispatcher.cs)

Systems initialization order and order of invocations are set in [CoreInitializer](https://bitbucket.org/Berazold/unity-burst-jobs-networking.transport-sample/src/master/Assets/Programming/Scripts/Core/CoreInitializer.cs)

Game systems hierarchy:

* NetworkSystem
	* NetworkObserverSystem
	* NetworkClientSystem | NetworkServerSystem
* LifecycleSystem
* BattleSystem
	* InputSystem | NetworkInputSystem | ClientInputSystem
	* AISystem
	* PhysicsSystem | WorldReplicationSystem
	* WorldSerializationSystem
	* CollisionSystem
	* VisualUpdateSystem
	* RenderExtensionSystem
* UISystem

Advantages of architectute: 

* Each system can be mocked and tested separately
* Reworking part of the game logic does not affect the rest of the application, it can be proven by single/split-screen/multiplayer game modes

Disadvantages:

* Order of systems invocatoin is important

Troubleshooting:

* Android client can't connect to Windows 10 host. 
	* Solution: turn off Windows firewall. Go to Network and Sharing Center -> Select current wireless network -> Properties -> Sharing -> Enable option "Allow other network users to connect trhought this computer's Internet connection"
* Multiplayer does not work in Unity Editor on mac os.
	* Solution:
		* Turn off File sharing in mac os settings
		* Turn off Burst compiler Safety Checks (menu item: Unity3d Jobs -> Burst -> Safety Checks -> Off)
		* Turn off JobsDubugger (menu item: Unity3d Jobs -> JobsDubugger)